import React, { useState } from "react";
import { Grid, Text, Textarea, Input, Button } from "@nextui-org/react";
import Layout from "../../layout";
import Image from "next/image";
import toast, { Toaster } from "react-hot-toast";
import { CONSTANTS } from "../../constants/index";
import axios from "axios";
import { useTranslation } from "next-i18next";
import back from "../../public/assets/lotties/back_black.json";
import Link from "next/link";
import Lottie from "react-lottie";

const CareersGenericComponent = ({
  title,
  description,
  mission1,
  mission2,
  mission3,
  mission4,
  mission5,
  qualification1,
  qualification2,
}) => {
  const defaultOptions1 = {
    loop: true,
    autoplay: true,
    animationData: back,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  const { t } = useTranslation("common");
  const [lastName, setLastName] = useState("");
  const [name, setName] = useState("");
  const [cv, setCv] = useState("");
  const [message, setMessage] = useState("");

  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleCvChange = (event) => {
    const selectedFile = event.target.files[0];
  };
  const handleMessageChange = (event) => {
    setMessage(event.target.value);
  };
  const handleSubmit = (event) => {
    const url = `${CONSTANTS.API_URL_PROD}/user/apply`;

    const formData = new FormData();
    formData.append("name", name);
    formData.append("email", email);
    formData.append("cv", cv);
    formData.append("message", message);

    axios
      .post(url, formData)
      .then((response) => {
        console.log(response);
        toast.success("Candidature envoyé !");
        setName("");
        setEmail("");
        setCv(null);
        setMessage("");
      })
      .catch((error) => {
        console.log(error);
        toast.error(error.response.data.message);
      });
  };
  return (
    <>
      <Layout>
        <Link href="/careers">
          <Grid
            className="back_arrow_terms"
            css={{ position: "fixed", top: 20, right: 120, zIndex: 99 }}
          >
            <Lottie options={defaultOptions1} width={50} />
          </Grid>
        </Link>
        <Toaster
          position="top-center"
          toastOptions={{
            duration: 5000,
          }}
        />
        <Grid.Container className="candidate" gap={2}>
          <Grid md={5} sm={4}>
            <Grid css={{ lineHeight: "1.6em" }}>
              <Text h3>{title}</Text>
              <br></br>

              <Text b>Description</Text>
              <br></br>

              <Text span>{description}</Text>
              <br></br>
              <br></br>

              <Text b>Missions</Text>
              <br></br>
              <br></br>

              <Text span>- {mission1}</Text>
              <br></br>

              <Text span>- {mission2}</Text>
              <br></br>

              <Text span>- {mission3}</Text>
              <br></br>

              <Text span>- {mission4}</Text>
              <br></br>

              <Text span>- {mission5}</Text>
              <br></br>
              <br></br>
              <Text b>Qualifications</Text>
              <br></br>
              <br></br>

              <Text span>- {qualification1}</Text>
              <br></br>
              <Text span>- {qualification2}</Text>
            </Grid>
          </Grid>
          <Grid md={6}>
            <Grid className="candidate-form">
              <Grid>
                <Text b>{t("apply_btn")}</Text>
              </Grid>
              <Grid>
                <Text span>
                  {" "}
                  <label>{t("name")}</label>
                </Text>

                <br></br>

                <Input fullWidth value={name} onChange={handleNameChange} />
              </Grid>

              <Grid>
                <Text span>
                  {" "}
                  <label>{t("lastname")}</label>
                </Text>

                <br></br>

                <Input
                  fullWidth
                  value={lastName}
                  onChange={handleLastNameChange}
                />
              </Grid>

              <Grid>
                <Text span>
                  <label>CV </label>
                </Text>

                <br></br>

                <Input
                  animated={false}
                  fullWidth
                  type="file"
                  onChange={handleCvChange}
                  id="file"
                  name="file"
                  style={{ position: "hidden" }}
                  contentRightStyling={false}
                  contentClickable={true}
                  contentRight={
                    <label aria-label="cv-input">
                      <Image
                        src="/assets/icons/upload.svg"
                        width={50}
                        height={20}
                        alt=""
                        css={{ cursor: "pointer", marginRight: "30px" }}
                      />
                    </label>
                  }
                />
              </Grid>

              <Grid>
                <label>
                  <Text span>MESSAGE</Text>
                </label>
                <br></br>
                <Textarea
                  rows={4}
                  css={{ width: "100%" }}
                  onChange={handleMessageChange}
                />
              </Grid>
              <Grid>
                <Button className="contact-button" onClick={handleSubmit}>
                  {t("send")}
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid.Container>
      </Layout>
    </>
  );
};

export default CareersGenericComponent;

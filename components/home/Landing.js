import React, { useState, useEffect } from "react";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import { Grid, Text } from "@nextui-org/react";

const Landing = () => {
  const { t } = useTranslation("common");
  const [isTelecomVisible, setIsTelecomVisible] = useState(true);
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsTelecomVisible(!isTelecomVisible);
    }, 1000);

    return () => clearTimeout(timer);
  }, [isTelecomVisible]);

  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 768px)");
    setIsMobile(mediaQuery.matches);

    const resizeHandler = () => {
      setIsMobile(mediaQuery.matches);
    };

    mediaQuery.addEventListener("change", resizeHandler);

    return () => {
      mediaQuery.removeEventListener("change", resizeHandler);
    };
  }, []);

  const socialIcons = [
    {
      icon: "/assets/icons/insta.svg",
      alt: "Instagram",
      link: "https://www.instagram.com/duetodata/",
    },
    {
      icon: "/assets/icons/fb.svg",
      alt: "Facebook",
      link: "https://www.facebook.com/DuetoData/",
    },
    {
      icon: "/assets/icons/linkedin.svg",
      alt: "LinkedIn",
      link: "https://www.linkedin.com/company/duetodata/?originalSubdomain=fr",
    },
    {
      icon: "/assets/icons/pinterest.svg",
      alt: "Pinterest",
      link: "https://www.pinterest.com/Due2Data/",
    },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Grid className="circle">
            <Image
              src="/assets/icons/circle.svg"
              alt="circle"
              width={650}
              height={650}
              objectFit="contain"
            />
          </Grid>

          {isTelecomVisible ? (
            <Grid align="center" className="tel">
              <Grid className="wrapper_home">
                <Text h1 className="title_home">
                  {t("tel")}
                </Text>
                <Grid>
                  <Image
                    src="/assets/home/tel.svg"
                    alt="telecommunication"
                    width={500}
                    height={300}
                    objectFit="contain"
                  />
                </Grid>
                <Link href="/services?category=telecommunication">
                  <Text h1 size={"$3xl"} className="link-hover">
                    <a> {t("home_tel_cta")}</a>
                  </Text>
                </Link>
              </Grid>
            </Grid>
          ) : (
            <Grid align="center" className="dev">
              <Grid className="wrapper_home">
                <Text h1 className="title_home">
                  {t("dev")}
                </Text>
                <Grid>
                  <Image
                    src="/assets/home/dev.svg"
                    alt="development"
                    width={500}
                    height={300}
                    objectFit="contain"
                  />
                </Grid>

                <Link href="/services?category=development">
                  <Text h1 size={"$3xl"} className="link-hover">
                    <a> {t("home_dev_cta")}</a>
                  </Text>
                </Link>
              </Grid>
            </Grid>
          )}
        </Grid>
        {isMobile && (
          <Grid className="bg_sm_landing">
            {socialIcons.map((socialIcon, index) => (
              <a
                key={index}
                href={socialIcon.link}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image
                  src={socialIcon.icon}
                  alt={socialIcon.alt}
                  width={25}
                  height={25}
                  objectFit="contain"
                  className="sm_icons"
                />
                <br />
              </a>
            ))}
          </Grid>
        )}
      </Grid>
    </>
  );
};

export default Landing;

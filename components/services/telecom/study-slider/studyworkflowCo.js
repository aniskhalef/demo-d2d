import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import { useTranslation } from "next-i18next";
import Image from "next/image";

const StudyWorkflow = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid align="center">
        <Grid>
          <Text b size={"$2xl"}>
            {t("workflow")}
          </Text>
        </Grid>
        <Grid md={8}>
          <Grid className="wrapper1">
            <Grid className="top">
              <Grid>
                <Text className="number">1</Text>
              </Grid>
              <Grid>
                <Text span className="contentTitle">
                  {t("1needs")}
                </Text>
              </Grid>
            </Grid>
            <Grid>
              <Text span className="content">
                {t("1desc")}
              </Text>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper2">
            <Grid className="top">
              <Grid>
                <Text className="number">2</Text>
              </Grid>
              <Grid>
                <Text span className="contentTitle">
                  {t("2pre")}
                </Text>
              </Grid>
            </Grid>
            <Grid>
              <Text span className="content">
                {t("2desc")}
              </Text>
              <br></br>
              <Text span className="content">
                - {t("2descfirst")}
              </Text>
              <br></br>
              <Text span className="content">
                - {t("2descsecond")}
              </Text>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper3">
            <Grid className="top">
              <Grid>
                <Text className="number">3</Text>
              </Grid>
              <Grid>
                <Text span className="contentTitle">
                  {t("3technical")}
                </Text>
              </Grid>
            </Grid>
            <Grid>
              <Text span className="content">
                {t("3desc")}
              </Text>
            </Grid>
          </Grid>
          {/**/}
          <Grid className="wrapper4">
            <Grid className="top">
              <Grid>
                <Text className="number">4</Text>
              </Grid>
              <Grid>
                <Text span className="contentTitle">
                  {t("4corrdination")}
                </Text>
              </Grid>
            </Grid>
            <Grid>
              <Text span className="content">
                {t("4desc")}
              </Text>
            </Grid>
          </Grid>
          {/**/}
          <Grid md={12} sm={12} xs={12} className="wrapper5">
            <Grid className="top">
              <Grid>
                <Text className="number">5</Text>
              </Grid>
              <Grid>
                <Text span className="contentTitle">
                  {t("5planning")}
                </Text>
              </Grid>
            </Grid>
            <Grid>
              <Text span className="content">
                {t("5desc")}
              </Text>
            </Grid>
            <br></br>
            <Grid style={{ display: "flex", alignItems: "center" }}>
              <Grid style={{ marginRight: "1em" }}>
                <Image src="/assets/icon.svg" alt="" height={80} width={80} />
              </Grid>
              <Text span className="subContent">
                {t("5desc")}
              </Text>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
export default StudyWorkflow;

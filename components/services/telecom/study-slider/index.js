import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import StudyWorkflow from "./studyworkflowCo";
const StudySlider = () => {
  const { t } = useTranslation("common");

  const techs = [
    { image: "/assets/icons/tel/optimum.png" },
    { image: "/assets/icons/tel/refsite.svg" },
    { image: "/assets/icons/tel/qgis.png" },
    { image: "/assets/icons/tel/autocad.png" },
    { image: "/assets/icons/tel/geofibre.png" },
  ];
  const partners = [
    { image: "/assets/icons/tel/circet.png" },
    { image: "/assets/icons/tel/sogetrel.png" },
    { image: "/assets/icons/tel/ensio.png" },
    { image: "/assets/icons/tel/effiage.svg" },
  ];
  return (
    <>
      <Grid>
        <StudyWorkflow />
      </Grid>

      <Grid css={{ marginTop: "5em" }}>
        <Text b size={"$xl"} css={{ textTransform: "uppercase" }}>
          {t("tools")}
        </Text>
      </Grid>
      <Grid.Container>
        {techs.map((tech) => (
          <Grid key={tech.image} className="techstack">
            <Grid>
              <Image
                src={tech.image}
                alt=""
                width={140}
                height={140}
                objectFit="contain"
              />
            </Grid>
          </Grid>
        ))}
      </Grid.Container>
      <Grid>
        <Text b size={"$xl"} css={{ textTransform: "uppercase" }}>
          {t("partners1")}
        </Text>
      </Grid>

      <Grid.Container>
        {partners.map((partner) => (
          <Grid key={partner.image} className="techstack">
            <Grid css={{ position: "relative" }}>
              <Image
                src={partner.image}
                alt=""
                width={140}
                height={140}
                objectFit="contain"
              />
            </Grid>
          </Grid>
        ))}
      </Grid.Container>
      <Grid className="contact-div">
        <Text b size={"$lg"}>
          {t("service_tel_cta_1")}
        </Text>
        <br></br>
        <Text span>{t("service_tel_cta_2")}</Text>
        <br></br>
        <br></br>
        <Button className="contact-button">
          <Link href="/contact-us">
            <Text b className="text-white">
              {t("header2")}
            </Text>
          </Link>
        </Button>
      </Grid>
    </>
  );
};
export default StudySlider;

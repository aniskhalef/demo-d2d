import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
import DeployWorkflow from "./deployWorkflow";
import ShemaTabs from "./tabs";
import CarouselPhoto from "./carousel";
const DeploySlider = () => {
  const { t } = useTranslation("common");

  const partners = [
    { image: "/assets/icons/tel/circet.png" },
    { image: "/assets/icons/tel/sogetrel.png" },
    { image: "/assets/icons/tel/ensio.png" },
    { image: "/assets/icons/tel/effiage.svg" },
  ];

  return (
    <>
      <Grid>
        <Grid>
          <ShemaTabs />
        </Grid>
        <Grid>
          <DeployWorkflow />
        </Grid>
        <br></br>
        <br></br>

        <Grid>
          <Text size={"$xl"} b>
            SCHEMA
          </Text>
        </Grid>
        <br></br>
        <br></br>
        <Grid align="center">
          <Image
            src="/assets/schema_en.svg"
            alt=""
            width={500}
            height={500}
            objectFit="cover"
          />
        </Grid>
        <br></br>
        <br></br>
        <Grid css={{ width: "90%" }}>
          <CarouselPhoto />
        </Grid>
        <br></br>
        <Grid>
          <Text size={"$xl"} b css={{ textTransform: "uppercase" }}>
            {t("security")}
          </Text>
          <br></br>
          <br></br>
          <Grid md={10}>
            <Text span>{t("securitydesc")}</Text>
          </Grid>
        </Grid>
        <br></br>
        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid>
            <Grid>
              <Text size={"$xl"} b css={{ textTransform: "uppercase" }}>
                {t("partners1")}
              </Text>
            </Grid>
            <Grid.Container>
              {partners.map((partner) => (
                <Grid key={partner.image} className="techstack">
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={partner.image}
                      alt=""
                      width={150}
                      height={150}
                      objectFit="contain"
                    />
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_deployment_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_deployment_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};
export default DeploySlider;

import React from "react";
import { Grid, Text } from "@nextui-org/react";
import Image from "next/image";
import { useTranslation } from "next-i18next";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
const CarouselPhoto = () => {
  const { t } = useTranslation("common");
  const images = [
    "/assets/images/tel/realphoto1.png",
    "/assets/images/tel/realphoto2.png",
    "/assets/images/tel/realphoto3.png",
    "/assets/images/tel/realphoto4.png",
    "/assets/images/tel/realphoto5.png",
    "/assets/images/tel/realphoto6.jpeg",
    "/assets/images/tel/realphoto7.jpeg",
    "/assets/images/tel/realphoto8.jpeg",
    "/assets/images/tel/realphoto9.jpeg",
  ];
  return (
    <>
      <Grid>
        <Text size={"$xl"} b css={{ textTransform: "uppercase" }}>
          {t("realphotos")}
        </Text>
      </Grid>
      <br></br>
      <Swiper slidesPerView={4} spaceBetween={10}>
        {images.map((image, index) => (
          <SwiperSlide key={index}>
            <Image
              src={image}
              alt={`Image ${index}`}
              height={900}
              width={900}
              objectFit="cover"
            />
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default CarouselPhoto;

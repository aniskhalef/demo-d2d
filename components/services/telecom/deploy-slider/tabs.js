import React, { useState } from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { Grid, Text } from "@nextui-org/react";
const ShemaTabs = () => {
  const [value, setValue] = useState(0);
  const { t, i18n } = useTranslation("common");

  const handleTabChange = (event, newValue) => {
    setValue(newValue);
  };
  const currentLanguage = i18n.language;

  const getGlobalShemaPath = () => {
    // if (currentLanguage === "en") {
    return "/assets/globalshema_en.svg";
    // } else return "/assets/globalshema.svg";
  };

  const getFTTOPath = () => {
    // if (currentLanguage === "en") {
    return "/assets/ftto_en.svg";
    // } else return "/assets/ftto.svg";
  };
  const getFTTEPath = () => {
    // if (currentLanguage === "en") {
    return "/assets/ftte_en.svg";
    // } else return "/assets/ftte.svg";
  };

  const getFTTHPROPath = () => {
    // if (currentLanguage === "en") {
    return "/assets/ftthpro_en.svg";
    // } else return "/assets/ftthpro.svg";
  };
  const getFTTHPath = () => {
    // if (currentLanguage === "en") {
    return "/assets/ftth_en.svg";
    // } else return "/assets/ftth.svg";
  };
  const renderImage = () => {
    switch (value) {
      case 0:
        return (
          <Image
            src={getGlobalShemaPath()}
            alt="Global Schema"
            height={500}
            width={500}
            objectFit="contain"
          />
        );
      case 1:
        return (
          <Image
            src={getFTTOPath()}
            alt="FTTO"
            height={500}
            width={500}
            objectFit="contain"
          />
        );
      case 2:
        return (
          <Image
            src={getFTTEPath()}
            alt="FTTE"
            height={500}
            objectFit="contain"
            width={500}
          />
        );
      case 3:
        return (
          <Image
            src={getFTTHPROPath()}
            alt="FTTH PRO"
            height={500}
            width={500}
            objectFit="contain"
          />
        );
      case 4:
        return (
          <Image
            src={getFTTHPath()}
            alt="FTTH"
            height={500}
            width={500}
            objectFit="contain"
          />
        );

      default:
        return null;
    }
  };

  return (
    <Grid>
      <Text b size={"$xl"}>
        SCHÉMA DES DIFFÉRENTES FIBRES
      </Text>
      <br></br>
      <br></br>
      <Tabs
        value={value}
        onChange={handleTabChange}
        aria-label="fibers"
        sx={{
          "& .MuiButtonBase-root": {
            color: "#000",
          },
          "& .MuiTabs-indicator": {
            backgroundColor: "#e56919",
          },
        }}
      >
        <Tab label="Global Schema" style={{ border: "1px solid #bbbbbb" }} />
        <Tab label="FTTO" style={{ border: "1px solid #bbbbbb" }} />
        <Tab label="FTTE" style={{ border: "1px solid #bbbbbb" }} />
        <Tab label="FTTH PRO" style={{ border: "1px solid #bbbbbb" }} />
        <Tab label="FTTH" style={{ border: "1px solid #bbbbbb" }} />
      </Tabs>
      {renderImage()}
    </Grid>
  );
};

export default ShemaTabs;

import React from "react";
import { Grid, Text } from "@nextui-org/react";
import { useTranslation } from "next-i18next";

const DeployWorkflow = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$xl"}>
            {t("ETAPES")}
          </Text>
        </Grid>
        <br></br>
        <br></br>
        <Grid md={8} css={{ margin: "auto" }}>
          <Grid>
            <Grid className="wrapper1">
              <Grid className="top">
                <Grid>
                  <Text span className={"number"}>
                    1
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("step1title")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("step1content")}</Text>
              </Grid>
            </Grid>
            {/**/}
            <Grid className={"wrapper2"}>
              <Grid className={"top"}>
                <Grid>
                  <Text span className={"number"}>
                    2
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("step2title")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("step2content")}</Text>
              </Grid>
            </Grid>
            {/**/}
            <Grid className={"wrapper3"}>
              <Grid className={"top"}>
                <Grid>
                  <Text span className={"number"}>
                    3
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("step3title")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("step3content")}</Text>
              </Grid>
            </Grid>
            {/**/}
            <Grid className={"wrapper4"}>
              <Grid className={"top"}>
                <Grid>
                  <Text span className={"number"}>
                    4
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("step4title")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("step4content")}</Text>
              </Grid>
            </Grid>{" "}
            {/**/}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
export default DeployWorkflow;

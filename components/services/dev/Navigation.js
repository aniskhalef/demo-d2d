import { useRouter } from "next/router";
import Image from "next/image";
import Link from "next/link";
import { Grid } from "@nextui-org/react";
const ServiceNavigation = ({ services, currentIndex, setCurrentIndex }) => {
  const router = useRouter();

  const navigateToService = (index) => {
    setCurrentIndex(index);
    router.push(services[index].link);
  };

  const goToPreviousService = () => {
    const newIndex = (currentIndex - 1 + services.length) % services.length;
    navigateToService(newIndex);
  };

  const goToNextService = () => {
    const newIndex = (currentIndex + 1) % services.length;
    navigateToService(newIndex);
  };

  return (
    <>
      <Grid className="left-side" css={{ position: "absolute" }}>
        <Grid
          css={{
            padding: 30,
            cursor: "pointer",
            position: "absolute",
            top: 0,
          }}
        >
          <Link href="/">
            <Image
              src={"/assets/icons/logo_white.svg"}
              alt="duetodata logo"
              width={80}
              height={80}
              objectFit="contain"
            />
          </Link>
        </Grid>
        <Grid
          style={{
            position: "fixed",
            bottom: "45%",
            left: "2em",
            padding: "10px",
            justifyContent: "space-between",
            backgroundImage: "url(/assets/icons/bg_arrows.svg)",
            backgroundSize: "100% 100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid css={{ cursor: "pointer" }}>
            <Image
              onClick={goToPreviousService}
              alt=""
              src={"/assets/icons/arrowleft.svg"}
              width={25}
              height={25}
              objectFit="cover"
            />
          </Grid>
          <Grid css={{ cursor: "pointer" }}>
            <Image
              onClick={goToNextService}
              alt=""
              src={"/assets/icons/arrowright.svg"}
              width={25}
              height={25}
              objectFit="cover"
            />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default ServiceNavigation;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";

import { useTranslation } from "next-i18next";
import Image from "next/image";
import WebWorkflow from "./webworflowCo";
import Link from "next/link";
const WebSlider = () => {
  const { t } = useTranslation("common");

  const frontendtechs = [
    { name: "Angular", image: "/assets/icons/dev/angular.svg" },
    { name: "React", image: "/assets/icons/dev/reactjs.svg" },
    { name: "Vue", image: "/assets/icons/dev/vuejs.svg" },
    { name: "HTML", image: "/assets/icons/dev/html.svg" },
    { name: "CSS", image: "/assets/icons/dev/css.svg" },
  ];
  const backendtechs = [
    { name: "Node.js", image: "/assets/icons/dev/nodejs.svg" },
    { name: "Python", image: "/assets/icons/dev/python.svg" },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("value")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_devweb_value_1")}
          </Text>
          <br></br>
          <Text span>{t("service_devweb_value_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_devweb_value_2")}
          </Text>
          <br></br>
          <Text span>{t("service_devweb_value_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_devweb_value_3")}
          </Text>
          <br></br>
          <Text span>{t("service_devweb_value_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_devweb_value_4")}
          </Text>
          <br></br>
          <Text span>{t("service_devweb_value_desc_4")}</Text>
        </Grid>
        <Grid>
          <WebWorkflow />
        </Grid>
        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>
        <br></br>
        <br></br>
        <Grid
          css={{
            margin: "auto",
            textAlign: "center",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid.Container>
            <Grid md={6} sm={12} xs={12} css={{ display: "inlin-block" }}>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  FRONTEND
                </Text>
              </Grid>
              <Grid.Container>
                {frontendtechs.map((frontendtech) => (
                  <Grid
                    key={frontendtech.name}
                    md={1}
                    sm={1}
                    xs={3}
                    className="techstack"
                  >
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={frontendtech.image}
                        alt={frontendtech.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {frontendtech.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>

            <Grid md={6} sm={12} xs={12}>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  BACKEND
                </Text>
              </Grid>
              <Grid></Grid>
              <Grid.Container>
                {backendtechs.map((backendtech) => (
                  <Grid
                    key={backendtech.name}
                    md={1}
                    sm={1}
                    xs={3}
                    className="techstack"
                  >
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={backendtech.image}
                        alt={backendtech.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {backendtech.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
          </Grid.Container>
        </Grid>
        <Grid css={{ marginTop: "3em", lineHeight: "2em" }}>
          <Text b size={"$lg"}>
            {t("service_web_title")}
          </Text>
          <br></br>
          <Text span>{t("service_web_desc_1")}</Text>
          <Link href={"/products/ellves"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_web_desc_link_1")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_web_desc_2")}</Text>
          <Link href={"/products/aweni"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_web_desc_link_2")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_web_desc_3")}</Text>
          <Link href={"/products/kitchensavvy"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_web_desc_link_3")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_web_desc_4")}</Text>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_devweb_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_devweb_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default WebSlider;

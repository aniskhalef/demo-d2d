import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";

import { useTranslation } from "next-i18next";
import Image from "next/image";
import BlockWorkflow from "./blockworkflowCo";
import Link from "next/link";

const BlockchainSlider = () => {
  const { t } = useTranslation("common");

  const languages = [
    { name: "Solidity", image: "/assets/icons/dev/solidity.svg" },
  ];
  const devs = [{ name: "Hardhat", image: "/assets/icons/dev/hardhat.svg" }];
  const networks = [
    { name: "Polygon", image: "/assets/icons/dev/polygon.svg" },
  ];

  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("value")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_blockchain_value_1")}
          </Text>
          <br></br>
          <Text span>{t("service_blockchain_value_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_blockchain_value_2")}
          </Text>
          <br></br>
          <Text span>{t("service_blockchain_value_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_blockchain_value_3")}
          </Text>
          <br></br>
          <Text span>{t("service_blockchain_value_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_blockchain_value_4")}
          </Text>
          <br></br>
          <Text span>{t("service_blockchain_value_desc_4")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_blockchain_value_5")}
          </Text>
          <br></br>
          <Text span>{t("service_blockchain_value_desc_5")}</Text>
        </Grid>
        <br></br>
        <Grid>
          <BlockWorkflow />
        </Grid>
        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <Grid
          css={{
            margin: "auto",
            textAlign: "center",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid.Container gap={4}>
            <Grid>
              <Grid>
                <Text
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  LANGUAGES
                </Text>
              </Grid>
              <Grid.Container
                css={{
                  justifyContent: "center",
                }}
              >
                {languages.map((languages) => (
                  <Grid key={languages.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Grid>
                        <Grid>
                          <Image
                            src={languages.image}
                            alt={languages.name}
                            width={50}
                            height={50}
                            objectFit="contain"
                          />
                        </Grid>

                        <Grid
                          css={{
                            position: "absolute",
                            bottom: -20,
                            left: 0,
                            width: "100%",
                            textAlign: "center",
                          }}
                        >
                          {languages.name}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
            <Grid>
              <Grid>
                <Text
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  {t("devenv")}
                </Text>
              </Grid>
              <Grid.Container
                css={{
                  justifyContent: "center",
                }}
              >
                {devs.map((dev) => (
                  <Grid key={dev.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={dev.image}
                        alt={dev.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {dev.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
            <Grid>
              <Grid>
                <Text
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  {t("network")}
                </Text>
              </Grid>
              <Grid.Container
                css={{
                  justifyContent: "center",
                }}
              >
                {networks.map((network) => (
                  <Grid key={network.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={network.image}
                        alt={network.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {network.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
          </Grid.Container>
        </Grid>
        <Grid css={{ marginTop: "3em", lineHeight: "2em" }}>
          <Text b size={"$lg"}>
            {t("service_blockchain_title")}
          </Text>
          <br></br>
          <Text span>{t("service_blockchain_desc_1")}</Text>
          <Link href={"/products/nftgalahub"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_blockchain_desc_link_1")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_blockchain_desc_2")}</Text>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_blockchain_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_blockchain_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default BlockchainSlider;

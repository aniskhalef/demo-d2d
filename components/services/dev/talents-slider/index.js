import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";

import { useTranslation } from "next-i18next";
import Link from "next/link";

const TalentsSlider = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("value")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_talents_value_1")}
          </Text>
          <br></br>
          <Text span>{t("service_talents_value_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_talents_value_2")}
          </Text>
          <br></br>
          <Text span>{t("service_talents_value_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_talents_value_3")}
          </Text>
          <br></br>
          <Text span>{t("service_talents_value_desc_3")}</Text>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_talents_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_talents_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default TalentsSlider;

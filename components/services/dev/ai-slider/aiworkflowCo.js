import React from "react";
import { Grid, Text } from "@nextui-org/react";
import { useTranslation } from "next-i18next";

const AiWorkflow = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid align="center" className="workflow">
        <Grid>
          <Text b size={"$2xl"}>
            {t("workflow")}
          </Text>
        </Grid>
        <Grid md={8}>
          <Grid>
            <Grid className="wrapper1">
              <Grid className="top">
                <Grid>
                  <Text span className={"number"}>
                    1
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("service_ai_flux_title_1")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("service_ai_flux_desc_1")}</Text>
              </Grid>
            </Grid>
            {/**/}
            <Grid className={"wrapper2"}>
              <Grid className={"top"}>
                <Grid>
                  <Text span className={"number"}>
                    2
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("service_ai_flux_title_2")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("service_ai_flux_desc_2")}</Text>
              </Grid>
            </Grid>
            {/**/}
            <Grid className={"wrapper3"}>
              <Grid className={"top"}>
                <Grid>
                  <Text span className={"number"}>
                    3
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("service_ai_flux_title_3")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("service_ai_flux_desc_3")}</Text>
              </Grid>
            </Grid>
            {/**/}
            <Grid className={"wrapper4"}>
              <Grid className={"top"}>
                <Grid>
                  <Text span className={"number"}>
                    4
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("service_ai_flux_title_4")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("service_ai_flux_desc_4")}</Text>
              </Grid>
            </Grid>{" "}
            {/**/}
            <Grid className={"wrapper5"}>
              <Grid className={"top"}>
                <Grid>
                  <Text span className={"number"}>
                    5
                  </Text>
                </Grid>
                <Grid>
                  <Text span className={"contentTitle"}>
                    {t("service_ai_flux_title_5")}
                  </Text>
                </Grid>
              </Grid>
              <Grid>
                <Text span>{t("service_ai_flux_desc_5")}</Text>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
export default AiWorkflow;

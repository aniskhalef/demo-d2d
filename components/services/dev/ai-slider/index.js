import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";

import { useTranslation } from "next-i18next";
import Image from "next/image";
import AiWorkflow from "./aiworkflowCo";
import Link from "next/link";

const AiSlider = () => {
  const { t } = useTranslation("common");

  const languages = [
    { name: "JavaScript", image: "/assets/icons/dev/js.svg" },
    { name: "Python", image: "/assets/icons/dev/python.svg" },
  ];
  const frameworks = [
    { name: "OpenCV", image: "/assets/icons/dev/opencv.svg" },
    { name: "", image: "/assets/icons/dev/groupaitools.png" },
  ];
  const platforms = [
    { name: "Google Cloud AI", image: "/assets/icons/dev/googlecloud.svg" },
    { name: "Jasper", image: "/assets/icons/dev/jasper.svg" },
  ];
  const tools = [
    { name: "Git", image: "/assets/icons/dev/git.svg" },
    { name: "Docker", image: "/assets/icons/dev/docker.svg" },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("value")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_ai_value_1")}
          </Text>
          <br></br>
          <Text span>{t("service_ai_value_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_ai_value_2")}
          </Text>
          <br></br>
          <Text span>{t("service_ai_value_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_ai_value_3")}
          </Text>
          <br></br>
          <Text span>{t("service_ai_value_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_ai_value_4")}
          </Text>
          <br></br>
          <Text span>{t("service_ai_value_desc_4")}</Text>
        </Grid>
        <Grid>
          <AiWorkflow />
        </Grid>
        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <Grid
          css={{
            margin: "auto",
            textAlign: "center",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid.Container>
            <Grid md={4} sm={12} xs={12} css={{ display: "inlin-block" }}>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  LANGUAGES
                </Text>
              </Grid>
              <Grid.Container>
                {languages.map((languages) => (
                  <Grid key={languages.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={languages.image}
                        alt={languages.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {languages.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>

            <Grid md={4} sm={12} xs={12}>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  FRAMEWORKS
                </Text>
              </Grid>
              <Grid.Container>
                {frameworks.map((framework) => (
                  <Grid key={framework.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Grid>
                        <Image
                          src={framework.image}
                          alt={framework.name}
                          width={50}
                          height={50}
                          objectFit="contain"
                        />
                      </Grid>

                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {framework.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
            <Grid md={4} sm={12} xs={12}>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  PLATFORMS
                </Text>
              </Grid>
              <Grid.Container>
                {platforms.map((platforme) => (
                  <Grid key={platforme.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={platforme.image}
                        alt={platforme.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {platforme.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>

            <Grid md={4} sm={12} xs={12}>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  Tools
                </Text>
              </Grid>
              <Grid.Container>
                {tools.map((tool) => (
                  <Grid key={tool.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={tool.image}
                        alt={tool.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {tool.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
          </Grid.Container>
        </Grid>
        <Grid css={{ marginTop: "3em", lineHeight: "2em" }}>
          <Text b size={"$lg"}>
            {t("service_ia_title")}
          </Text>
          <br></br>
          <Text span>{t("service_ia_desc_1")}</Text>
          <Link href={"/products/irukai"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_ia_desc_link_1")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_ia_desc_2")}</Text>
          <Link href={"/products/biocheckai"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_ia_desc_link_2")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_ia_desc_3")}</Text>
          <Link href={"/products/observermaster"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_ia_desc_link_3")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_ia_desc_4")}</Text>
          <Link href={"/products/audio2text"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_ia_desc_link_4")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_ia_desc_5")}</Text>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_ai_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_ai_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default AiSlider;

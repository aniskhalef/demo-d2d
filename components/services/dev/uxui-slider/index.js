import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";

import { useTranslation } from "next-i18next";
import Image from "next/image";
import UxUiWorkFlow from "./uxuiworkflowCo";
import Link from "next/link";

const UxUiSlider = () => {
  const { t } = useTranslation("common");

  const softwares = [
    { name: "Adobe Photoshop", image: "/assets/icons/dev/ps.svg" },
    { name: "Adobe Illustrator", image: "/assets/icons/dev/illustrator.svg" },
    { name: "Adobe XD", image: "/assets/icons/dev/xd.svg" },
  ];

  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("value")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_uxui_value_1")}
          </Text>
          <br></br>
          <Text span>{t("service_uxui_value_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_uxui_value_2")}
          </Text>
          <br></br>
          <Text span>{t("service_uxui_value_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_uxui_value_3")}
          </Text>
          <br></br>
          <Text span>{t("service_uxui_value_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_uxui_value_4")}
          </Text>
          <br></br>
          <Text span>{t("service_uxui_value_desc_4")}</Text>
        </Grid>
        <Grid>
          <UxUiWorkFlow />
        </Grid>
        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid>
            <Grid>
              <Text
                span
                size={"$lg"}
                b
                css={{
                  borderBottom: "2px solid #DE4F0A",
                }}
              >
                Software
              </Text>
            </Grid>
            <Grid.Container>
              {softwares.map((software) => (
                <Grid
                  key={software.name}
                  md={1}
                  sm={1}
                  xs={3}
                  className="techstack"
                >
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={software.image}
                      alt={software.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />
                    <Grid
                      css={{
                        position: "absolute",
                        bottom: -50,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {software.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_uxui_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_uxui_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};
export default UxUiSlider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";

import { useTranslation } from "next-i18next";
import Image from "next/image";
import MobileWorkFlow from "./mobilworkflowCo";
import Link from "next/link";

const MobileSlider = () => {
  const { t } = useTranslation("common");

  const techs = [
    { name: "React Native", image: "/assets/icons/dev/reactnative.svg" },
    { name: "React", image: "/assets/icons/dev/android.svg" },
    { name: "Vue", image: "/assets/icons/dev/ios.svg" },
  ];
  const tools = [
    { name: "Git", image: "/assets/icons/dev/git.svg" },
    { name: "Docker", image: "/assets/icons/dev/docker.svg" },
    { name: "Kubernetes", image: "/assets/icons/dev/kubernetes.svg" },
    { name: "Jest", image: "/assets/icons/dev/jest.svg" },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("value")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_devmobile_value_1")}
          </Text>
          <br></br>
          <Text span>{t("service_devmobile_value_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_devmobile_value_2")}
          </Text>
          <br></br>
          <Text span>{t("service_devmobile_value_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_devmobile_value_3")}
          </Text>
          <br></br>
          <Text span>{t("service_devmobile_value_desc_3")}</Text>
          <br></br>
        </Grid>
        <Grid>
          <MobileWorkFlow />
        </Grid>
        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid md={6} sm={12} xs={12}>
            <Grid.Container>
              {techs.map((tech) => (
                <Grid key={tech.name} md={3} sm={2} xs={3} className="tech">
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={tech.image}
                      alt={tech.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />
                    <Grid
                      css={{
                        position: "absolute",
                        bottom: -20,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {tech.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
          <br></br>
          <br></br>
          <br></br>
          <Grid md={6} sm={12} xs={12}>
            <Grid>
              <Text
                span
                size={"$lg"}
                b
                css={{
                  borderBottom: "2px solid #DE4F0A",
                }}
              >
                Tools
              </Text>
            </Grid>

            <Grid.Container>
              {tools.map((tool) => (
                <Grid
                  key={tool.name}
                  md={2}
                  sm={1}
                  xs={3}
                  className="techstack"
                >
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={tool.image}
                      alt={tool.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />
                    <Grid
                      css={{
                        position: "absolute",
                        bottom: -20,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {tool.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <Grid css={{ marginTop: "3em", lineHeight: "2em" }}>
          <Text b size={"$lg"}>
            {t("service_mobile_title")}
          </Text>
          <br></br>
          <Text span>{t("service_mobile_desc_1")}</Text>
          <Link href={"/products/ellves"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_mobile_desc_link_1")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_mobile_desc_2")}</Text>
          <Link href={"/products/aweni"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_mobile_desc_link_2")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_mobile_desc_3")}</Text>
          <Link href={"/products/kitchensavvy"}>
            <Text b css={{ cursor: "pointer" }}>
              {t("service_mobile_desc_link_3")}{" "}
            </Text>
          </Link>
          <Text span>{t("service_mobile_desc_4")}</Text>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_devmobile_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_devmobile_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default MobileSlider;

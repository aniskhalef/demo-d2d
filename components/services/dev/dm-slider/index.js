import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";

import { useTranslation } from "next-i18next";
import Image from "next/image";
import DmWorkFlow from "./dmworkflowCo";
import Link from "next/link";

const DmSlider = () => {
  const { t } = useTranslation("common");

  const tools = [
    { name: "Meta Ads manager", image: "/assets/icons/dev/meta.svg" },
    { name: "Linkedin ads", image: "/assets/icons/dev/linkedin.svg" },
    { name: "Google Analytics", image: "/assets/icons/dev/ga.svg" },
    { name: "Google Search Console", image: "/assets/icons/dev/gsc.svg" },
    { name: "Google ads", image: "/assets/icons/dev/googleads.svg" },
    { name: "Hootsuite", image: "/assets/icons/dev/hootsuite.svg" },
    { name: "Semrush", image: "/assets/icons/dev/semrush.svg" },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("value")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_marketing_value_1")}
          </Text>
          <br></br>
          <Text span>{t("service_marketing_value_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_marketing_value_2")}
          </Text>
          <br></br>
          <Text span>{t("service_marketing_value_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_marketing_value_3")}
          </Text>
          <br></br>
          <Text span>{t("service_marketing_value_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_marketing_value_4")}
          </Text>
          <br></br>
          <Text span>{t("service_marketing_value_desc_4")}</Text>
        </Grid>
        <Grid>
          <DmWorkFlow />
        </Grid>

        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <br></br>
          <Grid md={10} sm={12} xs={12}>
            <Grid>
              <Text
                span
                size={"$lg"}
                b
                css={{
                  borderBottom: "2px solid #DE4F0A",
                }}
              >
                SOFTWARE
              </Text>
            </Grid>
            <br></br>

            <Grid.Container>
              {tools.map((tool) => (
                <Grid key={tool.name} className="techstack">
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={tool.image}
                      alt={tool.name}
                      width={50}
                      height={50}
                    />
                    <Grid
                      css={{
                        position: "absolute",
                        bottom: -80,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {tool.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <br></br>
        <br></br>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_marketing_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_marketing_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default DmSlider;

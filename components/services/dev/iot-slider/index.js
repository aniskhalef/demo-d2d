import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";

import { useTranslation } from "next-i18next";
import Image from "next/image";
import IotWorkFlow from "./iotworkflowCo";
import Link from "next/link";

const IotSlider = () => {
  const { t } = useTranslation("common");

  const languages = [
    { name: "Java", image: "/assets/icons/dev/java.svg" },
    { name: "JavaScript", image: "/assets/icons/dev/js.svg" },
    { name: "Python", image: "/assets/icons/dev/python.svg" },
  ];

  const platforms = [
    { name: "Tensor Flow", image: "/assets/icons/dev/tensorflow.png" },
    { name: "Google Colab", image: "/assets/icons/dev/python.svg" },
    { name: "Jupiter Notebook", image: "/assets/icons/dev/jupyter.svg" },
  ];
  const captors = [
    { name: "Sound", image: "/assets/icons/dev/sound.svg" },
    {
      name: "Temperature",
      image: "/assets/icons/dev/temperature.svg",
    },
    {
      name: "Humidity",
      image: "/assets/icons/dev/humidity.svg",
    },
    {
      name: "Mouvment",
      image: "/assets/icons/dev/mouvement.svg",
    },
    {
      name: "Gaz",
      image: "/assets/icons/dev/gaz.svg",
    },
    {
      name: "Light",
      image: "/assets/icons/dev/light.svg",
    },
    {
      name: "Vibration",
      image: "/assets/icons/dev/vibrations.svg",
    },
  ];
  const communications = [
    { name: "Bluetooth", image: "/assets/icons/dev/bluetooth.svg" },
    {
      name: "Wifi",
      image: "/assets/icons/dev/wifi.svg",
    },
    {
      name: "Lora",
      image: "/assets/icons/dev/lorawan.svg",
    },
    {
      name: "Rfid",
      image: "/assets/icons/dev/Rfid.svg",
    },
  ];
  const cloudplatforms = [
    { name: "Azure", image: "/assets/icons/dev/azure.svg" },
    { name: "AWS", image: "/assets/icons/dev/aws.svg" },
    {
      name: "Google Cloud platform",
      image: "/assets/icons/dev/googlecloud.svg",
    },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("value")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_iot_value_1")}
          </Text>
          <br></br>
          <Text span>{t("service_iot_value_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_iot_value_2")}
          </Text>
          <br></br>
          <Text span>{t("service_iot_value_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_iot_value_3")}
          </Text>
          <br></br>
          <Text span>{t("service_iot_value_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_iot_value_4")}
          </Text>
          <br></br>
          <Text span>{t("service_iot_value_desc_4")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("service_iot_value_5")}
          </Text>
          <br></br>
          <Text span>{t("service_iot_value_desc_5")}</Text>
        </Grid>
        <br></br>
        <Grid>
          <IotWorkFlow />
        </Grid>
        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid.Container>
            <Grid css={{ marginRight: "8em" }}>
              <Grid>
                <Text
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  LANGUAGES
                </Text>
              </Grid>
              <Grid.Container gap={2}>
                {languages.map((languages) => (
                  <Grid key={languages.name} className="techstack " md={3}>
                    <Grid
                      css={{
                        position: "relative",
                        justifyContent: "center",
                        textAlign: "center",
                      }}
                    >
                      <Image
                        src={languages.image}
                        alt={languages.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -40,
                          left: 0,
                          width: "100%",
                        }}
                      >
                        {languages.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>

            <Grid>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  PLATFORMS
                </Text>
              </Grid>
              <Grid.Container gap={2}>
                {platforms.map((platform) => (
                  <Grid key={languages.name} className="techstack" md={3}>
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={platform.image}
                        alt={platform.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -50,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {platform.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
          </Grid.Container>

          <Grid xs={12}>
            <Grid>
              <Text
                span
                size={"$lg"}
                b
                css={{
                  borderBottom: "2px solid #DE4F0A",
                }}
              >
                {t("captors")}
              </Text>
            </Grid>
            <Grid.Container>
              {captors.map((captor) => (
                <Grid
                  key={captor.name}
                  className="techstack"
                  css={{
                    position: "relative",
                    justifyContent: "center",
                    textAlign: "center",
                  }}
                >
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={captor.image}
                      alt={captor.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />
                    <Grid
                      css={{
                        position: "absolute",
                        bottom: -35,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {captor.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
          <br></br>
          <Grid xs={12}>
            <Grid>
              <Text
                size={"$lg"}
                b
                css={{
                  borderBottom: "2px solid #DE4F0A",
                }}
              >
                COMMUNICATIONS
              </Text>
            </Grid>
            <Grid.Container>
              {communications.map((communication) => (
                <Grid
                  key={communication.name}
                  md={1}
                  sm={1}
                  xs={3}
                  className="techstack"
                >
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={communication.image}
                      alt={communication.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />
                    <Grid
                      css={{
                        position: "absolute",
                        bottom: -20,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {communication.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
          <br></br>
          <Grid xs={12}>
            <Grid>
              <Text
                span
                size={"$lg"}
                b
                css={{
                  borderBottom: "2px solid #DE4F0A",
                }}
              >
                CLOUD PLATFORMS
              </Text>
            </Grid>
            <Grid.Container>
              {cloudplatforms.map((cloudplatform) => (
                <Grid key={cloudplatform.name} className="techstack">
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={cloudplatform.image}
                      alt={cloudplatform.name}
                      width={70}
                      height={130}
                      objectFit="contain"
                    />
                    <Grid
                      css={{
                        position: "absolute",
                        bottom: -40,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {cloudplatform.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("service_iot_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("service_iot_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default IotSlider;

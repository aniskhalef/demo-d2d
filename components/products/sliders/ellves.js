import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const EllvesSlider = () => {
  const { t } = useTranslation("common");

  const techs = [
    { name: "Angular", image: "/assets/icons/dev/angular.svg" },
    { name: "Node JS", image: "/assets/icons/dev/nodejs.svg" },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("keyfeatures")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("ellves_keyfeature_1")}
          </Text>
          <br></br>
          <Text span>{t("ellves_keyfeature_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("ellves_keyfeature_2")}
          </Text>
          <br></br>
          <Text span>{t("ellves_keyfeature_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("ellves_keyfeature_3")}
          </Text>
          <br></br>
          <Text span>{t("ellves_keyfeature_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("ellves_keyfeature_4")}
          </Text>
          <br></br>
          <Text span>{t("ellves_keyfeature_desc_4")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("ellves_keyfeature_5")}
          </Text>
          <br></br>
          <Text span>{t("ellves_keyfeature_desc_5")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("ellves_keyfeature_6")}
          </Text>
          <br></br>

          <Text span>{t("ellves_keyfeature_desc_6_1")}</Text>
          <br></br>
          <Text span>{t("ellves_keyfeature_desc_6_2")}</Text>
          <br></br>
          <Grid>
            <Image
              src="/assets/products/ellves.png"
              alt="mockup ellves"
              height={300}
              width={400}
              objectFit="contain"
            />
          </Grid>
        </Grid>

        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid md={6} sm={12} xs={12}>
            <Grid.Container>
              {techs.map((tech) => (
                <Grid key={tech.name} md={6} sm={6} xs={6} className="tech">
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={tech.image}
                      alt={tech.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />

                    <Grid
                      css={{
                        bottom: -40,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {tech.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("ellves_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("ellves_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default EllvesSlider;

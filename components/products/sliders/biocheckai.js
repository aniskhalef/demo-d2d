import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const BioCheckAiSlider = () => {
  const { t } = useTranslation("common");

  const languages = [{ name: "Python", image: "/assets/icons/dev/python.svg" }];
  const libraries = [
    { name: "Tensor Flow", image: "/assets/icons/dev/tensorflow.png" },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("keyfeatures")}
          </Text>

          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("biocheckai_keyfeature_1")}
          </Text>
          <br></br>
          <Text span>{t("biocheckai_keyfeature_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("biocheckai_keyfeature_2")}
          </Text>
          <br></br>
          <Text span>{t("biocheckai_keyfeature_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("biocheckai_keyfeature_3")}
          </Text>
          <br></br>
          <Text span>{t("biocheckai_keyfeature_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("biocheckai_keyfeature_4")}
          </Text>
          <br></br>
          <Text span>{t("biocheckai_keyfeature_desc_4")}</Text>
          <br></br>
          <br></br>
          <br></br>
          <Grid.Container>
            <Grid>
              <Image
                src="/assets/products/biocheckai1.png"
                alt="mockup biocheckai"
                height={300}
                width={400}
                objectFit="contain"
              />
            </Grid>
            <Grid>
              <Image
                src="/assets/products/biocheckai2.png"
                alt="mockup biocheckai"
                height={300}
                width={400}
                objectFit="cover"
              />
            </Grid>
          </Grid.Container>
        </Grid>
        <br></br>
        <br></br>
        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid.Container>
            <Grid md={4} sm={4} xs={4} align="center">
              <Grid>
                <Grid>
                  <Text
                    size={"$lg"}
                    b
                    css={{
                      borderBottom: "2px solid #DE4F0A",
                    }}
                  >
                    LANGUAGES
                  </Text>
                </Grid>
                <br></br>
                <Grid>
                  {languages.map((languages) => (
                    <Grid key={languages.name} className="techstack">
                      <Grid css={{ position: "relative" }}>
                        <Image
                          src={languages.image}
                          alt={languages.name}
                          width={50}
                          height={50}
                          objectFit="contain"
                        />
                        <Grid
                          css={{
                            position: "absolute",
                            bottom: -30,
                            left: 0,
                            width: "100%",
                            textAlign: "center",
                          }}
                        >
                          {languages.name}
                        </Grid>
                      </Grid>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>

            <Grid md={4} sm={4} xs={4} align="center">
              <Grid>
                <Grid>
                  <Text
                    size={"$lg"}
                    b
                    css={{
                      borderBottom: "2px solid #DE4F0A",
                    }}
                  >
                    {t("libraries")}
                  </Text>
                </Grid>
                <br></br>
                <Grid>
                  {libraries.map((librarie) => (
                    <Grid key={librarie.name} className="techstack">
                      <Grid css={{ position: "relative" }}>
                        <Image
                          src={librarie.image}
                          alt={librarie.name}
                          width={50}
                          height={50}
                          objectFit="contain"
                        />
                        <Grid
                          css={{
                            position: "absolute",
                            bottom: -50,
                            left: 0,
                            width: "100%",
                            textAlign: "center",
                          }}
                        >
                          {librarie.name}
                        </Grid>
                      </Grid>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>
          </Grid.Container>
        </Grid>
        <br></br>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("biocheckai_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("biocheckai_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default BioCheckAiSlider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const IrukaiSlider = () => {
  const { t } = useTranslation("common");

  const techs = [
    { name: "Angular", image: "/assets/icons/dev/angular.svg" },
    { name: "Node JS", image: "/assets/icons/dev/nodejs.svg" },
    { name: "MongoDB", image: "/assets/icons/dev/mongo.svg" },
    { name: "Express JS", image: "/assets/icons/dev/express.svg" },
  ];

  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("keyfeatures")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("irukai_keyfeature_1")}
          </Text>
          <br></br>
          <Text span>{t("irukai_keyfeature_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$lg"}>
            {t("irukai_keyfeature_2")}
          </Text>
          <br></br>
          <Text span>{t("irukai_keyfeature_desc_2")}</Text>
          <br></br>
          <br></br>
          <Grid.Container gap={4}>
            <Grid>
              <Image
                src="/assets/products/irukai1.png"
                alt="mockup irukai"
                height={300}
                width={400}
                objectFit="cover"
              />
            </Grid>
            <Grid>
              <Image
                src="/assets/products/irukai2.png"
                alt="mockup irukai"
                height={300}
                width={400}
                objectFit="cover"
              />
            </Grid>
          </Grid.Container>
        </Grid>
        <br></br>

        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid md={6} sm={12} xs={12}>
            <Grid.Container>
              {techs.map((tech) => (
                <Grid key={tech.name} md={3} sm={3} xs={3} className="tech">
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={tech.image}
                      alt={tech.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />

                    <Grid
                      css={{
                        bottom: -40,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {tech.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <br></br>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("irukai_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("irukai_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>{" "}
          </Button>
        </Grid>
      </Grid>
    </>
  );
};
export default IrukaiSlider;

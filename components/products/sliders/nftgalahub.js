import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const NftGalaHubSlider = () => {
  const { t } = useTranslation("common");

  const techs = [
    { name: "Node JS", image: "/assets/icons/dev/nodejs.svg" },
    { name: "Next JS", image: "/assets/icons/dev/nextjs.svg" },
  ];

  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("keyfeatures")}
          </Text>

          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("nftgalahub_keyfeature_1")}
          </Text>
          <br></br>
          <Text span>
            <ul>
              <li> {t("nftgalahub_keyfeature_desc_1_1")}</li>
              <li> {t("nftgalahub_keyfeature_desc_1_2")}</li>
            </ul>
          </Text>
          <br></br>
          <Text b size={"$md"}>
            {t("nftgalahub_keyfeature_2")}
          </Text>
          <br></br>
          <Text span>
            <ul>
              <li> {t("nftgalahub_keyfeature_desc_2_1")}</li>
              <li> {t("nftgalahub_keyfeature_desc_2_1")}</li>
            </ul>
          </Text>
          <br></br>
          <Text b size={"$md"}>
            {t("nftgalahub_keyfeature_3")}
          </Text>
          <br></br>
          <Text span>
            <ul>
              <li> {t("nftgalahub_keyfeature_desc_3_1")}</li>
              <li> {t("nftgalahub_keyfeature_desc_3_1")}</li>
              <li> {t("nftgalahub_keyfeature_desc_3_1")}</li>
            </ul>
          </Text>
          <br></br>
        </Grid>

        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid md={6} sm={12} xs={12}>
            <Grid.Container>
              {techs.map((tech) => (
                <Grid key={tech.name} md={6} sm={6} xs={6} className="tech">
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={tech.image}
                      alt={tech.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />

                    <Grid
                      css={{
                        bottom: -40,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {tech.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("nftgalahub_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("nftgalahub_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default NftGalaHubSlider;

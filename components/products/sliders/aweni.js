import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const AweniSlider = () => {
  const { t } = useTranslation("common");

  const techs = [
    { name: "Node JS", image: "/assets/icons/dev/nodejs.svg" },
    { name: "Angular", image: "/assets/icons/dev/angular.svg" },
  ];

  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("keyfeatures")}
          </Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("aweni_keyfeature_1")}
          </Text>
          <br></br>
          <Text span>{t("aweni_keyfeature_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("aweni_keyfeature_2")}
          </Text>
          <br></br>
          <Text span>
            <ul>
              <li>{t("aweni_keyfeature_desc_2_1")}</li>
              <li>{t("aweni_keyfeature_desc_2_2")}</li>
              <li>{t("aweni_keyfeature_desc_2_3")}</li>
              <li>{t("aweni_keyfeature_desc_2_4")}</li>
            </ul>
          </Text>
          <br></br>
          <Text b size={"$md"}>
            {t("aweni_keyfeature_3")}
          </Text>
          <br></br>
          <Text span>
            <ul>
              <li>{t("aweni_keyfeature_desc_3_1")}</li>
              <li>{t("aweni_keyfeature_desc_3_2")}</li>
            </ul>
          </Text>
          <br></br>
          <br></br>
          <Grid.Container className="container-">
            <Grid>
              <Image
                src="/assets/products/aaweni2.png"
                alt="mockup aaweni"
                height={280}
                width={500}
                objectFit="contain"
              />
            </Grid>
            <Grid>
              <Image
                src="/assets/products/aaweni1.png"
                alt="mockup aaweni"
                height={290}
                width={400}
                objectFit="contain"
              />
            </Grid>
          </Grid.Container>
        </Grid>
        <br></br>
        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid md={6} sm={12} xs={12}>
            <Grid.Container>
              {techs.map((tech) => (
                <Grid key={tech.name} md={3} sm={1} xs={12} className="tech">
                  <Grid css={{ position: "relative" }}>
                    <Image
                      src={tech.image}
                      alt={tech.name}
                      width={50}
                      height={50}
                      objectFit="contain"
                    />

                    <Grid
                      css={{
                        bottom: -40,
                        left: 0,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      {tech.name}
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("aweni_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("aweni_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default AweniSlider;

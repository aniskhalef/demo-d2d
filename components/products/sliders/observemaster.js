import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const ObserveMasterSlider = () => {
  const { t } = useTranslation("common");

  const languages = [{ name: "Python", image: "/assets/icons/dev/python.svg" }];
  const frameworks = [{ name: "Flask", image: "/assets/icons/dev/flask.svg" }];
  const models = [{ name: "YOLO v5", image: "/assets/icons/dev/yolo.svg" }];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("keyfeatures")}
          </Text>

          <br></br>
          <br></br>
          <Text span size={"$md"}>
            {t("observermaster_keyfeature_1")}
          </Text>
          <br></br>
          <Text span>{t("observermaster_keyfeature_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text span size={"$md"}>
            {t("observermaster_keyfeature_2")}
          </Text>
          <br></br>
          <Text span>{t("observermaster_keyfeature_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text span size={"$md"}>
            {t("observermaster_keyfeature_3")}
          </Text>
          <br></br>
          <Text span>{t("observermaster_keyfeature_desc_3")}</Text>
          <br></br>
          <br></br>
          <Text span size={"$md"}>
            {t("observermaster_keyfeature_4")}
          </Text>
          <br></br>
          <Text span>{t("observermaster_keyfeature_desc_4")}</Text>
          <br></br>
          <br></br>
          <Text span size={"$md"}>
            {t("observermaster_keyfeature_5")}
          </Text>
          <br></br>
          <Text span>{t("observermaster_keyfeature_desc_5")}</Text>
        </Grid>
        <br></br>
        <br></br>

        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid.Container>
            <Grid md={4} sm={12} xs={12} css={{ display: "inlin-block" }}>
              <Grid>
                <Text
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  LANGUAGES
                </Text>
              </Grid>
              <Grid.Container>
                {languages.map((languages) => (
                  <Grid key={languages.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={languages.image}
                        alt={languages.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {languages.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>

            <Grid md={3} sm={12} xs={12}>
              <Grid>
                <Text
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  FRAMEWORKS
                </Text>
              </Grid>
              <Grid></Grid>
              <Grid.Container>
                {frameworks.map((framework) => (
                  <Grid key={framework.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={framework.image}
                        alt={framework.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {framework.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
            <Grid md={5} sm={12} xs={12}>
              <Grid>
                <Text
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  MODELS
                </Text>
              </Grid>

              <Grid.Container>
                {models.map((model) => (
                  <Grid key={model.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={model.image}
                        alt={model.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {model.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid.Container>
            </Grid>
          </Grid.Container>
        </Grid>
        <br></br>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("observermaster_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("observermaster_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default ObserveMasterSlider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const WpClickSlider = () => {
  const { t } = useTranslation("common");

  const languages = [{ name: "PHP", image: "/assets/icons/dev/php.svg" }];
  const platforms = [
    { name: "WOOCOMMERCE", image: "/assets/icons/dev/woocomerce.png" },
  ];
  return (
    <>
      <Grid>
        <Grid>
          <Text b size={"$lg"}>
            {t("keyfeatures")}
          </Text>

          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("click2pay_keyfeature_1")}
          </Text>
          <br></br>
          <Text span>{t("click2pay_keyfeature_desc_1")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("click2pay_keyfeature_2")}
          </Text>
          <br></br>
          <Text span>{t("click2pay_keyfeature_desc_2")}</Text>
          <br></br>
          <br></br>
          <Text b size={"$md"}>
            {t("click2pay_keyfeature_3")}
          </Text>
          <br></br>
          <Text span>{t("click2pay_keyfeature_desc_3")}</Text>
        </Grid>
        <br></br>
        <br></br>

        <Grid>
          <Text b size={"$lg"} css={{ textTransform: "uppercase" }}>
            {t("tools")}
          </Text>
        </Grid>

        <br></br>
        <Grid
          css={{
            margin: "auto",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid.Container>
            <Grid md={4} sm={12} xs={12} align="center">
              <Grid>
                <Grid>
                  <Text
                    span
                    size={"$lg"}
                    b
                    css={{
                      borderBottom: "2px solid #DE4F0A",
                    }}
                  >
                    LANGUAGES
                  </Text>
                </Grid>
                <br></br>

                <Grid>
                  {languages.map((languages) => (
                    <Grid key={languages.name} className="techstack">
                      <Grid css={{ position: "relative" }}>
                        <Image
                          src={languages.image}
                          alt={languages.name}
                          width={50}
                          height={50}
                          objectFit="contain"
                        />
                        <Grid
                          css={{
                            position: "absolute",
                            bottom: -20,
                            left: 0,
                            width: "100%",
                            textAlign: "center",
                          }}
                        >
                          {languages.name}
                        </Grid>
                      </Grid>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>

            <Grid md={4} sm={12} xs={12} align="center">
              <Grid>
                <Grid>
                  <Text
                    span
                    size={"$lg"}
                    b
                    css={{
                      borderBottom: "2px solid #DE4F0A",
                    }}
                  >
                    PLATFORMS
                  </Text>
                </Grid>
                <Grid>
                  {platforms.map((platform) => (
                    <Grid key={platform.name} className="techstack">
                      <Grid css={{ position: "relative" }}>
                        <Image
                          src={platform.image}
                          alt={platform.name}
                          width={80}
                          height={80}
                          objectFit="contain"
                        />
                        <Grid
                          css={{
                            position: "absolute",
                            bottom: -20,
                            left: 0,
                            width: "100%",
                            textAlign: "center",
                          }}
                        >
                          {platform.name}
                        </Grid>
                      </Grid>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>
          </Grid.Container>
        </Grid>
        <Grid className="contact-div">
          <Text b size={"$lg"}>
            {t("click2pay_cta_1")}
          </Text>
          <br></br>
          <Text span>{t("click2pay_cta_2")}</Text>
          <br></br>
          <br></br>
          <Button className="contact-button">
            <Link href="/contact-us">
              <Text b className="text-white">
                {t("header2")}
              </Text>
            </Link>
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default WpClickSlider;

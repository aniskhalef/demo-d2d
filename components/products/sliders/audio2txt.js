import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const Audi2TxtSlider = () => {
  const { t } = useTranslation("common");

  const languages = [{ name: "Python", image: "/assets/icons/dev/python.svg" }];
  const frameworks = [{ name: "", image: "/assets/icons/dev/audio_ai.svg" }];
  const models = [{ name: "", image: "/assets/icons/dev/speech_ai.svg" }];
  return (
    <>
      <Grid>
        <Text b size={"$lg"}>
          {t("keyfeatures")}
        </Text>

        <br></br>
        <br></br>
        <Text b size={"$md"}>
          {t("audio2txt_keyfeature_1")}
        </Text>
        <br></br>
        <Text span>{t("audio2txt_keyfeature_desc_1")}</Text>
        <br></br>
        <br></br>
        <Text b size={"$md"}>
          {t("audio2txt_keyfeature_2")}
        </Text>
        <br></br>
        <Text span>{t("audio2txt_keyfeature_desc_2")}</Text>
        <br></br>
        <br></br>
        <Text b size={"$md"}>
          {t("audio2txt_keyfeature_3")}
        </Text>
        <br></br>
        <Text span>{t("audio2txt_keyfeature_desc_3")}</Text>
        <br></br>
        <br></br>
        <Text b size={"$md"}>
          {t("audio2txt_keyfeature_4")}
        </Text>
        <br></br>
        <Text span>{t("audio2txt_keyfeature_desc_4")}</Text>
        <br></br>
        <br></br>
        <Text b size={"$md"}>
          {t("audio2txt_keyfeature_5")}
        </Text>
        <br></br>
        <Text span>{t("audio2txt_keyfeature_desc_5")}</Text>
      </Grid>
      <br></br>
      <br></br>

      <Grid>
        <Text b size={"$lg"}>
          {t("tools")}
        </Text>
      </Grid>

      <br></br>
      <br></br>
      <Grid
        css={{
          margin: "auto",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Grid.Container>
          <Grid md={3} sm={4} xs={6} align="center">
            <Grid>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  LANGUAGES
                </Text>
              </Grid>
              <br></br>
              <Grid>
                {languages.map((languages) => (
                  <Grid key={languages.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={languages.image}
                        alt={languages.name}
                        width={50}
                        height={50}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {languages.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>

          <Grid md={3} sm={4} xs={6} align="center">
            <Grid>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  PYTHON PACKAGES
                </Text>
              </Grid>
              <br></br>
              <Grid>
                {frameworks.map((framework) => (
                  <Grid key={framework.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={framework.image}
                        alt={framework.name}
                        width={100}
                        height={100}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {framework.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>

          <Grid md={3} sm={4} xs={6} align="center">
            <Grid>
              <Grid>
                <Text
                  span
                  size={"$lg"}
                  b
                  css={{
                    borderBottom: "2px solid #DE4F0A",
                  }}
                >
                  {t("libraries")}
                </Text>
              </Grid>
              <br></br>
              <Grid>
                {models.map((model) => (
                  <Grid key={model.name} className="techstack">
                    <Grid css={{ position: "relative" }}>
                      <Image
                        src={model.image}
                        alt={model.name}
                        width={150}
                        height={100}
                        objectFit="contain"
                      />
                      <Grid
                        css={{
                          position: "absolute",
                          bottom: -20,
                          left: 0,
                          width: "100%",
                          textAlign: "center",
                        }}
                      >
                        {model.name}
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>
        </Grid.Container>
      </Grid>
      <br></br>
      <Grid className="contact-div">
        <Text b size={"$lg"}>
          {t("audio2txt_cta_1")}
        </Text>
        <br></br>
        <Text span>{t("audio2txt_cta_2")}</Text>
        <br></br>
        <br></br>
        <Button className="contact-button">
          <Link href="/contact-us">
            <Text b className="text-white">
              {t("header2")}
            </Text>
          </Link>
        </Button>
      </Grid>
    </>
  );
};
export default Audi2TxtSlider;

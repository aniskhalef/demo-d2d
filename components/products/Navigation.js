import { useRouter } from "next/router";
import Image from "next/image";
import { Grid } from "@nextui-org/react";
import Link from "next/link";
const ProductNavigation = ({ products, currentIndex, setCurrentIndex }) => {
  const router = useRouter();

  const navigateToProduct = (index) => {
    setCurrentIndex(index);
    router.push(products[index].link);
  };

  const goToPreviousProduct = () => {
    const newIndex = (currentIndex - 1 + products.length) % products.length;
    navigateToProduct(newIndex);
  };

  const goToNextProduct = () => {
    const newIndex = (currentIndex + 1) % products.length;
    navigateToProduct(newIndex);
  };

  return (
    <>
      <Grid className="left-side" css={{ position: "absolute" }}>
        <Grid
          css={{
            padding: 30,
            cursor: "pointer",
            position: "absolute",
            top: 0,
          }}
        >
          <Link href="/">
            <Image
              src={"/assets/icons/logo_black.svg"}
              alt="duetodata logo"
              width={80}
              height={80}
              objectFit="contain"
            />
          </Link>
        </Grid>
        <Grid
          style={{
            position: "fixed",
            bottom: "45%",
            left: "2em",
            padding: "10px",
            justifyContent: "space-between",
            backgroundImage: "url(/assets/icons/bg_arrows.svg)",
            backgroundSize: "100% 100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid css={{ cursor: "pointer" }}>
            <Image
              onClick={goToPreviousProduct}
              alt=""
              src={"/assets/icons/arrowleft.svg"}
              width={25}
              height={25}
              objectFit="cover"
            />
          </Grid>
          <Grid css={{ cursor: "pointer" }}>
            <Image
              onClick={goToNextProduct}
              alt=""
              src={"/assets/icons/arrowright.svg"}
              width={25}
              height={25}
              objectFit="cover"
            />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default ProductNavigation;

import React, { useEffect, useRef } from "react";
import { Grid, Text } from "@nextui-org/react";
import Image from "next/image";
import Layout from "../../layout";
import Lottie from "react-lottie";
import animationData from "../../public/assets/lotties/scroll_black.json";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import back from "../../public/assets/lotties/back_black.json";
import { useTranslation } from "next-i18next";
import Link from "next/link";
gsap.registerPlugin(ScrollTrigger);

const HeroGenericComponent = ({
  challenge1,
  challenge2,
  value1,
  value2,
  imageSrc,
  backgroundColor,
  slider,
}) => {
  const { t } = useTranslation("common");

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  const defaultOptions1 = {
    loop: true,
    autoplay: true,
    animationData: back,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  const mainRef = useRef(null);
  const sliderRef = useRef(null);
  useEffect(() => {
    const sliderElement = sliderRef.current;

    gsap.set(sliderElement, { x: 2500 });

    ScrollTrigger.create({
      start: "top top",
      end: "center 0%",
      scrub: true,
      onEnter: () => {
        gsap.to(sliderElement, {
          x: 0,
          duration: 0.6,
          scrub: true,
        });
      },
      onLeaveBack: () => {
        gsap.to(sliderElement, {
          x: 2500,
          duration: 0.6,
          scrub: true,
        });
      },
    });
  }, []);

  return (
    <>
      <Layout>
        <Grid className="main" css={{ height: "100vh", overflowY: "hidden" }}>
          <Link href="/products">
            <Grid
              css={{ position: "fixed", top: 20, right: 140, zIndex: 99 }}
              className="back_arrow"
            >
              <Lottie options={defaultOptions1} width={50} />
            </Grid>
          </Link>
          <Grid.Container
            ref={mainRef}
            gap={2}
            className="product_container"
            css={{
              backgroundColor: backgroundColor,
              overflow: "hidden",
            }}
          >
            <Grid xs={6} md={4}>
              <Image
                src={imageSrc}
                width={300}
                height={300}
                alt=""
                objectFit="contain"
              />
            </Grid>
            <Grid md={6} xs={12} sm={12}>
              <Grid className="values">
                <Text h3>{t("challenges")}</Text>
                <br></br>
                <Text span>{challenge1}</Text>
                <br></br>
                <Text span>{challenge2}</Text>
                <br></br>

                <Text h3>{t("value")}</Text>

                <br></br>
                <Text span>{value1}</Text>
                <br></br>
                <br></br>
                <Text span>{value2}</Text>
              </Grid>
            </Grid>
            <Grid
              md={1}
              sm={1}
              xs={2}
              css={{ position: "absolute", right: 200, bottom: 0 }}
            >
              <Lottie options={defaultOptions} />
            </Grid>
          </Grid.Container>
          <Grid className="slider" ref={sliderRef}>
            {slider}
          </Grid>
        </Grid>
      </Layout>
    </>
  );
};

export default HeroGenericComponent;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const Blog21Slider = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid className={styles.body}>
          <Text h1 className={styles.title}>
            How AI is reshaping Marketing
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Artificial Intelligence transformed the way we all do businesses
            which engendered a new career path revolution. Businesses nowadays
            search for optimised
            <Link href="/">
              <a
                rel="noopener noreferrer"
                target="_blank"
                className={styles.href}
              >
                {" "}
                Digital solutions
              </a>
            </Link>{" "}
            in order to understand their customer’s state of mind; the way they
            think and react toward their content
          </Text>
          <br></br>
          <br></br>
          <Grid.Container className={styles.content} justifyContent="center">
            <Grid md={1} sm={1} xs={2}>
              <Image
                height={25}
                width={25}
                src="/images/arrow.svg"
                alt=""
                objectFit="contain"
              />
            </Grid>
            <Grid md={11} sm={11} xs={11}>
              <Text span>
                That’s where the role of Artificial Intelligence Marketing comes
                in.
              </Text>
            </Grid>
          </Grid.Container>
          <br></br>
          {/*******************/}
          <Grid align="center" md={12}>
            <Image
              alt="Marketing generate growth"
              src="/images/aiMarketing1.svg"
              width={450}
              height={350}
              objectFit="contain"
            />
          </Grid>
          <br></br>
          <Text span className={styles.title} style={{ color: "#ff4f00" }}>
            What is AI Marketing?
          </Text>
          <br></br>
          <Text span className={styles.content}>
            AI Marketing is a method of exploiting Customer’s Data and AI tools
            to anticipate the customer’s next steps and offer Digital solutions
            that help businesses personalize the customer’s experience. It Uses:
          </Text>
          <br></br>
          <br></br>
          <Grid.Container justifyContent="center" alignItems="center">
            <Grid alignItems="center" md={7} sm={12}>
              <Text span className={styles.content}>
                <ul className="dashed">
                  <li>- Data Analysis</li>
                  <li>- Generating Content</li>
                  <li>- Media planning and Media buying</li>
                  <li>- Automated decision making</li>
                  <li>- Personalization</li>
                </ul>
              </Text>
            </Grid>
            <Grid md={3} sm={12}>
              <Image
                src="/images/aiMarketing2.svg"
                alt="Search for growth statistic"
                width={600}
                height={600}
                objectFit="contain"
              />
            </Grid>
          </Grid.Container>
          <br></br>
          <Grid>
            <Grid md={12} justifyContent="center" alignItems="center">
              <Text span className={styles.content}>
                Benefits of implementing AI in Marketing; they may have the form
                of quantifiable or none quantifiable points such us reduction of
                the risk, increasing revenue, satisfiedcustomers, etc...
              </Text>
            </Grid>
            <br></br>
            <br></br>

            <Text span className={styles.titlev}>
              1. Personalization for customers:
            </Text>
            <br></br>
            <br></br>

            <Text span className={styles.content}>
              Predective analysis helps businesses concentrate on customer’s
              prefrences and make actions based on that Data. These actions will
              take the form of a relevent content that every user will have the
              feeling that it was destinated specially for him to respond to his
              specific needs or solvinghis problems .
            </Text>
            <Grid.Container alignItems="center">
              <Grid md={9}>
                <Text span className={styles.content}>
                  That will lead to the improvement of customer retention.
                  Digital Solutions are not only beneficial for users but also
                  are a life saver for marketers since it represents an easy way
                  for marketers to solvethe user’s problems.
                </Text>
              </Grid>
              <Grid md={3} sm={12}>
                <Image
                  src="/images/aiMarketing3.svg"
                  alt="Target marketing"
                  width={600}
                  height={600}
                  objectFit="contain"
                />
              </Grid>
            </Grid.Container>
            <Text span className={styles.titlev}>
              2. AI help in content creating
            </Text>
            <br></br>
            <br></br>
            <Text span className={styles.content}>
              The biggest challenge for marketers is content creation because of
              the effort and time they spend on generating content. AI is
              solving this problem and making it easy on marketeres. It can
              understand different forms of communication. Which means creating
              content that goes with your brand personality and resonating it
              with your audience. AI can also plan for your future content and
              distribute it in an optimal way through your communication
              channels.
            </Text>
            <br></br>
            <br></br>
            <Text span className={styles.titlev}>
              3. Cost saving for a better ROI
            </Text>
            <br></br>
            <br></br>
            <Text span className={styles.content}>
              Targeted Content is a lot less expensive and the power of AI will
              give you the best ways for an optimised ROI. AI will not only let
              you gain time, but will also minimize marketing costs, while
              staying in the personalization experience.
            </Text>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog21Slider;

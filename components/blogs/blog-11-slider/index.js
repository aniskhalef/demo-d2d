import React from "react";
import { Grid, Text } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog11Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            Artificial intelligence&apos;s influence on customer experience
          </Text>
        </Grid>
        <br></br>

        <Text span className={styles.titleB}>
          Artificial Intelligence’s definition
        </Text>
        <br></br>

        <Text span className={styles.content}>
          Check out this article to learn more about Artificial Intelligence:
          <Link href="/blog/Artificial-Intelligence-Trends">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.green}
            >
              {" "}
              Artificial Intelligence trends
            </a>
          </Link>
          .
        </Text>
        <br></br>
        <br></br>

        <Grid.Container>
          <Grid justifyContent="center" md={8}>
            <Grid>
              <Text span className={styles.titleB}>
                Understanding customer experience?
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Customer experience is your customers&apos; overall view of
                their interaction with your company.
              </Text>
              <br></br>

              <Text span className={styles.content}>
                CX is the outcome of every engagement a consumer has with your
                company, it can be in a Digital way. Check out this article to
                know more about the
                <Link href="/blog/digitalisation-of-customer-experience">
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    className={styles.oranger}
                  >
                    {" "}
                    digitalization of Customer Experience
                  </a>
                </Link>
                .
              </Text>
              <br></br>
              <Text span className={styles.content}>
                From exploring the website to contacting customer support and
                receiving the product or service they purchased from you.
                <br></br>
                Everything that you do influences your customers&apos;
                perception, thus providing an excellent customer experience is
                critical to your success.
              </Text>
            </Grid>
          </Grid>

          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={4}
            sm={12}
          >
            <Image
              src="/images/blog14.1.svg"
              alt="Customer Experience"
              height={250}
              width={250}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>

        <Text span className={styles.titleB}>
          The importance of customer loyalty
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Loyal customers tend to shop regularly due to positive experiences
          they had with your business.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This will automatically make them spend more money than new ones
          because they already trust the products or services you are offering,
          which will generate profits easily.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Although, loyal customers generate higher conversion rates. It can go
          to 70% while new customers have a conversion rate from 5 to 20%. You
          also need to know that retaining existing customers is easier and
          cheaper than customer recruitment. It is surely important to gain new
          ones but it is more expensive and needs more effort.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container>
          <Grid justifyContent="center" md={8} sm={12}>
            <Grid>
              <Text span className={styles.titleB}>
                4 ways ai is driving better customer experience
              </Text>

              <br></br>
              <Text span className={styles.blue}>
                Artificial Intelligence assists in gaining a deeper knowledge of
                the customer:
              </Text>
              <br></br>

              <Text span className={styles.content}>
                &nbsp;Technologies made the world go further and facilitate
                human tasks and journeys, same as AI, it&apos;s continuously
                evolving and improving from the data it analyzes. This empowers
                businesses to provide relevant content that is adaptative to
                their target audience and helps to improve the customer journey.
              </Text>

              <br></br>
              <br></br>

              <Text span className={styles.blue}>
                Predictive Behavior Analysis and Real-Time Decision Making:
              </Text>
              <br></br>

              <Text span className={styles.content}>
                &nbsp;AI and predictive analytics can go beyond historical data,
                providing deeper insights to facilitate a sale through
                recommendations for related products and services, making the CX
                more relevant and most likely to result in a sale.
              </Text>
            </Grid>
          </Grid>
          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={3}
            sm={12}
          >
            <Image
              src="/images/blog14.2.svg"
              alt="AI and customer experience"
              height={250}
              width={250}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.blue}>
          AI and Chatbots:
        </Text>
        <br></br>

        <Text span className={styles.content}>
          Chatbots; the leading application of AI is being used today. It
          facilitated the tasks of businesses by always responding to their
          needs without being confused when customers change the subject.
          Chatbots became relevant in a way it can jump from topic to topic.
          This is how it helps in a better CX.
        </Text>
        <br></br>

        <br></br>
        <Text span className={styles.blue}>
          AI and personalized solutions:
        </Text>
        <br></br>
        <Text span className={styles.content}>
          When talking about personalization, we are talking about content.
          Personalized content can have the form of conversational one. AI
          solved this problem with &apos;Conversational AI&apos; to provide a
          personalized experience to customers.
        </Text>
      </Grid>
    </>
  );
};

export default Blog11Slider;

import React, { useState, useRef, useEffect } from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Image from "next/image";
import styles from "./styles.module.css";

const blogs = [
  {
    link: "/blog/5-steps-for-a-successfu-digital-marketing-audit",
  },
  {
    link: "/blog/2022-digital-marketing-toolbox",
  },
  {
    link: "/blog/Al-Ethics",
  },
  {
    link: "/blog/Al-helps-in-company-growth",
  },
  {
    link: "/blog/Al-Marketing",
  },
  {
    link: "/blog/Al-Solutions",
  },
  {
    link: "/blog/Al-vs-human",
  },
  {
    link: "/blog/artificial-emotional-intelligence",
  },
  {
    link: "/blog/Artificial-Intelligence-and-Robots",
  },
  {
    link: "/blog/Artificial-Intelligence-is-changing-the-world",
  },
  {
    link: "/blog/Artificial-Intelligence-Trends",
  },
  {
    link: "/blog/Artificial-Intelligence's-influence-on-customer-experience",
  },
  {
    link: "/blog/blockchain-importance",
  },
  {
    link: "/blog/Business-Growth",
  },
  {
    link: "/blog/combinig-Al-with-marketing-automation",
  },
  {
    link: "/blog/digital-businesses",
  },
  {
    link: "/blog/digital-problem",
  },
  {
    link: "/blog/digitalisation-of-customer-experience",
  },
  {
    link: "/blog/machine-learning",
  },
  {
    link: "/blog/melding-Al-and-data",
  },
  {
    link: "/blog/strong-digital-marketing-audit",
  },
  {
    link: "/blog/types-of-digital-solutions",
  },
  {
    link: "/blog/when-should-you-run-a-marketing-audit",
  },
];
const Blog1Slider = () => {
  const [count, setCount] = useState(0);
  const counterRef = useRef(null);

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          const target = 3.1;
          const increment = 0.1;
          let currentCount = 0;

          const intervalId = setInterval(() => {
            currentCount += increment;
            if (currentCount >= target) {
              currentCount = target;
              clearInterval(intervalId);
            }
            setCount(currentCount);
          }, 100);

          observer.disconnect();
        }
      });
    });

    observer.observe(counterRef.current);

    return () => observer.disconnect();
  }, []);

  const [countM, setCountM] = useState(0);
  const counterMRef = useRef(null);

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          const target = 85;
          const increment = 1;
          let currentCount = 0;

          const intervalId = setInterval(() => {
            currentCount += increment;
            if (currentCount >= target) {
              currentCount = target;
              clearInterval(intervalId);
            }
            setCountM(currentCount);
          }, 50);

          observer.disconnect();
        }
      });
    });

    observer.observe(counterRef.current);

    return () => observer.disconnect();
  }, []);

  return (
    <>
      <Grid>
        <Text h1 className={styles.title}>
          Blockchain importance
        </Text>
      </Grid>
      <br></br>
      <Grid md={11} sm={12} css={{ lineHeight: "2.2em" }}>
        <Text span>
          You should have noticed that people, nowadays, are trusting technology
          with their identity including their health info and even their
          fortune.
        </Text>
        <br></br>
        <Text span>
          You may be notified that most banks in developed countries are
          considering technology in their systems.
        </Text>
        <br></br>

        <Text span>
          Do you think these people and banks are fool enough to jeopardize
          their identity, brand, or money; at risk of hackers?
        </Text>
        <br></br>
        <Text span>
          Of course not, they are considering the blockchain. No doubt that you
          heard talking about it. Right?
          <br></br>
          If you don’t, no worries:
        </Text>
        <br></br>

        <Text span className={styles.bold}>
          This blog will define blockchain technology, highlight its importance,
          and explain how businesses can implement it.
        </Text>

        <br></br>
        <br></br>

        <Text span className={styles.orange}>
          What is blockchain?
        </Text>
        <br></br>

        <Text span>
          A blockchain is a decentralized, immutable, and transparent ledger.
        </Text>
        <br></br>

        <Text span>
          • Decentralization: the blockchain is a distributed ledger that
          records transactions on multiple computers that are considered blocks.
        </Text>

        <Grid md={12} align={"center"}>
          <Image
            height={500}
            width={500}
            src={"/images/blog24.1.svg"}
            alt="Decentralized aspect of blockchain"
            objectFit="contain"
          />
        </Grid>
        <Text span>
          • Immutability: the record will not be modified unless all the blocks
          are changed.
        </Text>
        <br></br>

        <Text span>
          • Transparency: the record cannot be modified without the network’s
          approval
        </Text>
        <br></br>

        <Text span>
          The use of cryptocurrencies, such as Bitcoin is one of the most
          well-known applications of blockchain technology.
        </Text>
        <br></br>

        <Text span>
          However, the potential applications of blockchain technology go far
          beyond just cryptocurrencies.
          <br></br>
          For example, it could be used to securely store and share medical
          records, track the movement of goods in a supply chain, or facilitate
          secure voting systems…
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.orange}>
          Importance of blockchain
        </Text>
        <br></br>

        <Text span>
          The importance of blockchain technology lies in the fact that it
          ensures a secure, transparent, and immutable recording of data and
          transactions. This has the potential to revolutionize a wide range of
          industries, including finance, supply chain management, healthcare,
          and more.
        </Text>
        <br></br>
        <br></br>
        <Grid
          css={{
            display: "flex",
            justifyContent: "space-evenly",
            width: "100%",
            textAlign: "center",
          }}
        >
          <Grid css={{ width: "40%" }}>
            <Grid className={styles.bigorange} ref={counterRef}>
              <Text span>$ {count.toFixed(1)} trillion</Text>
            </Grid>

            <br></br>
            <Grid>
              <span className={styles.blue}> By 2030 </span>, the blockchain has
              the potential to add to the{" "}
              <span className={styles.blue}>global economy</span>
            </Grid>
            <Grid>
              <Text span className={styles.bold}>
                Source: Gartner
              </Text>
            </Grid>
          </Grid>

          <Grid css={{ width: "40%" }}>
            <Grid className={styles.bigorange} ref={counterMRef}>
              <Text span>+{countM.toFixed(1)}M wallets</Text>
            </Grid>
            <Grid>
              <Text span>
                <br></br>
                Users have registered{" "}
                <span className={styles.blue}>over 85 million </span>
                blockchain wallets
              </Text>
            </Grid>
            <Grid>
              <Text span className={styles.bold}>
                Source: Blockchain.com
              </Text>
            </Grid>
          </Grid>
        </Grid>
        <br></br>
        <Grid.Container
          align="center"
          css={{
            alignItems: "center",
            justifyContent: "center",
            marginTop: "10%",
          }}
        >
          <Grid>
            <Image
              height={300}
              width={300}
              src={"/images/blog24.2.svg"}
              alt="90% of US and European banks"
              objectFit="contain"
            />
            <Grid>
              <Text span className={styles.bold}>
                Source: Fortunly
              </Text>
            </Grid>
          </Grid>

          <Grid css={{ width: "50%" }}>
            <Text span>
              <span className={styles.blue}> 90% </span> of US and European
              banks had started exploring blockchain’s potential since 2018
            </Text>
          </Grid>
        </Grid.Container>

        <Grid.Container
          css={{ alignItems: "center", justifyContent: "center" }}
          align="center"
        >
          <Grid css={{ width: "50%" }}>
            <Text span>
              <span className={styles.blue}>40% </span> of top health executives
              see blockchain as one of their top 5 priorities.
            </Text>
          </Grid>

          <Grid>
            <Image
              height={300}
              width={300}
              src={"/images/blog24.3.svg"}
              alt="40% of top health executives "
              objectFit="contain"
            />
            <Grid>
              <Text span className={styles.bold}>
                Source: Healthcare Weekly
              </Text>
            </Grid>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Text span className={styles.orange}>
          Types of blockchains:
        </Text>
        <br></br>

        <Text span>
          There are several types of blockchains. We can classify these types
          based on different factors, we can mention the type of network, the
          way of storing and validating data, and the level of decentralization.
        </Text>
        <br></br>

        <Grid xs={12}>
          <Grid align="center">
            <Image
              height={500}
              width={500}
              src={"/images/blog24.4.svg"}
              alt="Types of blockchain"
              objectFit="contain"
            />
          </Grid>

          <Text span>
            <span className={styles.bold}>
              {" "}
              1. Permissionless blockchain (public blockchain):{" "}
            </span>
            It is an open and decentralized network where anyone is allowed to
            participate as a node without having any permission. Example:
            Ethereum and Bitcoin networks.
          </Text>
          <br></br>
          <br></br>

          <Text span>
            <span className={styles.bold}> 2. Permissioned blockchain: </span>
            to join this type of network and participate in its consensus
            process, users should grant permission first. We can have two forms
            of this type based on the level of centralization:
          </Text>
          <br></br>

          <Text span>
            <span className={styles.bold}> a. Private blockchain: </span>it is a
            closed and fully centralized network that is controlled by a single
            entity. Access to such a network is allowed to a specific group of
            people.
          </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}> b. Consortium blockchain: </span>: it
            is a partially decentralized network that is controlled by a group
            of pre-selected nodes.
          </Text>
          <br></br>
          <br></br>

          <Text span>
            <span className={styles.bold}> 3. Hybrid blockchain: </span>it is a
            network that mixtures elements of private and public networks;
            meaning that some processes are public, and others are kept private.
            This combination allows a degree of customization and flexibility.
            Giving the example of a healthcare system that is based on a
            blockchain network, a patient can manage access to his personal
            information by giving access to certain data and limiting others.
          </Text>
          <br></br>
          <br></br>

          <Text span className={styles.orange}>
            Implement blockchain in businesses:
          </Text>
          <br></br>
          <Text span>
            Improving efficiency is the main reason why a business might
            consider implementing blockchain technology since it ensures
            peer-to-peer transactions without needing an intermediary.
          </Text>
          <br></br>
          <Text span>Therefore, blockchain can help businesses to:</Text>
          <br></br>
          <br></br>

          <Text span>
            <span className={styles.bold}> • Optimize Transactions:</span> by
            reducing the cost and time of completing a transaction.
          </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}>
              {" "}
              • Define business logic through smart contracts:
            </span>{" "}
            by storing and replicating self-executing contracts respecting the
            terms of the agreement between the seller and buyer, blockchain
            technology helps businesses to automate and streamline their
            processes. Also, this technology reduces the risk of fraud/errors
            and improves the efficiency of executing contracts.
          </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}> • Verify identities: </span> the
            blockchain creates a digital identity that is tied to a person’s
            real-world identity, and it is represented by private and public
            keys. To perform an action on blockchain such as sending a
            transaction, the user has to sign the transaction with his private
            key that proves his identity.
            <br></br>
            Digital identity is used for several purposes. We can mention the
            verification of illegibility for a particular program or service,
            checking the user’s age, and accessing online services.
          </Text>
          <br></br>
          <Text span>We can use the blockchain in several cases such as: </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}> 1. Payment processing: </span>by
            ensuring a near-instantaneous verification and settlement of
            transactions, blockchain technology improves the speed of payment
            processing and reduces fees.
          </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}> 2. Supply chain management: </span>
            using a blockchain-based supply chain system allows businesses to
            track the records of all transactions and activities such as the
            movement of goods and materials through its supply chain. Therefore,
            each business can easily trace the origin of its products and
            ensures their quality. With its high level of transparency, the
            blockchain ensures trust and greater collaboration among supply
            chain partners.
          </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}> 3. Healthcare: </span>
            implementing blockchain in a healthcare solution permit optimizing
            the journey of patients by reducing costs and increasing the
            efficiency of medical data management. Therefore, blockchain
            technology improves the accessibility and quality of the healthcare
            sector. One of the use cases of blockchain in the health sector is
            the access and sharing of EHRs (electronic health records) after
            having the patient’s permission.
          </Text>
          <br></br>
          <br></br>
          <Grid.Container css={{ alignItems: "center" }}>
            <Grid md={1} sm={1} xs={1}>
              <Image
                height={40}
                width={40}
                src={"/images/blog24.5.svg"}
                alt="Important"
                objectFit="contain"
              />
            </Grid>
            <Grid md={11} sm={11} xs={11}>
              <Text span className={styles.orange}>
                Misconceptions about blockchain:
              </Text>
            </Grid>
          </Grid.Container>
          <br></br>
          <Text span>
            Commonly we held some wrong notions about blockchain such as:
          </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}>
              1. Blockchain is the same as Bitcoin:{" "}
            </span>
            well, bitcoin is a type of cryptocurrency that uses blockchain
            technology. Blockchain is a wider concept that can be applied in
            various cases as mentioned above.
          </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}>
              {" "}
              2. Blockchain is a new technology:{" "}
            </span>
            here we need to make a separation between the naming of the term and
            the concept itself. The term “blockchain” is relatively new indeed.
            It was only coined in 2008 with the creation of Bitcoin. However,
            the technology in itself has seen the light since 1970. At first, it
            all starts with the concept of a decentralized ledger. Then, the
            first practical implementation of a blockchain-like system, which
            dates back to 1991, has been developed by Stuart Haber and W. Scott
            Stornetta to create a system of secure document timestamping.
          </Text>
          <br></br>
          <Text span>
            <span className={styles.bold}>
              {" "}
              3. Blockchain and web3 are the same:{" "}
            </span>
            well, it is somehow expected that people may confuse these two
            technologies since they are closely related. Blockchain technology
            is a key part of the Web3 vision in other other words it is just a
            piece of the puzzle. Web3 is the third generation of the worldwide
            web. For this generation, using decentralized technologies is
            essential. The main goal here is to give users more control over
            their data by creating a more open and transparent internet. Of
            course, since we have been talking about the decentralized aspect of
            blockchain in this blog, we guess you can now conclude that
            blockchain is a technology used to reach the goal of web3.
          </Text>
          <br></br>

          <Text span>
            Hoping you now have a clear overview of blockchain technology.
          </Text>
          <br></br>

          <Text span>
            <span className={styles.bold}> Please note: </span>if you want to
            implement it in your solution you should first, determine how
            blockchain can benefit your business,
            <br></br> Don’t hesitate to contact our experts, they will help you
            in determining the best way to implement blockchain in your business
            taking into consideration the specificities of your solution and,
            they will take care of the implementation process as well.
          </Text>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog1Slider;

import { useRouter } from "next/router";
import Image from "next/image";
import { Grid } from "@nextui-org/react";
import Link from "next/link";
const BlogNavigation = ({ blogs, currentIndex, setCurrentIndex }) => {
  const router = useRouter();

  const navigateToBlog = (index) => {
    setCurrentIndex(index);
    router.push(blogs[index].link);
  };

  const goToPreviousBlog = () => {
    const newIndex = (currentIndex - 1 + blogs.length) % blogs.length;
    navigateToBlog(newIndex);
  };

  const goToNextBlog = () => {
    const newIndex = (currentIndex + 1) % blogs.length;
    navigateToBlog(newIndex);
  };

  return (
    <>
      <Grid className="left-side" css={{ position: "absolute" }}>
        <Grid
          css={{
            padding: 30,
            cursor: "pointer",
            position: "absolute",
            top: 0,
          }}
        >
          <Link href="/">
            <Image
              src={"/assets/icons/logo_white.svg"}
              alt="duetodata logo"
              width={80}
              height={80}
              objectFit="contain"
            />
          </Link>
        </Grid>
        <Grid
          style={{
            position: "fixed",
            bottom: "45%",
            left: "2em",
            padding: "10px",
            justifyContent: "space-between",
            backgroundImage: "url(/assets/icons/bg_arrows.svg)",
            backgroundSize: "100% 100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Grid css={{ cursor: "pointer" }}>
            <Image
              onClick={goToPreviousBlog}
              alt=""
              src={"/assets/icons/arrowleft.svg"}
              width={25}
              height={25}
              objectFit="cover"
            />
          </Grid>
          <Grid css={{ cursor: "pointer" }}>
            <Image
              onClick={goToNextBlog}
              alt=""
              src={"/assets/icons/arrowright.svg"}
              width={25}
              height={25}
              objectFit="cover"
            />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default BlogNavigation;

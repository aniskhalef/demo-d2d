import React, { useEffect, useRef } from "react";
import { Grid, Text } from "@nextui-org/react";
import Image from "next/image";
import Layout from "../../layout";
import Lottie from "react-lottie";
import animationData from "../../public/assets/lotties/scroll_white.json";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import back from "../../public/assets/lotties/back_white.json";
import Link from "next/link";
gsap.registerPlugin(ScrollTrigger);

const HeroGenericComponent = ({
  title,
  description,
  subtitle,
  paragraph,
  imageSrc,
  backgroundColor,
  slider,
}) => {
  const mainRef = useRef(null);
  const sliderRef = useRef(null);
  useEffect(() => {
    const sliderElement = sliderRef.current;

    gsap.set(sliderElement, { x: 2500 });

    ScrollTrigger.create({
      start: "top top",
      end: "center 0%",
      scrub: true,
      onEnter: () => {
        gsap.to(sliderElement, {
          x: 0,
          duration: 0.6,
          scrub: true,
        });
      },
      onLeaveBack: () => {
        gsap.to(sliderElement, {
          x: 2500,
          duration: 0.6,
          scrub: true,
        });
      },
    });
  }, []);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  const defaultOptions1 = {
    loop: true,
    autoplay: true,
    animationData: back,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <>
      <Layout>
        <Grid className="main" css={{ height: "100vh" }}>
          <Link href="/blog">
            <Grid
              css={{ position: "fixed", top: 20, right: 140, zIndex: 99 }}
              className="back_arrow"
            >
              <Lottie options={defaultOptions1} width={50} />
            </Grid>
          </Link>
          <Grid.Container
            ref={mainRef}
            gap={2}
            className="product_container"
            css={{
              backgroundColor: backgroundColor,
              overflow: "hidden",
            }}
          >
            <Grid xs={8} md={4}>
              <Image
                src={imageSrc}
                width={400}
                height={400}
                alt=""
                objectFit="contain"
              />
            </Grid>
            <Grid md={6} xs={12} sm={12}>
              <Grid className="values">
                <Text h1 className="text-white">
                  {title}
                </Text>
                <br />
                <Text span className="text-white">
                  {description}
                </Text>
                <br />
                <br />
                <Text span className="text-white">
                  {subtitle}
                </Text>
                <br />
                <br />
                <Text span className="text-white">
                  {paragraph}
                </Text>
              </Grid>
            </Grid>
            <Grid
              md={1}
              xs={1}
              css={{ position: "absolute", right: 200, bottom: 0 }}
            >
              <Lottie options={defaultOptions} />
            </Grid>
          </Grid.Container>
          <Grid className="slider" ref={sliderRef}>
            {slider}
          </Grid>
        </Grid>
      </Layout>
    </>
  );
};

export default HeroGenericComponent;

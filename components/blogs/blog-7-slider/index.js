import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";
import Image from "next/image";
const Blog7Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            When should you run a marketing audit?
          </Text>
          <br></br>
        </Grid>

        <Grid md={11} sm={12}>
          <Text span className={styles.content}>
            Let’s talk more about why a marketing audit is important and how it
            can benefit your organization:
            <br></br>
            -&nbsp; Feature your best assets and shortcomings in your current
            marketing / Communication plan
            <br></br>
            -&nbsp; Gives you the needed data to redistribute assets on
            additional helpful exercises
            <br></br>
            -&nbsp; Guarantees that your organization extends to the most
            optimal image across your digital steps
            <br></br>
            -&nbsp; Helps decide how to adjust your communication / Marketing
            intend to build its adequacy.
          </Text>
          <br></br>
          <Text span className={styles.blue}>
            Now, when is the best time for a marketing audit?
          </Text>
          <br></br>
          <br></br>
          <Text span className={styles.numlist}>
            1. When you&apos;re launching a new business
          </Text>
          <br></br>
          <Text span className={styles.content}>
            The marketing audit is crucial in this phase because it reduces the
            risk of losing investments and defines if your efforts are going to
            pay off. Let’s imagine you are willing to hire a community manager
            (you’re most certainly giving them your money) and you are expecting
            a ROI (return on investment), a marketing audit allows you to define
            what to expect and what are the KPIs that you should focus on to
            define when and how you will be rewarded for that.
          </Text>
          <br></br>
          <br></br>
          <Text span className={styles.numlist}>
            2. When you&apos;re introducing a new product
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Marketing audit in this phase will help to pick the proper channels
            for the new product. New products are like babies, people usually
            tend to be very attached and protective of their ideas and new
            products, which means that they often miss out on strategic details
            due to a lack of perception, Marketing audit helps you prevent that
            by providing all the needed insights for a solid and efficient
            strategy.
          </Text>
          <br></br>
          <br></br>
          <Text span className={styles.numlist}>
            3. When You’ve bought an existing business
          </Text>
          <br></br>
          <Text span className={styles.content}>
            It is mandatory in this case to criticize the old marketing
            activities, to know whether you should plug the gaps for better
            performance or opt for a rebranding strategy since the existing
            business is not hitting the goals as expected.
          </Text>
          <br></br>
          <br></br>
          <Text span className={styles.numlist}>
            4. When Things don’t work out as expected
          </Text>
          <br></br>
          <Text span className={styles.content}>
            In this case, you should examine your strategies to detect knots and
            find ways to untie it.
            <br></br>
            The marketing audit will assist you with recognizing the most
            important points in your methodology that aren’t functioning as
            expected, yet that you’re investing resources on.
          </Text>
          <br></br>
          <br></br>

          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={12}
            sm={12}
          >
            <Image
              src="/images/blog18.1.svg"
              alt="Marketing Audit"
              width={300}
              height={200}
              objectFit="contain"
            />
          </Grid>
          <br></br>
          <Text span className={styles.content}>
            Marketing audits or any type of audit usually provoke negative
            emotions, which are usually: fear and boredom. It takes an enormous
            amount of work, but it’s surely for the greater good.
            <br></br> <br></br>
            If you’ve done it before then you surely know what we are talking
            about, if not you should start thinking about it, we would gladly
            help you through the first steps of your own marketing audit.
            <br></br>
            <br></br>
            Check how we can help you start your full
            <Link href="#">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.green}
              >
                {" "}
                Digital Marketing Strategy{" "}
              </a>
            </Link>
          </Text>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog7Slider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const Blog15Slider = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Text h1 className={styles.title}>
          Artificial Emotional Intelligence
        </Text>

        <br></br>
        <br></br>
        <Grid align="center" md={12}>
          <Image
            width={350}
            height={250}
            objectFit="contain"
            src="/images/blog10.1.svg"
            alt="Artificial Emotional Intelligence"
          />
        </Grid>
        <br></br>
        <br></br>
        <Text span className={styles.title}>
          What is Artificial Emotional Intelligence?
        </Text>
        <Grid md={11}>
          <Text span className={styles.content}>
            As we can tell from its name, artificial emotional intelligence is a
            mix of emotions and
            <Link href="/">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.iruk}
              >
                {" "}
                AI
              </a>
            </Link>
            .<br></br>
            It’s also called emotion AI or affective computing.
            <br></br>
            But How?
            <br></br>
            By the ability for machines or we can say computers to read emotions
            by analyzing Data, Counting the tone of voice, the gestures we use,
            the facial expressions, the verbal and the nonverbal speech and
            other expressions since people explicit emotions in various ways.
            This is what is called artificial emotional intelligence. It’s the
            ability of computers to understand humans which can lead to an
            interaction between these two sides in a very natural way as if two
            people are interacting.
          </Text>
        </Grid>
        <br></br>
        <Text span className={styles.title}>
          How does it work?
        </Text>
        <Grid md={11}>
          <Text span className={styles.content}>
            Affective computing detect emotions through the mix of sensors,
            cameras, speech science,
            <a
              target="_blank"
              href="/blog/machine-learning"
              className={styles.blue}
            >
              {" "}
              Machine Learning{" "}
            </a>
            and deep Learning alogorithms. By the collection of Data and
            processing it to finally identify key emotions such us Fear, anger,
            as well as joy. Learn more about Irukai, our Digital Solution that
            will allow you to follow, analyze, and classify your client’s state
            of emotions through their opinion, comments Etc. to get better
            insights about your customer satisfaction.
          </Text>
        </Grid>
        <br></br>
        <Text span className={styles.title}>
          The potential of affective computing
        </Text>
        <Grid align="center">
          <Image
            width={450}
            height={300}
            objectFit="contain"
            src="/images/blog10.2.svg"
            alt=" Potential of affective computing"
          />
        </Grid>
        <br></br>
        <br></br>

        <Text span className={styles.title}>
          What Fields can take advantage of emotion AI?
        </Text>
        <br></br>
        <Text span className={styles.content}>
          There are lots of industries that use affective computing . Here are
          just few examples:
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.titleA}>
          Healthcare
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Healthcare is a field that seek for AI since its providing lots of
          benefits, That’s why it can take advantage of emotion AI to predict
          sudden attacks of illness or even predict people from depression
          before it happen. Thanks to this technology it is also recommended to
          communicate with autistic people
        </Text>
        <br></br>
        <br></br>
        <Grid.Container>
          <Grid md={8}>
            <Grid>
              <Text span className={styles.titleA}>
                Marketing
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Customer Journey is an important task that a Marketer should pay
                attention on. However, When customer go through a positive
                experience with a brand, they are more likeky to become loyal
                and will also recommend this brand to others,which we can call
                it the word of mouth. But these customers could also go through
                a bad experience which will generate a bad buzz for this brand.
                Therefore if brands want to improve their customer journey
                it&apos;s primordial to use emotion AI to facilitate this task
                and guarantee a good experience.
              </Text>
            </Grid>
          </Grid>
          <Grid justifyContent="center" alignItems="center" md={4}>
            <Image
              width={400}
              height={350}
              objectFit="contain"
              src="/images/blog10.3.svg"
              alt="Emotion AI and Marketing"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.titleA}>
          Education
        </Text>
        <br></br>
        <Text span className={styles.content}>
          AI can be an amazing tool to detect learning disabilities, it can also
          detect children&apos;s facial expressions and deduce if they are
          folloing the course or not. This can be used perfectly in online
          courses.
        </Text>
      </Grid>
    </>
  );
};

export default Blog15Slider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const Blog23Slider = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid className={styles.body}>
          <Text h1 className={styles.title}>
            <span className={styles.span}>AI Solutions</span>, Let&apos;s talk
            about it.
          </Text>
          <Text span className={styles.content}>
            We talked about{" "}
            <span className={styles.span}>Digital Solutions</span> in the first
            quarter and the utility of Big Data in its use and about{" "}
            <span className={styles.span}>Artificial Intelligence</span> and the
            progressiveness that comes with its fast pace evolution while
            companies are more and more aware of it, so it&apos;s about time we
            focus on the merge of both of these tools and engage into the main
            subject which is,<span className={styles.span}> AI Solutions</span>{" "}
            .We will be setting a clear definition of IT, Introducing the
            methods of implementing IT and finally exposing the advantages that
            IT produces.
          </Text>
          <br></br>
          <Grid
            align="center"
            justifyContent="center"
            alignItems="center"
            md={12}
          >
            <Image
              className={styles.ai}
              alt="Digital Solution"
              src="/images/digital+AI.png"
              objectFit="contain"
              height={220}
              width={400}
            />
          </Grid>
          <Text span className={styles.numlist}>
            1. Definition of AI Solutions :
          </Text>
          <br></br>
          <Text span className={styles.list}>
            <li>- The business of personalized Automation.</li>
            <li>
              - It is the business of personalized automation in a digital
              marketing aspect.
            </li>
            <li>
              - All tools that help with personalizing a business by using Big
              Data on targeted audiences with the use of Artificial intelligence
              in a digital marketing aspect.
            </li>
          </Text>
          <br></br>

          <Text span className={styles.content}>
            We define the first one as a slogan, the second as a clarification
            of the slogan and the last one as the definition.
            <br></br>For AI Solutions are the technological part of the new era
            of marketing and consists of everything done outside manual inputs
            of any kind.
          </Text>
          <br></br>
          <br></br>
          <Text span gutterBottom className={styles.title}>
            The importance of AI Solutions for Business :
          </Text>
          <br></br>
          <br></br>

          <Grid align="center">
            <Grid.Container className={styles.stat} align="center">
              <Grid
                container
                align="center"
                className={styles.stat}
                md={6}
                sm={12}
                xs={12}
              >
                <Image
                  src="/images/38.svg"
                  alt=""
                  objectFit="contain"
                  height={150}
                  width={150}
                />
              </Grid>

              <Grid md={6} sm={12} xs={12} className={styles.stat}>
                <Text span className={styles.content1}>
                  AI will boost profitability by 38% and generate $14 Trillion
                  of additional revenue by 2035.
                </Text>
              </Grid>
            </Grid.Container>

            <Grid.Container align="center" className={styles.stat}>
              <Grid
                container
                align="center"
                className={styles.stat}
                md={6}
                sm={12}
                xs={12}
              >
                <Image
                  src="/images/61.svg"
                  alt=""
                  objectFit="contain"
                  height={150}
                  width={150}
                />
              </Grid>

              <Grid md={6} sm={12} xs={12} className={styles.stat}>
                <Text span className={styles.content1}>
                  61% of Marketers say ‘Artificial intelligence is the most
                  important aspect of their data strategy’.
                </Text>
              </Grid>
            </Grid.Container>

            <Grid.Container align="center" className={styles.stat}>
              <Grid
                container
                align="center"
                className={styles.stat}
                md={6}
                sm={12}
                xs={12}
              >
                <Image
                  src="/images/83.svg"
                  alt=""
                  objectFit="contain"
                  height={150}
                  width={150}
                />
              </Grid>
              <Grid md={6} sm={12} xs={12} className={styles.stat}>
                <Text span className={styles.content1}>
                  83% of Early AI Adopters have already achieved substaintial
                  (30%) or moderate (53%) economical benefit.
                </Text>
              </Grid>
            </Grid.Container>
          </Grid>
          {/*******************/}

          <Text span className={styles.numlist}>
            2. Implementation Methods :
          </Text>
          <br></br>
          <Text span className={styles.content}>
            It has become quite easy to integrate these solutions into any form
            of company there is, specially after the Rise in popularity of this
            sector and the, now, proven availablity in the variety of choice in
            IT companies. With the use of either plugins that get integrated
            into your well-known platforms and directly benefit from them or Get
            specialized dashboards from service providers and external sourcing
            to respond to certain business needs that you might need or just
            develop your own even if it doesn’t seem that easy.The final result
            is what we call a Dashboard in which you have access to and should
            look like this :
          </Text>

          <Grid align="center" className={styles.stat}>
            <Image
              className={styles.imgs}
              src="/images/dashboard.png"
              alt="Digital Solution Dashboard"
              objectFit="contain"
              height={300}
              width={350}
            />
          </Grid>

          <Text span className={styles.title}>
            What are Dashboards ?
          </Text>
          <br></br>
          <br></br>
          <Text span className={styles.content}>
            Modern dashboards display key performance indicators (KPIs) and key
            risk indicators (KRIs), in related charts, maps and scorecards, in
            order to enable an enterprise to focus on the most important
            performance activities. The purpose of AI Solutions is to display
            information on a single screen in a clear manner in order to be
            understood by everyone. It is an application or a user interface
            that helps to measure the performance of the enterprise, understand
            the organizational units and the business processes. There are three
            types of dashboards: operational, tactical and strategic.
          </Text>

          <br></br>
          <Grid align="center" className={styles.stat}>
            <Image
              src="/images/pyramid.png "
              alt="Dashboard Pyramid"
              objectFit="contain"
              height={250}
              width={400}
            />
          </Grid>
          <br></br>

          <Grid.Container>
            <Grid md={6} sm={6} xs={12}>
              <Grid>
                <Text span className={styles.numlist}>
                  3. Benefits of AI Solutions for a business :
                </Text>

                <br></br>
                <br></br>

                {/*******************/}

                <Text span className={styles.list}>
                  <li>1. Efficiency and productivity gains</li>
                  <li>2. Improved speed of business</li>
                  <li>3. Efficiency and productivity gains</li>
                  <li>4. New capabilities and business model expansion</li>
                  <li>5. Better customer service</li>
                  <li>6. Improved monitoring</li>
                  <li>7. Real-time customer analytics</li>
                  <li>8. Key performance indicators</li>
                  <li>9. Better forecasting</li>
                  <li style={{ marginLeft: "-10px" }}>
                    10. Enhanced visibility
                  </li>
                </Text>
              </Grid>
            </Grid>

            <Grid justifyContent="center" md={4} sm={6} xs={12}>
              <Image
                objectFit="contain"
                height={350}
                width={350}
                src="/images/bussiness.png"
                alt="AI for Business growth"
              />
            </Grid>
          </Grid.Container>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog23Slider;

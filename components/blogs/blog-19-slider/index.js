import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const Blog19Slider = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            How does Artificial Intelligence helps in company growth
          </Text>
          <br></br>
          <Text span className={styles.content}>
            <Link href="https://irukai.com">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.iruk}
              >
                {" "}
                Digital solutions{" "}
              </a>
            </Link>
            transformed the way of thinking and made people realize the
            importance of Digital presence these days. That’s where
            <Link href="/">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.iruk}
              >
                {" "}
                Artificial Intelligence{" "}
              </a>
            </Link>
            takes place, However, in order to being present Digitally, it’s
            important to consider AI. In other termes, AI makes it possible for
            companies to learn more about their customers and their experience
            within their brand. It plays the role of performing of human tasks
            using computer technologies relying on different languages.
            <br></br>
            Learn more on Languages used by
            <Link href="#">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.bold}
              >
                {" "}
                Web development experts{" "}
              </a>
            </Link>
            to help you scale up your company.
            <br></br>
            <br></br>
          </Text>
        </Grid>

        <Grid align="center">
          <Image
            alt="AI and Company growth"
            src="/images/blog6.1.svg"
            height={250}
            width={350}
            objectFit="contain"
          />
        </Grid>
        <br></br>
        <Text span className={styles.content}>
          Digital Solutions helped AI evolve and took place in our everyday
          lives. However, it facilitates the tasks we&apos;ve been doing
          manually by managing a large amount of Data in various manners.
          Peolple as well as companies uses Artificial Intelligence not only
          because of it&apos;s way of getting things done easier but also in
          evolving, scaling, or even generating growth to the Business it deals
          with.
        </Text>
        <br></br>
        <br></br>

        <Grid justifyContent="center" alignItems="center" md={10}>
          <Text span className={styles.title}>
            The use of AI in companies
          </Text>
          <br></br>
          <br></br>
          <br></br>
          <Grid.Container className={styles.content} alignItems="center">
            <Grid md={1} sm={1} xs={2}>
              <Image
                height={25}
                width={25}
                src="/images/bluearrow.svg"
                alt=""
                objectFit="contain"
              />
            </Grid>
            <Grid>
              <Text span className={styles.blueTitle}>
                AI in Marketing
              </Text>
            </Grid>
          </Grid.Container>
          <br></br>
          <Grid.Container alignItems="center">
            <Grid md={8} sm={1} xs={2}>
              <Text span className={styles.content}>
                The question is how can AI work with Marketing and where can we
                find growth in the combination of these domains? As we all know
                it is essential for a Business to use Marketing especially in
                the Internet in order to promote its brand and develop a
                connection with its target audience. To do so, a marketer needs
                to gain information about the target, to increase customer
                experience through personalization in order to generate
                conversions.
              </Text>
            </Grid>

            <Grid justifyContent="center">
              <Image
                src="/images/blog6.2.svg"
                alt="Online Marketing"
                height={250}
                width={250}
                objectFit="contain"
              />
            </Grid>
          </Grid.Container>

          <br></br>
          <br></br>
        </Grid>

        <span className={styles.bold}>- Website Experience</span>
        <br></br>
        <br></br>
        <Text span className={styles.content}>
          Due to Artificial Intelligence, marketers have the ability to define a
          user&apos;s interaction, his location, or even his interest to your
          website to convert prospects into customers.
        </Text>
        <br></br>
        <br></br>
        <span className={styles.bold}>- Notifications</span>
        <br></br>
        <br></br>
        <Text span className={styles.content}>
          Getting notifications to users that we can define it as
          &apos;Push-notifications&apos; helps Businesses convey messages,and
          diverse information through any browser. This pop-up messages can lead
          companies scale up their sales,help users gain notoriety of your brand
          and somehow growing your Business perfectly.
        </Text>
        <br></br>
        <br></br>
        <span className={styles.bold}> - SEO Optimization </span>
        <br></br>
        <br></br>
        <Text span className={styles.content}>
          Mareters put on focus on optimizing content so that users can find it
          in SERP&apos;s. AI help them to do so by informing them the
          &apos;search volume&apos; , and what words or phrases do they use
          while searching for an information. Although, thanks to AI, marketers
          can take advantage of keywords that aren&apos;t used by competitors,
          and analyze competitor SEO strategy.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container className={styles.content} alignItems="center">
          <Grid md={1} sm={1} xs={2}>
            <Image
              height={25}
              width={25}
              src="/images/bluearrow.svg"
              alt=""
              objectFit="contain"
            />
          </Grid>
          <Grid>
            <Text span className={styles.blueTitle}>
              AI in sales
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <Grid md={11}>
          <Text span className={styles.content}>
            AI is affecting sales in a very positive way. However, it helps in
            converting Leads into loyal customers through Data that was
            collected about clients and their desire . So, nowadays it is easier
            to reach a client and make him a loyal one.
          </Text>
        </Grid>

        <Grid align="center">
          <Image
            src="/images/blog6.3.svg"
            alt="AI and sales"
            height={300}
            width={250}
            objectFit="contain"
          />
        </Grid>

        <Grid.Container className={styles.content} alignItems="center">
          <Grid md={1} sm={1} xs={2}>
            <Image
              height={25}
              width={25}
              src="/images/bluearrow.svg"
              alt=""
              objectFit="contain"
            />
          </Grid>
          <Grid>
            <Text span className={styles.blueTitle}>
              AI in Human Ressources
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <Grid>
          <Text span className={styles.content}>
            It helps analyze candidate profiles; such as hiring condidates. This
            can make HR manager&apos;s tasks easier.
            <br></br>
            This Digital solution helps companies maintain their relationship
            while exchanging informations, needs or offers.
          </Text>
        </Grid>

        <br></br>
        <br></br>
        <Grid.Container className={styles.content}>
          <Grid md={1} sm={1} xs={2} css={{ alignItems: "flex-start" }}>
            <Image
              src="/images/orangeArrow.svg"
              alt=""
              height={25}
              width={25}
              objectFit="contain"
            />
          </Grid>
          <Grid md={10} sm={1} xs={2}>
            <Text span className={styles.content}>
              To conclude, Digital Solutions became an important factor to
              succeed nowadays, It&apos;s all about facilitating tasks on
              Businesses as well as users, making things easier in collecting
              information, using them in a way that will generate growth to
              companies as well as to society.{" "}
            </Text>
          </Grid>
        </Grid.Container>
      </Grid>
    </>
  );
};

export default Blog19Slider;

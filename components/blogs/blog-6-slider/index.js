import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog6Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            The why and how of a strong digital marketing audit
          </Text>
        </Grid>
        <br></br>

        <Grid>
          <Text span className={styles.content}>
            After reading
            <Link href="/blog/when-should-you-run-a-marketing-audit">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.green}
              >
                {" "}
                &quot;when should you run a marketing audit&quot;
              </a>
            </Link>
            , you might be wondering why you have even to think about it in the
            first place.
          </Text>
          <br></br>

          <Text span className={styles.content}>
            Or maybe it’s blurry and you’re confused between an audit and a
            digital marketing audit.
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Well, first, we will neutralize all ambiguities. Next, we&apos;ll
            prove to you how important a digital marketing audit is. And
            finally, we will outline how a digital marketing audit should
            -ideally- be.
          </Text>
          <br></br>
          <Text span className={styles.content}>
            An audit is a form of investigation and there are a bunch of types
            of audits: from auditing finance activities to management to
            marketing to accounting etc. Long story short, an audit reports on
            the performance of every single department in your company. It
            checks whether all your business requirements are met or not.
          </Text>
          <br></br>
          <Text span className={styles.content}>
            So digital marketing audit is one of the many types of audits. It
            outlines all marketing activities and efforts undertaken on digital
            marketing channels: practices, ads, posts, and strategies.
          </Text>
          <br></br>
          <Text span className={styles.content}>
            The audit should include all the company’s marketing channels, its
            competitor performance, its content performance, its paid
            performance, its owned assets, each channel-specific trend and areas
            of optimization. <br></br>When executed correctly, it evaluates your
            company’s marketing strategy and provides you with the answers to
            what you could do to improve the digital gaps and loopholes in your
            ongoing strategy.
          </Text>
          <br></br>
          <br></br>
          <Grid.Container alignItems="center">
            <Grid justifyContent="center" md={6} sm={12}>
              <Text span className={styles.content}>
                <span className={styles.span}> Here are </span>
                <span className={styles.spanG}>
                  6 reasons why you should conduct a digital marketing audit
                </span>
                <span className={styles.span}> as soon as possible </span>
              </Text>
            </Grid>
            <Grid
              container
              justifyContent="center"
              align="center"
              md={5}
              sm={12}
            >
              <Image
                src="/images/blog19.1.svg"
                alt="Digital Marketing Audit"
                height={300}
                width={300}
                objectFit="contain"
              />
            </Grid>
          </Grid.Container>
          <br></br>
          <Text h2 className={styles.numlist}>
            1. To detect weaknesses
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Facing your company&apos;s imperfections would by no means be easy.
            But keep moving ahead without keeping an eye on how things stand,
            will never be a solution. Bottlenecks and weaknesses in a business
            obstruct growth, it could even crash your company. Scary thoughts,
            right? Therefore, weaknesses should be identified sooner rather than
            later to get rid of them. Once you overcome the hold-ups you pave
            the path for growth!
          </Text>
          <br></br>
          <br></br>
          <Text h2 className={styles.numlist}>
            2. To explore growth opportunities
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Exploiting opportunities at the right timing is the key factor for
            the sustainable success of a company. A good digital marketing audit
            reveals untapped opportunities that you can take advantage of to
            stand out from the competition.
          </Text>
          <br></br>
          <br></br>

          <Text h2 className={styles.numlist}>
            3. To stay consistent with business goals
          </Text>
          <br></br>
          <Text span className={styles.content}>
            The sight of the main goals could be lost in the daily grind. Once
            the work starts to get tougher and some problems show up, most teams
            put aside the business objectives and tend to focus on the sooner
            deadlines. The problem is that the main goals are the provider of
            requirements; it&apos;s your roadmap. Hence, it&apos;s important to
            run a constant digital marketing audit to remind you of your
            company&apos;s milestones, so you don&apos;t stray.
          </Text>
          <br></br>

          <br></br>
          <Text h2 className={styles.numlist}>
            4. To rekindle the team&apos;s spirits
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Dealing with the same product/service every single day blinds the
            team to its bright side. The digital marketing audit is the perfect
            way to remind them of the quality of the product/service they are
            working on, which will help them to fall in love with what they are
            doing all over again.
          </Text>
          <br></br>
          <br></br>
          <Text h2 className={styles.numlist}>
            5. To highlight the company’s strengths
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Revealing the strong points of your company through a digital
            marketing audit helps to consolidate your position in the market.
            Showing your strengths boost your authenticity and build a unique
            value in the eyes of your clients.
          </Text>
          <br></br>
          <br></br>
          <Text h2 className={styles.numlist}>
            6. To Boost your ROI
          </Text>
          <br></br>
          <Text span className={styles.content}>
            By dropping what doesn&apos;t work and sticking with what works, you
            increase your company&apos;s efficiency and thus improve its
            long-term return on investment (ROI). The digital marketing audit is
            therefore a profitable deal that deserves consideration.
            <br></br>
            As you noticed the digital marketing audit acts preventively; it
            gives you a bird&apos;s-eye view of where your business stands, so
            you can fill gaps in your strategy and leverage your strengths. The
            question here is what a strong digital marketing audit means?
          </Text>
          <br></br>
          <br></br>
          <Grid.Container>
            <Grid align="center" md={4} sm={12}>
              <Image
                src="/images/blog19.2.svg"
                alt="Marketing Audit"
                height={200}
                width={350}
                objectFit="contain"
              />
            </Grid>
            <Grid alignItems="center" md={6} sm={12}>
              <Text span className={styles.content}>
                <span className={styles.span}> Well, there are </span>{" "}
                <span className={styles.spanB}>
                  5 characteristics that an audit should validate to be
                  consistent
                </span>
                .
              </Text>
            </Grid>
          </Grid.Container>
          <br></br>
          <Text h2 className={styles.numlist}>
            1. Structured
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Facing your company&apos;s imperfections would by no means be easy.
            But keep moving ahead without keeping an eye on how things stand,
            will never be a solution. Bottlenecks and weaknesses in a business
            obstruct growth, it could even crash your company. Scary thoughts,
            right? Therefore, weaknesses should be identified sooner rather than
            later to get rid of them. Once you overcome the hold-ups you pave
            the path for growth!
          </Text>
          <br></br>
          <br></br>
          <Text h2 className={styles.numlist}>
            2. Exhaustive
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Exploiting opportunities at the right timing is the key factor for
            the sustainable success of a company. A good digital marketing audit
            reveals untapped opportunities that you can take advantage of to
            stand out from the competition.
          </Text>

          <br></br>
          <br></br>
          <Text h2 className={styles.numlist}>
            3. Objective
          </Text>
          <br></br>
          <Text span className={styles.content}>
            The sight of the main goals could be lost in the daily grind. Once
            the work starts to get tougher and some problems show up, most teams
            put aside the business objectives and tend to focus on the sooner
            deadlines. The problem is that the main goals are the provider of
            requirements; it&apos;s your roadmap. Hence, it&apos;s important to
            run a constant digital marketing audit to remind you of your
            company&apos;s milestones, so you don&apos;t stray.
          </Text>
          <br></br>
          <br></br>
          <Text h2 className={styles.numlist}>
            4. Regular
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Dealing with the same product/ service every single day blinds the
            team to its bright side. The digital marketing audit is the perfect
            way to remind them of the quality of the product/service they are
            working on, which will help them to fall in love with what they are
            doing all over again.
          </Text>
          <br></br>
          <br></br>
          <Text h2 className={styles.numlist}>
            5. Contextual
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Revealing the strong points of your company through a digital
            marketing audit helps to consolidate your position in the market.
            Showing your strengths boost your authenticity and build a unique
            value in the eyes of your clients.
          </Text>
          <br></br>
          <br></br>

          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={12}
            sm={12}
          >
            <Image
              src="/images/blog19.3.svg"
              alt="Digital Marketing"
              height={200}
              width={350}
              objectFit="contain"
            />
          </Grid>
          <br></br>
          <Text span className={styles.content}>
            They said prevention is better than cure, so don&apos;t think twice
            and start your digital marketing audit, even if you think it is late
            it will be much better than never.
            <br></br>
            If you have more questions about digital marketing audits or any
            other digital marketing services, don&apos;t hesitate to contact us,
            it will be our pleasure to help you.
            <br></br>
            <br></br>
            Check how we can help you start your full
            <Link href="#">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.green}
              >
                &nbsp;digital marketing services
              </a>
            </Link>
          </Text>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog6Slider;

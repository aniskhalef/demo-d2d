import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const Blog22Slider = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Text h1 className={styles.title}>
          Rethink Businesses in a digital way
        </Text>
        <br></br>

        <Text span className={styles.content}>
          We&apos;re in the 21st century where we shifted from an industrial
          revolution to an economy based on information technology . This era is
          called The Digital Era where the Internet and solutions make the
          perfect combination and let things appear easier and clearer for
          businesses to evolve and ultimately engendered a whole career
          revolution.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container className={styles.content} justifyContent="center">
          <Grid md={1} sm={1} xs={2}>
            <Image
              height={25}
              width={25}
              src="/images/arrow.svg"
              alt=""
              objectFit="contain"
            />
          </Grid>
          <Grid md={11} sm={11} xs={11}>
            <Text span>
              AI, or Artificial Intelligence, lead to a second Digital
              transformation.
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        {/*******************/}

        <Grid.Container>
          <Grid justifyContent="center" alignItems="center" md={8}>
            <Text span className={styles.content}>
              Artificial Intelligence generated a positive revolution in the way
              we&apos;re doing businesses. Howerver, this technology is already
              affecting our everyday life to finally arrive to the Business
              revolution.
            </Text>
          </Grid>

          <Grid
            align="center"
            justifyContent="center"
            alignItems="center"
            md={3}
          >
            <Image
              height={350}
              width={350}
              objectFit="contain"
              src="/images/article3.svg"
              alt="Digital Marketing"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <Grid.Container className={styles.content} justifyContent="center">
          <Grid md={1} sm={1} xs={2}>
            <Image
              height={25}
              width={25}
              src="/images/arrow.svg"
              alt=""
              objectFit="contain"
            />
          </Grid>

          <Grid md={11} sm={11} xs={11}>
            <Text span>
              Artificial Intelligence has an immense impact on businesses within
              their progress.
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.title}>
          The impact of Artificial Intelligence on Businesses
        </Text>
        <br></br>
        <Text span className={styles.content}>
          All companies tend to stand next to AI since it’s offering many
          benefits that anyone would like to master
          <br></br>
          <br></br>
          <ul className={styles.list}>
            <li>- Relevant skills among various solutions.</li>
            <li>- Cost-effective solutions.</li>
            <li>
              - Computer armed with machine learning replaced the role of
              repetitive tasks that a human being used to do.
            </li>
          </ul>
        </Text>
        <br></br>
        <Text span className={styles.content}>
          AI Technology help reducing operational tasks, increase efficiency and
          improve customer experience.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          <br></br>
          <span style={{ fontWeight: 700 }}>Conclusion: </span>
          We can confirm that Artificial Intelligence has an important impact on
          business strategies.
        </Text>
        <br></br>
        <br></br>

        <Text span className={styles.title}>
          AI and Digital solutions
        </Text>
        <br></br>
        <Grid.Container justifyContent="center" alignItems="center">
          <Grid md={9}>
            <Text span className={styles.content}>
              DS is so easy for any enterpise to understand. It’s just like any
              other old strategy. It is about scaling up the business you’re
              doing and adapting it with your customer’s needs and expectations
              since the customer is in the center of any CRM.
            </Text>
          </Grid>
          <Grid md={3}>
            <Image
              height={200}
              width={200}
              objectFit="contain"
              src="/images/article3.1.svg"
              alt="AI and Digital Solutions"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.content}>
          Take advantage of Artificial Intelligence for an optimised Digital
          Solutions; a technology that facilitates solving problems. Basically,
          both the issue and the solution would take a Digital format;
        </Text>
        <br></br>
        <Text span className={styles.content}>
          To make things clearer, A DS Can take the form of:
        </Text>
        <br></br>
        <br></br>

        <Text span className={styles.titlev}>
          1. The solution to a problem
        </Text>
        <br></br>

        <Text span className={styles.content}>
          Check out this Article for more details
          <Link href="/blog/digital-problem">
            <a className={styles.href}>
              {" "}
              The &quot;Digital&quot; Problem has a very Digital Solution.
            </a>
          </Link>
        </Text>

        <br></br>
        <br></br>
        <Text span className={styles.titlev}>
          2. DS based on Marketing
        </Text>
        <br></br>
        <Text span>
          <ul className={styles.list}>
            <li>- Personalised Digital Solutions to the right audience.</li>
            <li>- Engaging with customers</li>
            <li>- Optimisation of SEO for valuable websites</li>
          </ul>
          <br></br>
        </Text>
      </Grid>
    </>
  );
};

export default Blog22Slider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";

import styles from "./styles.module.css";
import { useTranslation } from "next-i18next";
import Image from "next/image";
const Blog17Slider = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Text h1 className={styles.title}>
          Melding AI and Data
        </Text>
        <br></br>
        <Text span className={styles.content}>
          The question is how
          <Link href="/">
            <a target="_blank" className={styles.iruk}>
              {" "}
              Artificial Intelligence{" "}
            </a>
          </Link>
          and Data can be working together and what are the results they are
          contributing?
        </Text>
        <br></br>
        <br></br>

        <Grid.Container className={styles.content} justifyContent="center">
          <Grid md={1} sm={1} xs={2}>
            <Image
              src="/images/solidArrow.svg"
              alt="expertise"
              width={33}
              height={33}
              objectFit="contain"
            />
          </Grid>
          <Grid md={11} sm={11} xs={11}>
            <Text span>
              Melding these two areas will lead Businesses, Technologies, and
              maybe every work we’re dealing with to progress to finally become
              trendy.
            </Text>
          </Grid>
        </Grid.Container>

        <Grid align="center" md={12}>
          <Image
            src="/images/blog8.1.svg"
            alt="Artificial Intelligence and Data"
            width={350}
            height={360}
            objectFit="contain"
          />
        </Grid>
        <br></br>
        <Grid md={10}>
          <Text span className={styles.content}>
            As AI and Data can work together in order to achieve positive
            results.
            <br></br>
            In this case;
            <br></br>- Data is helping AI to become smarter
            <br></br>- Less human intervention for AI
          </Text>
        </Grid>
        <br></br>

        <Grid.Container className={styles.content} justifyContent="center">
          <Grid md={1} sm={1} xs={2}>
            <Image
              src="/images/blankArrow.svg"
              alt="expertise"
              className={styles.arrow1}
              width={33}
              height={33}
              objectFit="contain"
            />
          </Grid>
          <Grid md={11} sm={11} xs={11}>
            <Text span>
              The less AI is needing people to manage it, the more people will
              realize the potential of merging these two terms.{" "}
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.title}>
          How AI is used in big Data
        </Text>
        <Grid.Container>
          <Grid md={8} justifyContent="center" alignItems="center">
            <Text span className={styles.content}>
              Digital Solutions provides nowadays an amount of informations
              about customer behavior, likes and dislikes, personal preferences,
              as well as the activities he’s involved in. This was impossible to
              recognize years ago. This amount of Data is collected through
              different sources; Social media accounts and platforms, reviews,
              sharing or liking a content, even through the research that an
              individual does through his journey. This is called gathering
              user’s information. Let’s learn more about this trend.
            </Text>
          </Grid>
          <br></br>
          <br></br>
          <Grid align="center" md={4} sm={12}>
            <Image
              src="/images/blog8.2.svg"
              alt="Big Data"
              width={250}
              height={250}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <Grid>
          <Text span className={styles.titleB}>
            Gathering user’s information
          </Text>

          <Grid justifyContent="center" alignItems="center">
            <Text span className={styles.content}>
              The learning ability is one of the biggest assets of Artificial
              Intelligence. By knowing the different kinds of customer state of
              mind and the feedback of each one that considered as notable. The
              combination of AI and and big Data is due to the work of
              Artificial Intelligence and data analytics. Gathering Data in
              order to generate new rules and decisions for an ultimate goal
              which is genrate growth to Businesses within Machine learning,
              deep learning.
            </Text>
          </Grid>
        </Grid>

        <Grid align="center">
          <Image
            src="/images/blog8.3.svg"
            alt="AI gathering user's information"
            width={350}
            height={250}
            objectFit="contain"
          />
        </Grid>
        <Grid md={12}>
          <Text span className={styles.title}>
            Conclusion
          </Text>
          <Grid md={12} justifyContent="center" alignItems="center">
            <Text span className={styles.content}>
              Big Data will remain extending and evolving and Artificial
              Intelligence will go further and become achievable for automating
              duties. And it will become stronger and a larger field as long as
              Data is available to learn and anlyse.
            </Text>
          </Grid>
        </Grid>
        <br></br>
        <Text span align="center" className={styles.titleB}>
          AI Goals
        </Text>
        <br></br>
        <br></br>
        <Grid.Container justifyContent="center" alignItems="center">
          <Grid md={3} sm={6} xs={6}>
            <Image
              src="/images/1.AL.png"
              alt="Automated Learning"
              objectFit="contain"
              width={400}
              height={400}
            />
          </Grid>
          <Grid md={3} sm={6} xs={6}>
            <Image
              objectFit="contain"
              width={400}
              height={400}
              src="/images/2.ML.png"
              alt="Machine Learning"
            />
          </Grid>

          <Grid md={3} sm={6} xs={6}>
            <Image
              objectFit="contain"
              width={400}
              height={400}
              src="/images/3.NLP.png"
              alt="Natural Language Processing"
            />
          </Grid>

          <Grid md={3} sm={6} xs={6}>
            <Image
              objectFit="contain"
              width={400}
              height={400}
              src="/images/4.Robotics.png"
              alt="Robotics"
            />
          </Grid>
        </Grid.Container>
      </Grid>
    </>
  );
};

export default Blog17Slider;

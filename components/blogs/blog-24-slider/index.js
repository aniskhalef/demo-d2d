import React from "react";
import { Grid, Text } from "@nextui-org/react";
import styles from "./styles.module.css";
import { useTranslation } from "next-i18next";
import Image from "next/image";
const Blog24Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            The “Digital” Problem has a very Digital Solution.
          </Text>

          <br></br>
          <Text span className={styles.content}>
            Well, the answer has become pretty obvious : Artificial
            Intelligence. It is a competitive advantage, for now, and will be a
            straight up necessity as we unveil, each day, more about its
            capacity. The use of Artificial intelligence has progressively shown
            us that it will always succeed in what we fail at:
          </Text>
          <Grid>
            <Grid.Container justifyContent="center" alignItems="center">
              <Grid md={8}>
                <Text span className={styles.content}>
                  <ul className="dashed">
                    <li>- Delivering accurate Results</li>
                    <li>- Understanding human behavior</li>
                    <li>- Having an unlimited memory (Big Data)</li>
                    <li>- Learning Frequency & Capacity (Machine-Learning)</li>
                    <li>- Processing Mechanics</li>
                  </ul>
                </Text>
              </Grid>
              <Grid justifyContent="center" alignItems="center" md={4}>
                <Image
                  height={250}
                  width={250}
                  src="/images/articleblog1.svg"
                  alt="Artificial Intelligence Robot"
                  objectFit="contain"
                />
              </Grid>
            </Grid.Container>
          </Grid>
          <Text span className={styles.content}>
            And this is the main reason why early adopters of this technology
            will succeed. For any marketer outthere, the awareness of the new
            personalization trend made it quite clear that it is not just a
            “Trend” but rather a whole adaptation for the increase of the social
            standards that made it competitively unacceptable to stick with the
            basics we knew.
          </Text>
          <br></br>
          <br></br>
          <Grid.Container className={styles.content} justifyContent="center">
            <Grid md={1} sm={1} xs={2}>
              <Image
                height={25}
                width={25}
                src="/images/arrow.svg"
                alt=""
                objectFit="contain"
              />
            </Grid>
            <Grid md={11} sm={11} xs={10}>
              <Text span>
                We are now facing a picky consumer with no sense of loyalty to a
                specific brand, a consumer that we have to understand
              </Text>
            </Grid>
          </Grid.Container>
          <br></br>

          <Text span className={styles.title}>
            The Creation of Digital solutions utilizing Artificial Intelligence
            as a response to the problem:
          </Text>
          <br></br>

          <Text span className={styles.content}>
            Not every company has the ressources, capital, technology or
            knowledge that allows them to implement their own AI based solutions
            and thus, you might of seen multiple ads or service providers
            offering some type of AI driven marketing solution. Our duty relies
            on understanding what it actually is and adapting it into our
            current organizational strategy as organically as possible since it
            is currently being developed and further advanced, all we have to do
            is to follow the natural flow to assure a steady growth on a purely
            Digital scale.
          </Text>
          <br></br>
          <Text span className={styles.titlee}>
            {" "}
            Think of it this way !
          </Text>
          <br></br>

          <Grid align="center">
            <Image
              src="/images/blogarticle2.png"
              alt="Digital Solution"
              height={350}
              width={350}
              objectFit="contain"
            />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog24Slider;

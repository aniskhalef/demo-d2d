import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog5Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            5 STEPS FOR A SUCCESSFUL DIGITAL MARKETING AUDIT{" "}
          </Text>
        </Grid>
        <br></br>
        <Text span className={styles.content}>
          Dear reader, our goal is to inform you about subjects we think will be
          helpful to you as a business owner and to your business itself; as we
          mentioned in our latest
          <Link href="/blog/strong-digital-marketing-audit">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.oranger}
            >
              {" "}
              article
            </a>
          </Link>
          , running a digital marketing audit is a must for all types of
          companies.
        </Text>
        <br></br>

        <Text span className={styles.content}>
          So now you know how crucial the digital marketing audit is for a
          healthier business, it is time therefore to dig deeper into the
          action.
        </Text>
        <br></br>

        <Text span className={styles.content}>
          We will be talking here about the steps you should follow to elaborate
          your digital marketing audit. An audit recommends an action plan to
          uplift the company’s performance.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container alignItems="center">
          <Grid justifyContent="center" md={4} sm={12}>
            <Text span className={styles.content}>
              <span className={styles.span}> Here are </span>{" "}
              <span className={styles.spanB}>
                {" "}
                5 steps to perform your digital marketing audit{" "}
              </span>
            </Text>
          </Grid>
          <Grid justifyContent="center" align="center" md={6} sm={12}>
            <Image
              src="/images/blog20.1.svg"
              alt="Digital Marketing Audit"
              height={200}
              width={400}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          1. Determination of goals and key metrics
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This phase of your digital marketing audit depends on your vision,
          it’s subjective since the marketing goal of one company may be
          increasing brand awareness while another company may be centered
          around brand engagement.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          After setting your marketing goal, ask yourself what KPIs (Key
          Performance Indicators) you want to focus on; what you want to
          measure? Google analytics alone can track more than 200 metrics added
          to countless other tools that you can use.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          But don’t overwhelm yourself and try to measure everything. Ask
          yourself what are the top 3 things you want to measure for each
          channel?
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Of course, your key metrics should be closely tied to your main goals.
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          2. Gathering performance data
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Now you need to get statistics from your channels aligned with the
          metrics you set in the step before. The goal of this phase is to
          compare your current state with your MOS (measures of success).
        </Text>
        <br></br>
        <Text span className={styles.content}>
          NOTE: a measure of success is the standard that you define by which
          you assess whether you are achieving your goals or not.
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.bold}>
          And we have 3 types of performances data needed to be gathered:
        </Text>
        <br></br>
        <Text span className={styles.content}>
          - &nbsp; Website performance: you can measure your click-through rate,
          bounce rate, average session duration, conversion rate, page load
          time, etc.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          - &nbsp; Email performance: here you can gather the open rate,
          unsubscribe rate, email forwarding rate, conversion rate, clickthrough
          rate, etc.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          - &nbsp; Social media performance: you can track likes, shares,
          impressions, click-through rate, etc.
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.bold}>
          NOTE: The meaning of bounce rate differs from web to email
          performance:
        </Text>
        <br></br>
        <Text span className={styles.content}>
          • &nbsp; In the web, bounce rate is the percentage of visitors that
          leave a page without performing a specific action.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          • &nbsp; Email bounce rate refers to the percentage of emails that
          don’t get delivered to some of the email addresses in your mailing
          list.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Numbers never lie, so whatever the results you find, accept them and
          base your actions on them.
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          3. Evaluation of content marketing strategy
        </Text>
        <br></br>
        <Text span className={styles.content}>
          According to the Content Marketing Trends report, companies that
          document their content marketing strategy are almost 50% more
          effective than those that don’t.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          It doesn’t only give you clarity and a sense of direction, but also,
          it’s useful for your digital marketing audit. In this step, you should
          revisit your strategy by checking:
        </Text>
        <br></br>
        <Text span className={styles.content}>
          • Your keywords whether they are relevant to what your customers are
          searching for or not.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          • Your distribution whether your channels of distribution are
          effective or not
        </Text>
        <br></br>
        <Text span className={styles.content}>
          • Your content type if it matches your digital marketing goal;
          increase your brand awareness or generate leads or build links.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          It’s fine to have more than one objective. The matrix below can help
          you to classify your content according to your marketing goals:
        </Text>
        <br></br>
        <br></br>

        <Grid
          justifyContent="center"
          align="center"
          alignItems="center"
          md={12}
          sm={12}
        >
          <Image
            height={200}
            width={300}
            objectFit="contain"
            src="/images/blog20.2.svg"
            alt="content marketing matrix"
          />
        </Grid>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          4. Evaluation of the competitive landscape
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Keeping an eye on your competitors is a foundational tactic to have
          insights into who they are, the threats that they pose to your
          company, and their strategy. It will give you a competitive edge.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          First, identify your competitors; it is a good practice to conduct a
          search from period to period to avoid surprises. Then, dig into their
          content to know the tactics and tools they are using. It helps you to
          inspire by them.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Remember that inspiration doesn’t mean copy-paste. You can also find
          your competitors’ gaps to take advantage of.
        </Text>
        <br></br>
        <br></br>

        <Text h2 className={styles.numlist}>
          5. Examination of messaging and positioning
        </Text>
        <br></br>
        <Text span className={styles.content}>
          We mean by messaging the way you communicate your values to your
          prospects. While positioning means the creation of a positive
          impression in the minds of your customers.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This phase of your digital marketing audit has the same importance as
          the steps above. By digging into action most businesses forget to
          revisit their positioning and messaging.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          They tend to show the benefits and features of their products
          forgetting to build a position. Ignoring this step may lead to product
          failure.
        </Text>
        <br></br>

        <Text span className={styles.content}>
          • &nbsp; Your value proposition: you can try the STEVE BLANK formula
          to elaborate your value proposition: “we help (X) do (Y) by doing
          (Z)”. This statement will remind you of the reason for your existence.
          It emphasizes why a customer will buy your product.
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.content}>
          • &nbsp; Your unique selling proposition: it is the one thing that
          makes you stand out from your competitors.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          • &nbsp; In terms of messaging, you must check out these two factors:
        </Text>
        <br></br>
        <br></br>

        <Text span className={styles.content}>
          <span className={styles.bold}>1. </span> Your consistency: in a
          digital marketing audit it is crucial to verify whether you are
          conserving the same way of messaging across your channels or not. If
          you don’t, it is better to fix this. Because inconsistency confuses
          customers.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          <span className={styles.bold}>2. </span> Customer focus: of course,
          any product should fulfill a need. Therefore, you should communicate
          the value of your product using a language that fits your target.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          We can’t deny that it is a headache task, but at the end of the day,
          you can conclude that the digital marketing audit is a decisive tactic
          that should be scheduled on your company’s to-do list.
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.content}>
          Don’t hesitate, we can help you figure this out. Not only to conduct
          your digital marketing audit but also to start your full
          <Link href="#">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.oranger}
            >
              {" "}
              digital marketing strategy
            </a>
          </Link>
          .
        </Text>
      </Grid>
    </>
  );
};

export default Blog5Slider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Image from "next/image";
import styles from "./styles.module.css";

const Blog2Slider = () => {
  return (
    <>
      <Grid>
        <Text h1 className={styles.title}>
          Data Science Objectives
        </Text>
      </Grid>
      <br></br>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid className={styles.container}>
          <Grid>
            <Text span>
              A data scientist aims to explore, sort, and analyze data in order
              to help your company to take advantage of trends that will help to
              make better decisions.
            </Text>
            <br></br>
            <Text span className={styles.green}>
              This blog’s main goal is to remind you how important is to have a
              data scientist on your team. Therefore, we will dive deeper into
              the objectives and skills of a data scientist.
            </Text>
            <br></br>

            <Text span>
              First, let’s check whether you really need a data scientist for
              your company or not.
            </Text>

            <br></br>
            <br></br>

            <Grid md={12}>
              <Image
                height={13}
                width={50}
                src="/images/arrow23.1.svg"
                alt=""
                objectFit="contain"
              />
              <Text span>
                Do you cross large amounts of raw information from day to day?
              </Text>
            </Grid>

            <Grid.Container md={12}>
              <Image
                height={13}
                width={50}
                src="/images/arrow23.1.svg"
                alt=""
                objectFit="contain"
              />
              <Text span>
                Do you need to follow trends and be up-to-date to overcome your
                competitors?
              </Text>
            </Grid.Container>

            <Grid.Container md={12}>
              <Image
                height={13}
                width={50}
                src="/images/arrow23.1.svg"
                alt=""
                objectFit="contain"
              />
              <Text span>
                Do you think that you are facing complex data that you can’t
                interpret?
              </Text>
            </Grid.Container>

            <br></br>
            <br></br>
            <Text span>
              If the answer is yes, then you have the urge to hire a data
              scientist.{" "}
              <span className={styles.oranger}>
                We can help you find the right profile
              </span>
              , but first, keep reading to know how he will help you.
            </Text>
            <br></br>
            <br></br>

            <Text span>
              • First a data scientist starts work in your company, he will
              identify the valuable data sources. The objective here is to mine
              and extract usable data.
            </Text>
            <br></br>
            <Text span>
              • Then he will select data features that he will use to train the
              machine learning models. It’s the process of automatically
              selecting the features that contribute the most to predicting the
              output that you are interested in. The main goal is to automate
              the process of collecting data to optimize performance.
            </Text>
            <br></br>
            <Text span>
              • Then he will select data features that he will use to train the
              machine learning models. It’s the process of automatically
              selecting the features that contribute the most to predicting the
              output that you are interested in. The main goal is to automate
              the process of collecting data to optimize performance.
            </Text>
            <br></br>

            <Text span>
              • After getting pre-processed, the integrity of data should be
              processed, cleansed, and validated for analysis. In this step, the
              data scientist should have a final selection of data to be used.
            </Text>
            <br></br>

            <Text span>
              • Here comes the time of one of the most important tasks of a data
              scientist, the analysis of the large amounts of information which
              will help him to find patterns and solutions. The importance of
              this task comes from the dependency of the following steps on it.
            </Text>
            <br></br>

            <Text span>
              • Based on his analysis, the data scientist should develop
              prediction systems and machine learning algorithms that will tend
              to become smarter with each iteration.
            </Text>
            <br></br>

            <Text span>
              • For the sake of highlighting his efforts, a great data scientist
              should present the results he found in a clear manner to a
              technical or non-technical audience.
            </Text>
            <br></br>

            <Text span>
              • After presenting facts and findings, the data scientist should
              propose solutions and strategies that will help to tackle business
              challenges.
            </Text>
            <br></br>

            <Text span>
              • And finally, a great data scientist should be up to collaborate
              with business and IT teams.
            </Text>

            <br></br>
            <br></br>
            <Text span className={styles.bold}>
              To meet these objectives a data scientist should master a set of
              skills.
            </Text>
            <br></br>

            <Text span>
              • Programming skills: it is desirable to have knowledge of
              languages like R, Python, SQL, Hive, Pig, and preferably to be
              familiar with Scala, Java, or C++.
            </Text>
            <br></br>

            <Text span>
              • Statistics: proficiency in statistics is crucial for a great
              data scientist.
            </Text>
            <br></br>

            <Text span>
              • Machine learning: it is important to have good knowledge of
              methods like Naive, SVM, K-Nearest Neighbors, Decision Forests.
            </Text>
            <br></br>

            <Text span>
              • Strong math skills: good understanding of the fundamentals of
              linear algebra and multivariable calculus since they are the basis
              of the algorithm optimization techniques or predictive
              performance.
            </Text>
            <br></br>

            <Text span>
              • Mastering of data visualization tools: it is important to
              visually encode data using tools such as Matplotlib, ggplot,
              d3.js.
            </Text>
            <br></br>
            <Text span>
              • Problem-solving aptitude, great business sense, and analytical
              mind.
            </Text>
            <br></br>
            <Text span>
              • Effective communication skills: it is extremely important to
              clearly communicate and present his efforts and data findings.
            </Text>
            <br></br>
            <br></br>
            <br></br>

            <Grid.Container alignItems="center">
              <Grid md={6} sm={12} xs={12}>
                <Text span className={styles.oranger}>
                  This figure shows the extreme importance of well-explained
                  data.
                </Text>
              </Grid>

              <Grid md={6} sm={12} xs={12}>
                <Image
                  height={500}
                  width={200}
                  src="/images/blog23.svg"
                  alt=""
                  objectFit="contain"
                />
              </Grid>
            </Grid.Container>
            <br></br>
            <Grid.Container>
              <Grid
                md={1}
                sm={12}
                xs={12}
                alignItems="flex-start"
                css={{ marginTop: "0.5em" }}
              >
                <Image
                  height={20}
                  width={50}
                  src="/images/arrow23.2.svg"
                  alt=""
                  objectFit="contain"
                />
              </Grid>

              <Grid md={10} sm={12} xs={12}>
                <Text span>
                  Data science is a mixture of science and computing that aims
                  to generate insights. It is a young field that is growing fast
                  over the last few years due to the boom in the volume of data
                  stored by companies.
                </Text>
              </Grid>
            </Grid.Container>
            <Text span>
              <br></br>
              Due to its high technicity, you may struggle to find the right
              profile of a data scientist who will be able to fulfill all these
              objectives and have these skills.
            </Text>
            <br></br>
            <Text span>
              <br></br>
              We will gladly help you figure this out. We already have a team of
              experienced data scientists that we put at your disposal to help
              you take your business to the next level. Don’t hesitate to
              contact us.
            </Text>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog2Slider;

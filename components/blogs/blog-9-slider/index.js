import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog9Slider = () => {
  return (
    <>
      <Grid md={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            Can Artificial Intelligence replace human intelligence ?
          </Text>
          <br></br>
        </Grid>

        <Grid.Container>
          <Grid justifyContent="center" md={8}>
            <Text span className={styles.content}>
              Artificial intelligence is the computerized imitation of Human
              Intelligence(HI). It is the method through which a robot or a
              computer accomplishes jobs that are normally performed by people
              owing to the need for human intellect.
              <br></br> AI may now be found everywhere and has become a part of
              our daily life. But, aside from becoming a part of our journey and
              assisting us in improving the efficiency of our tasks. It became
              closer to human intelligence by participating in discussions,
              displaying inventiveness, and even replicating people emotions.
            </Text>
          </Grid>

          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={4}
            sm={12}
          >
            <Image
              src="/images/blog16.1.svg"
              alt="AI technology"
              height={200}
              width={250}
              objectFit="contain"
            />
          </Grid>

          <Text span className={styles.blue}>
            But the question is whether this technology can surpass its creator
            or not?
          </Text>
        </Grid.Container>
        <br></br>
        <Text span className={styles.content}>
          Let's start with HI : it's the mind's ability to learn from past
          experiences and adapt to new circumstances.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          In brief, the human brain typically learns hundreds of distinct
          abilities during life. So, how can Artificial Intelligence replace us?
        </Text>
        <br></br>
        <Text span className={styles.content}>
          No matter how far Artificial Intelligence has progressed, it will
          never be able to replace HI. The most essential trait of human
          intelligence is our ability to think concerning ourselves and others
          and make judgments based not only on data but also on instinct, which
          is a major component of human connection.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          It can help jobs become more efficient: It can assist to healthcare,
          offer personalized services with AI Marketing, can also be merged with
          IOT, and so on .<br></br>
          To learn more on what Artificial intelligence can do, Check out this
          article:
          <Link href="/blog/Artificial-Intelligence-Trends">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.green}
            >
              {" "}
              Artificial Intelligence Trends
            </a>
          </Link>
          .
        </Text>
        <Grid.Container alignItems="center">
          <Grid md={8} justifyContent="center">
            <Text span className={styles.content}>
              We have seeked to implant intelligence in machines in order to
              make our jobs easier. Bots, humanoids, robots, and digital people
              outperform or collaborate with humans in a variety of ways. These
              AI-driven apps outperform humans in terms of execution speed,
              operational ability, and accuracy, while also being extremely
              significant in repetitive and boring occupations. HI, on the other
              hand, is associated with flexible learning and experience.
            </Text>
          </Grid>
          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={4}
            sm={12}
          >
            <Image
              src="/images/blog16.2.1.png"
              alt="AI technology"
              width={250}
              height={400}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <Text span className={styles.content}>
          It does not always rely on pre-fed data, such as that necessary for
          AI. Human memory, computational capacity, and the human body as an
          entity may appear tiny in comparison to the hardware and software
          infrastructure of the machine. However, the depth and layers existing
          in our brains are significantly more intricate and clever, which
          robots will never ever be able to match, at least in the long term!
        </Text>
        <Text span className={styles.content}>
          To summarize, AI can assist us improve our work efficiency, help
          businesses expand, and even support human jobs speed and succeed.
          <br></br>
          However, humans cannot be replaced.
        </Text>
      </Grid>
    </>
  );
};

export default Blog9Slider;

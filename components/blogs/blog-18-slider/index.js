import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog18Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid sm={12}>
          <Text span className={styles.title}>
            Artificial Intelligence Trends
          </Text>
        </Grid>
        <br></br>

        <Grid.Container alignItems="center">
          <Grid justifyContent="center" alignItems="center" md={5} sm={12}>
            <Text span className={styles.content}>
              <ul className="dashed">
                <li>
                  <Link href="#">
                    <a
                      className={styles.link}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      - Digital Marketing
                    </a>
                  </Link>
                </li>
                <li>- Healthcare</li>
                <li>- Finance</li>
                <li>- Accounting</li>
                <li>- Etc..</li>
              </ul>
            </Text>
            <br></br>
          </Grid>
          <Grid md={5} sm={12}>
            <Image
              src="/images/blog7.1.svg"
              alt="AI in healthcare"
              height={350}
              width={300}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>

        <Text span className={styles.content}>
          It also made things easier on entrepreneurs to evaluate their work.
          Added to this, companies tend to choose
          <Link href="https://irukai.com">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.iruk}
            >
              {" "}
              Digital solutions{" "}
            </a>
          </Link>{" "}
          adapted to their businesses in order to be on page with their target
          audience to match customer’s needs or expectations. Companies do also
          devote in Artificial Intelligence not only to have the ability of
          measuring performance of their work, but also to be as close as they
          can be to their audience.
        </Text>

        <Text span className={styles.content}>
          Let’s take a look at what we really need to know about AI evolvement
          over the time and what are its trends.
        </Text>
        <br></br>
        <br></br>

        <Grid md={11}>
          <Text span className={styles.title}>
            AI in Healthcare
          </Text>

          <Grid justifyContent="center" alignItems="center">
            <Text span className={styles.content}>
              AI allowing people all over the world to have the best patient
              care and it made things easier to manipulate; Detecting the
              disease, preveting it, as well as healing. Also, the ability of
              Artificial Intelligence of collecting Data about client’s history
              and latest informations and classify them to finally exploit,
              interpret, and evaluate it in a way that enable concerned people
              to know their capabilities in their administration.
            </Text>
          </Grid>
          <br></br>
          <Text span className={styles.title}>
            Personalized Services with AI in Marketing
          </Text>

          <Grid justifyContent="center" alignItems="center">
            <Text span className={styles.content}>
              One of the biggest
              <Link href="/">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.home}
                >
                  {" "}
                  Artificial Intelligence{" "}
                </a>
              </Link>
              trends is personalisation. Adapting personalised services is one
              of the most valuable tasks for marketers. In other terms, being
              able to touch every customer through Digital Solutions based on
              Data in a targeted way generate lots of profits. This is by
              interpreting and analyzing the ritual of users, what are they
              searching for online, what keywords are they using. This is not
              only beneficial for Businesses to offer their content or their
              services to the right audience that is interesting in it. But also
              useful to users to find the informations they’re looking for so
              much easier which generate a better experience for them.
            </Text>
            <br></br>
          </Grid>

          <br></br>
          <br></br>
          <Grid align="center" sm={12}>
            <Image
              src="/images/blog7.2.svg"
              alt="Personalisation with AI"
              height={200}
              width={300}
              objectFit="contain"
            />
          </Grid>
          <br></br>
          <br></br>
        </Grid>
        <Grid>
          <Text span className={styles.title}>
            Merging AI and IOT
          </Text>
          <Grid justifyContent="center" alignItems="center">
            <Text span className={styles.content}>
              These two technologies are evolving through time and became
              important nowadays in this Digital era. Imagine melding them up
              together. Let us first define IOT; The Internet Of Things, It's a
              huge universe to talk about but let us be specific and brief. It's
              about using the internet beyond the limits of smartphones and
              computers. It's the ability of using Data and transferring it
              without the need of a human in this task, whether it was a
              human-to-human interaction or human-to-machine interaction.
              Melding AI and IOT signify; Using the collected Data by IOT so
              that AI take out values of this Data to finally create useful
              results. This interpretation and analyse made by AI is taking IOT
              systems to another level by making it more Intelligent.
            </Text>
          </Grid>
        </Grid>
        <br></br>
        <Grid>
          <Text span className={styles.title}>
            Facial Recognition
          </Text>
          <Grid>
            <Grid>
              <Text span className={styles.content}>
                Facial Recognition became trending past the years. Incorporating
                this technology in smartphones and other devices is not only
                about security. With Deep Learning Technologies, this technology
                is going further, it’s going to provide more personalised topics
                and Marketing Actions toward consumer’s. This will be trending
                for the near future.
              </Text>
            </Grid>
            <br></br>
            <Grid align="center">
              <Image
                src="/images/blog7.3.svg"
                alt="Facial Recognition"
                height={300}
                width={300}
                objectFit="contain"
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog18Slider;

import React from "react";
import { Grid, Text } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";
import Image from "next/image";
const Blog13Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid md={11} sm={12}>
          <Text h1 className={styles.title}>
            Artificial Intelligence and Robots
          </Text>
          <br></br>
          <Text span className={styles.content}>
            It may be obvious, but
            <Link href="/">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.orange}
              >
                {" "}
                Artificial Intelligence{" "}
              </a>
            </Link>
            and robotics are two different disciplines.
          </Text>
        </Grid>
        <br></br>
        <Text span className={styles.titleV}>
          What is Robotics
        </Text>
        <Grid.Container>
          <Grid alignItems="center" md={7}>
            <Text span className={styles.content}>
              Robotics is the intersection of engineering, science and
              technology where machines, called robots, are built to achieve
              programmed duty without the interaction of a human-being.
            </Text>
          </Grid>
          <Grid
            align="center"
            justifyContent="center"
            alignItems="center"
            md={4}
          >
            <Image
              height={250}
              width={350}
              objectFit="contain"
              src="/images/blog12.1.svg"
              alt="Robotics"
            />
          </Grid>
        </Grid.Container>
        <Grid>
          <Text span className={styles.bold}>
            What is a robot?
          </Text>
          <br></br>
          <Text span className={styles.content}>
            It’s the product of the robotics field. By programming it to handle
            monotonous tasks (like building cars for example) But now, since the
            evolvement, this field solved many tasks that used to be done by a
            human, like cleaning houses, and assisting to surgeries, Etc..
            Still, we can not find a single robot that does all the work. Each
            robot has a job to offer and a problem to solve. Furthermore, each
            one has a level of autonomy, going from a one that needs to be
            supervised and controlled by the humans to fully-autonomous ones
            that don’t need any external interaction.
          </Text>
          <br></br>
          <br></br>
          <Text span className={styles.titleV}>
            The difference between Artificial Intelligence and robotics
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Robotics and AI are different thing. Beginning with AI, it’s where
            systems imitate human minds in order to learn, solve problems, as
            well as make decisions, without the need of humans interacion, and
            do not need to be programmed. To learn more about Artificial
            Intelligence, take a look at this article «
            <Link href="/blog/Artificial-Intelligence-Trends">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.green}
              >
                {" "}
                Artificial Intelligence Trends
              </a>
            </Link>{" "}
            ».
            <br></br>
            However, as mentionned above, Robotics are programmed and created to
            perform a very determined tasks. These tasks can be related with ai
            but it’s not a necessity. Robots can function without the
            intervention of AI. But it can be very beneficial to robots in order
            to make them intelligent. Let’s take a look on some examples of AI
            applied to robotics
          </Text>
        </Grid>
        <br></br>

        <Text span className={styles.bold}>
          Examples of robotics using AI
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.titleV}>
          The Robot Vaccum
        </Text>
        <br></br>
        <Text span className={styles.content}>
          The use of AI help robot navigate around the house in order to clean
          surfaces, and has the ability to see and hear when people are abscent
          thanks to a periscope camera.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container>
          <Grid justifyContent="center" md={7} sm={12}>
            <Grid>
              <Text span className={styles.titleV}>
                Robotics in healthcare
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Since robots don’t get tired it’s very beneficial to use them in
                healtcare field. To assist at surgeries and perform at
                operations and can be so steady during the operations. But they
                do not have the ability to do highly demanded activities like
                surgeries obviously. They can take care of lower skilled work
                and free up some time for professionals.
              </Text>
            </Grid>
          </Grid>

          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={5}
            sm={12}
          >
            <Image
              height={270}
              width={350}
              objectFit="contain"
              src="/images/blog12.2.svg"
              alt="Robotics in healthcare"
            />
          </Grid>
        </Grid.Container>
        <br></br>

        <Text span className={styles.titleV}>
          Robotic Delivery
        </Text>
        <br></br>
        <Text span className={styles.content}>
          These robots are conceptualized to carry items, navigate streets all
          by it self, and provide package delivery for consumers. They are
          equipped with sensors, Artificial Intelligence, as well as mapping
          systems in order to understand the road they’re taking and the
          location. We can also talk about the speed of these robots, they make
          local delivery faster and are more cost-efficient.
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.titleV}>
          Humanoid Robots
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Designed to interact with people, talk, share informations, and assist
          them in order to help them in retail stores. The robot talk in several
          languages, use emotion recognition AI to interpret human state of
          emotions for a better communication. It can for example end up the
          talk with a smile when a customer is happy by detecting joy emotion.
          This robot help people in stores; help them find products, sell,
          communicate, Etc..
        </Text>
      </Grid>
    </>
  );
};

export default Blog13Slider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog14Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text span h1 className={styles.title}>
            Machine Learning
          </Text>
        </Grid>
        <br></br>

        <Grid.Container>
          <Grid md={9}>
            <Grid>
              <Text span className={styles.titleB}>
                What is Machine Learning
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Machine Learning is a subfield of
                <Link href="/blog/Artificial-Intelligence-Trends">
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    className={styles.green}
                  >
                    {" "}
                    Artificial Intelligence{" "}
                  </a>
                </Link>
                that wil help it predict outcomes without being explicitly
                programmed. In other terms it’s the imitation of human behaviour
                in learning staff. ML uses Data in order to come out with new
                outputs. The Learning mechanism begins with looking for patterns
                in Data that will help in making decisions without human
                interactions or even a small assistance.
              </Text>
            </Grid>
          </Grid>
          <Grid align="center" justifyContent="center" md={2}>
            <Image
              height={400}
              width={350}
              objectFit="contain"
              src="/images/blog11.1.svg"
              alt="Machine Learning"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <Grid container alignItems="center" item md={10} sm={12}>
          <Text span className={styles.bold}>
            The thing about ML is the ability of computers to learn
            automatically without even being programmed to do so.
          </Text>
        </Grid>
        <br></br>
        <Text span className={styles.titleV}>
          How does it work?
        </Text>
        <br></br>
        <br></br>
        <Grid.Container gap={2} alignItems="center" align="center">
          <Grid item md={2} sm={10} xs={12}>
            <Image
              width={200}
              height={100}
              objectFit="contain"
              src="/images/inputData.svg"
              alt="Input Data"
            />
          </Grid>
          <Grid item md={2} sm={10} xs={12}>
            <Image
              width={200}
              height={100}
              objectFit="contain"
              src="/images/analyseData.svg"
              alt="Analyse Data"
            />
          </Grid>
          <Grid item md={2} sm={10} xs={12}>
            <Image
              src="/images/findPatterns.svg"
              alt="Find Patterns"
              width={200}
              height={100}
              objectFit="contain"
            />
          </Grid>
          <Grid item md={2} sm={10} xs={12}>
            <Image
              width={200}
              height={100}
              objectFit="contain"
              src="/images/predictions.svg"
              alt="Predictions"
            />
          </Grid>
          <Grid item md={2} sm={10} xs={12}>
            <Image
              width={200}
              height={100}
              objectFit="contain"
              src="/images/storeFeedBack.svg"
              alt="Store the feedback"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.titleB}>
          Machine Learning Methods
        </Text>
        <br></br>
        <Text span className={styles.content}>
          ML offers noticeable benefits for Artificial Intelligence technology.
          But which methods that goes with your business? There are many ML
          training methods, including:
        </Text>
        <br></br>
        <Text span className={styles.list}>
          <li>- Supervised Learning</li>
          <li>- Unsupervised Learning</li>
          <li>- Reinforcement Learning</li>
        </Text>
        <Text span className={styles.content}>
          Let’s learn more about them and discover what each one has to offer.
        </Text>
        <br></br>
        <br></br>

        <Grid.Container>
          <Grid justifyContent="center" item md={8} sm={12}>
            <Grid>
              <Text span className={styles.titleV}>
                Supervised Learning
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Also know as Supervised machine learning,
                <Link href="#">
                  <a
                    target="_blank"
                    className={styles.pink}
                    rel="noopener noreferrer"
                  >
                    {" "}
                    Data scientists{" "}
                  </a>
                </Link>{" "}
                supply algorithms with labeled trained data to predict the
                output. They also define what variables they want the algorithm
                to asees. Both the input and output of the algorithm is defined.
              </Text>
            </Grid>
          </Grid>

          <Grid
            container
            justifyContent="center"
            align="center"
            item
            md={4}
            sm={12}
          >
            <Image
              width={200}
              height={200}
              objectFit="contain"
              src="/images/blog11.2.svg"
              alt="Robot"
            />
          </Grid>
        </Grid.Container>
        <br></br>

        <Text span className={styles.titleV}>
          Unsupervised Learning
        </Text>
        <br></br>

        <Text span className={styles.content}>
          This method is used when the information used to train is not
          classified nor labeled. This type of ML involves algorithms that train
          on unlabeled data in order to describe a hidden structure or come out
          with a meaningful connection. The Data trained as well as the
          predictions are pre-established.
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.titleV}>
          Reinforcement Learning
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Reinforcement machine learning algorithms are a learning method that
          data scientists use to teach a machine to complete tasks or steps that
          are defined. Which mean a programmation of the algorithm to complete
          tasks with the ability of deciding on its own what step to take along
          the way.
        </Text>
      </Grid>
    </>
  );
};

export default Blog14Slider;

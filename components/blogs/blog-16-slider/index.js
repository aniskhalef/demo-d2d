import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import { useTranslation } from "next-i18next";
import Image from "next/image";
const Blog16Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Text h1 className={styles.title}>
          Combining AI with Marketing Automation
        </Text>
        <br></br>
        <Text span className={styles.content}>
          We talked about Artificial Intelligence trends and mentionned how it
          affected businesses in different fields in a very good way. Check out
          this article for more details
          <Link href="/blog/Artificial-Intelligence-Trends">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.iruk}
            >
              {" "}
              Artificial Intelligence Trends.
            </a>
          </Link>
        </Text>
        <br></br>
        <Grid align="center">
          <Image
            src="/images/blog9.1.svg"
            alt="AI and Marketing automation"
            width={350}
            height={250}
            objectFit="contain"
          />
        </Grid>
        <br></br>

        <Text span className={styles.title}>
          What is Marketing Automation
        </Text>
        <br></br>
        <Grid>
          <Text span className={styles.content}>
            To make it simple, MA is about using technology to automate tasks on
            marketers that will ease the flow as well as tasks will be easier
            and become more efficient. Although, it help generating marketing
            strategies and compaigns at a time in order to offer a great
            customer journey which is the goal of any marketer. Marketing
            Compaigns are sent with deversified channels; E-mail, Social
            channels, Etc..
          </Text>
        </Grid>
        <br></br>
        <Text span className={styles.title}>
          What is AI Marketing
        </Text>
        <Grid>
          <Text span className={styles.content}>
            To learn more about AI Marketing; Take a look on this article «
            <Link href="/blog/AI-Marketing">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.iruk}
              >
                {" "}
                How AI is Reshaping Marketing
              </a>
            </Link>{" "}
            »
          </Text>
        </Grid>
        <br></br>
        <Text span className={styles.title}>
          Combination of AI and Marketing Automation
        </Text>
        <Grid md={11}>
          <Text span className={styles.content}>
            Artificial Intelligence transformed the way we all do Businesses
            icluding Marketing. However, it plays a crucial function within
            Marketing Automation. Beginning with Content creation thanks to
            chatbots, the creation of your exact persona(s) became easier.
            Although, it help you generate leads easier, as well as a better
            User Experience which will lead to loyalty for your brand..{" "}
            <br></br> And other{" "}
            <a
              target="_blank"
              href="https://irukai.com"
              rel="noopener noreferrer"
              className={styles.green}
            >
              {" "}
              Digital Solutions
            </a>{" "}
            that in the end will engender growth for your business. These are
            just some AI simplified ways marketers use in their strategy. Also,
            Collecting Data is one of the important tasks that should be done,
            However, it’s all about how to manage collected Data into pragmatic
            insights. The use of this Data help Marketing personalize offers for
            its customers.
          </Text>
        </Grid>

        <Text span className={styles.titleA}>
          How can this comobination be beneficial to your Bussiness
        </Text>
        <br></br>
        <br></br>

        <Grid.Container justifyContent="center" alignItems="center">
          <Grid md={9} sm={12}>
            <Grid>
              <Text span className={styles.title}>
                AI in content creation
              </Text>
              <br></br>
              <Text span className={styles.content}>
                It changed the way we perceive content. With the rise of Social
                networks it is more important to share content for that your
                target audience know you exist in the Digital platforms as it
                became the most important strategy that a Business should think
                about. If you want to know how to rethink Digitally go ahead and
                take a look at this article
                <Link href="/blog/digital-businesses">
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    className={styles.iruk}
                  >
                    {" "}
                    Rethink Businesses in a Digital way
                  </a>
                </Link>
                .<br></br>
                To remain strong and be always updated some Businesses choose to
                meld AI in their Marketing strategies; It can help you collect,
                analyze Data of your customers to finally create content that is
                adaptative and pesonalized to your target audience.Content
                Creation can also be done automatically, How? With AI bots. As
                simple as that. Let us explain more this point; Autmatical
                Content can be done with or without human intervention. It can
                generate content with the use of Natural Language Processing by
                translating Data into eloquent content.
              </Text>
            </Grid>
          </Grid>

          <Grid align="center" md={3} sm={12}>
            <Image
              src="/images/blog9.2.svg"
              alt="AI in content creation"
              width={350}
              height={350}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.title}>
          AI and UX
        </Text>
        <br></br>
        <Text span className={styles.content}>
          User Experience is all about making sure of the experience the user is
          going through. It&apos;s all about offering the content that is a
          hundred percent adaptative to his anticipating. The use of chatbots is
          a good customer support channel that businesses can use to guarantee a
          well done experience with speed and precisions.
          <br></br>
          Although, Chatbots can also be used as Data generator; by collecting
          customers Data through conversations done with them.
        </Text>
        <Grid align="center">
          <Image
            src="/images/blog9.3.svg"
            alt="AI and user experience"
            width={350}
            height={350}
            objectFit="contain"
          />
        </Grid>
      </Grid>
    </>
  );
};

export default Blog16Slider;

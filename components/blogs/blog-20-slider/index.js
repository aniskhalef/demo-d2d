import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog20Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid md={12}>
          <Grid>
            <Text h1 className={styles.title}>
              Digitalisation of customer experience
            </Text>
            <Text span className={styles.content}>
              As we&apos;re in the Digital era, people nowadays prefer
              <Link href="https://irukai.com">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.iruk}
                >
                  {" "}
                  Digital solutions{" "}
                </a>
              </Link>
              since they are easier for their everyday life. That&apos;s why
              offering personalized experiences through Digital platforms
              generates opportunities for Businesses to respond to people&apos;s
              needs.
            </Text>
          </Grid>
          <Grid align="center">
            <Image
              alt="Digital Customer Experience"
              width={250}
              height={250}
              objectFit="contain"
              src="/images/blog5.1.svg"
            />
          </Grid>
        </Grid>
        <br></br>
        <Text span className={styles.title}>
          What is Digital Customer Experience (DCE)?
        </Text>
        <br></br>
        <Text span className={styles.content}>
          How to
          <Link href="/blog/digital-businesses">
            <a
              className={styles.href}
              rel="noopener noreferrer"
              target="_blank"
            >
              {" "}
              rethink Businesses in a Digital way
            </a>
          </Link>{" "}
          and the way we use Digital solutions and tools for a personalized CE
          digitally.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Digital Customer Experience is the way your customer sees or considers
          your brand.
          <br></br>Furthermore; the perception of your brand through your
          customer’s eyes.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Examples of Digital Platforms that Businesses can focus on for a
          better CE:
        </Text>
        <br></br>
        <br></br>
        <Grid.Container justifyContent="center" alignItems="center">
          <Grid alignItems="center" md={6} sm={12}>
            <Text span className={styles.content}>
              <ul className="dashed">
                <li>
                  - Social Media; (Facebook, Instagram, Pinterest, Twitter,
                  Etc..)
                </li>
                <li>- Websites</li>
                <li>- Mobile Applications</li>
                <li>- Blog Posts</li>
                <li>- Ebooks</li>
                <li>- Reviews</li>
                <li>- Etc..</li>
              </ul>
            </Text>
            <br></br>
          </Grid>
          <Grid md={4} sm={12}>
            <Image
              src="/images/blog5.2.svg"
              alt="Digital Marketing growth"
              width={250}
              height={250}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Grid align="left">
          <Grid md={11} justifyContent="center" alignItems="center">
            <Text span className={styles.content}>
              DCE is not only about managing or mastering Digital platforms,
              Businesses should also take into consideration customer
              expectations in terms of personalization . However, customers look
              for a personalized journey that goes with their needs and
              desires.. That’s why businesses have to be aware of this and
              improve it. Companies that pay attention and work hard on the
              consumer journey always come out with amazing results.
            </Text>
          </Grid>
          <br></br>
          <Text span className={styles.content}>
            The customer journey can be defined in three simple stages:
          </Text>
          <br></br>
          <br></br>
          <br></br>
          <Grid align="center">
            <Image
              width={400}
              height={300}
              objectFit="contain"
              src="/images/blog5.3.svg"
              alt="Customer journey"
            />
          </Grid>
          <br></br>
          <br></br>
          <Grid md={12}>
            <Text span className={styles.content}>
              Businesses can use this journey to create opportunities through
              every stage, and this is by creating content that is adaptative to
              each stage. To do so, let’s begin with the first stage; which is
              the Awareness phase, marketers have to{" "}
              <span className={styles.bold}> Attract </span>customers by
              creating Digital content that goes with the problem experienced.
              Second; according to the Consideration Stage, a marketer is
              responsible for <span className={styles.bold}> engaging </span>{" "}
              customers by creating Digital Solutions to the problem. Finishing
              with the final stage of your customer which is the Decision stage,
              in this phase, marketers have to{" "}
              <span className={styles.bold}> Delight </span> their clients with
              content that answers on customer’s questions about your products
              or services.
            </Text>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog20Slider;

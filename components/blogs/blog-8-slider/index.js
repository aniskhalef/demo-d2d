import React from "react";
import { Grid, Text } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog8Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            AI ethics
          </Text>
          <br></br>
        </Grid>

        <Grid>
          <Grid>
            <Text span className={styles.content}>
              The term "robot ethics" (roboethics) refers to the morality of how
              humans design, it is a concept that expresses how robots are
              created and how to use them.
            </Text>

            <Text span className={styles.content}>
              The ethics of artificial intelligence match the ethics of robots.
              Robots are objects that can be sensed and seen with the naked eye,
              while artificial intelligence is a concept that summarizes how
              some mechanisms in modern technology work.
              <br></br>
              Not all robots work with artificial intelligence, and The ethics
              of robotics is the concept of using modern machines to benefit or
              harm humanity, which raises many questions about social justice.
              <br></br>
              Check this article :
              <Link href="/blog/Artificial-Intelligence-Trends">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.blue}
                >
                  {" "}
                  Artificial Intelligence and Robots{" "}
                </a>
              </Link>
              for more insights about AI and robots
            </Text>
          </Grid>
        </Grid>

        <Grid.Container gap={4} justifyContent="center" alignItems="center">
          <Image
            src="/images/blog17.2.jpg"
            alt="Artificial Intelligence"
            height={200}
            width={300}
            objectFit="contain"
          />
          <Grid md={8} sm={12}>
            <Text span className={styles.content}>
              In the end, no one can deny the usefulness of artificial
              intelligence, but the question we asked is whether this technology
              will be used for good or ill. Every day we watch the development
              of this technology with new and advanced developments, and we are
              sure that it improves our lives and how it changes our view of our
              surroundings.
              <br></br>
              It has contributed greatly to many fields, including medical,
              military, and educational, but there is a dark side to this coin
              As billionaire Elon Musk stated that the conflict between humans
              in World War III will be at the hands of artificial intelligence
              <span className={styles.bold}>
                {" "}
                “AI will be the best or worst thing ever for humanity.”{" "}
              </span>
            </Text>
          </Grid>
        </Grid.Container>
        <Grid>
          <Text span className={styles.content}>
            There is no solution to avoid these risks, everything is based on
            perceptions and expectations that we are witnessing in our current
            crises, and the matter depends on how the leaders of countries
            communicate with each other to achieve the common interest.
          </Text>
        </Grid>
      </Grid>
    </>
  );
};

export default Blog8Slider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog4Slider = () => {
  return (
    <>
      <Grid>
        <Text h1 className={styles.title}>
          YOUR DIGITAL MARKETING AUDIT TOOLBOX
        </Text>
      </Grid>
      <br></br>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Text span className={styles.content}>
          In the
          <Link href="/blog/when-should-you-run-a-marketing-audit">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.oranger}
            >
              {" "}
              case of considering a digital marketing audit
            </a>
          </Link>
          , and you are aware of its
          <Link href="/blog/strong-digital-marketing-audit">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.oranger}
            >
              {" "}
              importance
            </a>
          </Link>
          , here is a toolbox that will accompany you through
          <Link href="/blog/5-steps-for-a-successfu-digital-marketing-audit">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.oranger}
            >
              {" "}
              all the steps of your digital marketing audit
            </a>
          </Link>
          . Of course, these tools won’t do the work for you, but surely, they
          will make this journey easier.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This article will be divided into two parts; at first, we will provide
          you with free tools then the paid ones.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          The choice between free and paid tools depends on the budget and the
          size of your company. It is obvious that paid ones provide more
          features and insights. But free tools are often enough for beginnings.
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.pink}>
          Free tools:
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          1. For traffic and website insights
        </Text>
        <br></br>
        <Text span className={styles.content}>
          No one can deny the importance of having a website, but just having
          it, isn’t enough. It’s crucial to track it, with the help of these
          tools:
        </Text>
        <br></br> <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.1.svg"
              alt="Google Analytics"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <br></br>
            <Text>
              {" "}
              <Text h2 className={styles.span}>
                • Google Analytics
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Is one of the most powerful, robust, and sophisticated tools for
                monitoring traffic on your website. It becomes a must for any
                business wanting to expand and grow on the internet. It gives
                you insights into who are your website visitors, where they are
                coming from, and what they are looking for by showing you the
                pages they are spending the most of their time. It even can
                provide you with customized tips to improve your results. We
                highly recommend you this tool to succeed in your digital
                marketing audit.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.2.svg"
              alt="Google Search Console"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • Google Search Console
              </Text>
              <br></br>
              <Text span className={styles.content}>
                While google analytics is user-focused, google console is search
                engine oriented. It gives you insights that will help you to
                improve your presence and visibility on the search engine
                results page (SERPs).
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.content}>
          You may be wondering, what tool to choose? Well, the answer is it
          depends! It is up to you to determine what to focus on depending on
          the factors that matter the most to the success of your website.
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          2. For keyword selection and review
        </Text>
        <br></br>
        <Text span className={styles.content}>
          We mentioned in the 3rd step of your digital marketing audit is that
          you must check your keywords, here are 2 free tools that will help you
          through this:
        </Text>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.3.svg"
              alt="Google Keyword Planner"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • Google Keyword Planner
              </Text>
              <br></br>
              <Text span className={styles.content}>
                It is completely free to use. It helps you to build a strong
                keyword list by generating new ideas and showing you the search
                volume and trending of keywords. Also, it gives you forecasts on
                the performance cost based on the average bids.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.4.svg"
              alt="Ubersuggest"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • Ubersuggest
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Is a free chrome extension. It shows the monthly volume of
                searches of keywords and their cost per click (CPC). It gives
                keyword queries not only on Google but also on YouTube, Amazon,
                and other sites.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Text span className={styles.content}>
          Ubersuggest is more accurate for content creation; it helps to
          formulate sentences out of search terms. On the other hand, google
          keyword planner is more accurate for PPC marketers (pay per click),
          since it provides a set of variants of keywords that have similar
          search intent.
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          3. For page speed calculation and troubleshooting
        </Text>
        <br></br>
        <Text span className={styles.content}>
          While checking your website performance, it’s important to check the
          page speed of your website. Because it affects the user perception,
          about 45% of users abandon the navigation in 3 seconds.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.5.svg"
              alt="Google Pagespeed Insights"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • Google Pagespeed Insights
              </Text>
              <br></br>
              <Text span className={styles.content}>
                It shows you scores and results for both desktop and mobile,
                through performance diagnostics. It also offers suggestions for
                improvement.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br> <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.6.svg"
              alt="GTmetrix"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • GTmetrix
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Other than measuring your page speed, this tool offers you a
                comprehensive analysis of the performance of your website.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.content}>
          The difference here is that GTmetrix is more likely customizable; you
          can test options, you just have to register. For example, you can
          select screen resolution, location, connection speed, etc.) Google
          Pagespeed insights doesn’t provide this option.
        </Text>
        <br></br> <br></br>
        <Text h2 className={styles.numlist}>
          4. For a technical SEO audit
        </Text>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.7.svg"
              alt="Screamingfrog"
              height={100}
              width={130}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • Screamingfrog
              </Text>
              <br></br>
              <Text span className={styles.content}>
                This tool is a website crawler. It is a time and effort saver
                compared to manual crawling; it audits and extracts your
                website’s SEO issues. It can help you in finding broken links,
                errors, and redirects. It also analyses pages title and
                metadata. It crawls up to 500 URLs for free and an unlimited
                number in the paid version. And a lot of other features.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.content}>
          This analysis during your digital marketing audit is important; it
          helps you to detect your website issues so you can improve your onsite
          SEO.
        </Text>
        <br></br>
        <br></br>
        <Text span className={styles.pink}>
          Paid tools:
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          1. For brand sentiment analysis
        </Text>
        <br></br>
        <Text span className={styles.content}>
          It is important when running your digital marketing audit to keep an
          eye on your user experience by identifying what they like and dislike
          and analyzing their sentiments.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.8.svg"
              alt="Irukai"
              height={100}
              width={130}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • Irukai
              </Text>
              <br></br>
              <Text span className={styles.content}>
                It is a Tunisian solution that analysis your customer’s emotions
                through their comments. It also compares it to your competitors.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.9.svg"
              alt="Awario"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • Awario
              </Text>
              <br></br>
              <Text span className={styles.content}>
                Is monitoring software that crawls webpages and APIs to help you
                learn about your client mentions.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          2. For comprehensive SEO
        </Text>
        <br></br>
        <Text span className={styles.content}>
          We have mentioned quite a few times, the importance of analyzing your
          website performance while conducting a digital marketing audit. These
          tools will help you to audit in-depth your SEO.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.10.svg"
              alt="Ahrefs"
              height={100}
              width={125}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • Ahrefs
              </Text>
              <br></br>
              <Text span className={styles.content}>
                It is a fully featured SEO software suite that can help you to
                analyze your SEO health. It also helps you to conduct research
                for Google, YouTube, and amazon keywords. And even it provides
                you with the most performing content on a given topic.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.11.svg"
              alt="SEMrush"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • SEMrush
              </Text>
              <br></br>
              <Text span className={styles.content}>
                It is also an SEO tool that helps you conduct your keyword
                research, analyze your competitors and optimize your google ads.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Text span className={styles.content}>
          These two tools are head-to-head competitors; They, both, offer pretty
          much the same features. But we can say that SEMrush is more oriented
          to technical SEO analysis, while Ahrefs is more accurate for link
          building and chasing backlinks opportunities.
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          3. For analyzing competitors’ most profitable keywords
        </Text>
        <br></br>
        <Text span className={styles.content}>
          The analysis of your competition often opens your eyes to growth
          opportunities. Therefore, you might need these tools:
        </Text>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.12.svg"
              alt="SimilarWeb"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              <Text h2 className={styles.span}>
                • SimilarWeb
              </Text>
              <br></br>
              <Text span className={styles.content}>
                It shows you scores and results for both desktop and mobile,
                through performance diagnostics. It also offers suggestions for
                improvement.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Grid.Container gap={3} alignItems="center">
          <Grid>
            <Image
              src="/images/blog21.13.svg"
              alt="Spyfu"
              height={100}
              width={150}
              objectFit="contain"
            />
          </Grid>
          <Grid md={9}>
            <Text>
              {" "}
              <Text h2 className={styles.span}>
                • Spyfu
              </Text>
              <br></br>
              <Text span className={styles.content}>
                This tool helps you to track your competitors. It even helps you
                to define your closest competing websites.
              </Text>
            </Text>
          </Grid>
        </Grid.Container>
        <br></br>
        <Text span className={styles.content}>
          You should know that the tools that we mentioned, provide more
          features than what we introduced. But the goal of this article is to
          give you a glimpse of each tool; so that once you want to start your
          digital marketing audit you know what tools to use.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Of course, you’ll need in-depth info about each tool you’ll use. You
          can contact us to help you figure this out, and even throughout your
          whole digital marketing strategy.
        </Text>
      </Grid>
    </>
  );
};

export default Blog4Slider;

import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";
import Image from "next/image";
const Blog3Slider = () => {
  return (
    <>
      <Grid>
        <Text h1 className={styles.title}>
          Types of Digital Solutions
        </Text>
      </Grid>
      <br></br>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Text span className={styles.content}>
          We are pretty sure that you are already aware of its importance,
          therefore we will dedicate this article to the different types of
          digital solutions.
        </Text>
        <br></br>

        <Text span className={styles.content}>
          It’s not just you, we also asked ourselves before writing this blog:
          <Text span className={styles.blue}>
            &nbsp;why should someone care about the types of digital solutions?
            What is the point if a business owner knows that the solution he is
            providing, belongs to one type and not the other?
          </Text>
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Well, the answer is just simple. Due to our exposure to a massive
          amount of information every single day, we often fail to see the
          entire picture. We drop down important details that could be key
          factors for improvement. Therefore, this blog somehow puts together
          <br></br>
          the pieces of the puzzle. You can consider it also as a reminder; you
          can check whether you are aware of all these types or not, or maybe
          you start considering one new solution.
        </Text>
        <br></br>
        <br></br>

        <Grid.Container alignItems="center">
          <Grid md={4} sm={12}>
            <Text span className={styles.content}>
              <span className={styles.spanO}>
                {" "}
                Here are the 6 types of digital solutions{" "}
              </span>
            </Text>
          </Grid>
          <Grid md={6} sm={12}>
            <Image
              src={"/images/blog22.1.svg"}
              alt="Connecting two pieces of digital puzzle"
              height={200}
              width={300}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          1. Digital consulting
        </Text>
        <br></br>
        <Text span className={styles.content}>
          In a vigilant digital landscape where navigating is a challenge, this
          digital solution aims to enable businesses to better leverage their
          technology and data in order to boost their business value.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          The focus of digital consulting is on implementing, efficiently,
          business strategies across digital platforms. It is considered a
          game-changer for businesses.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          If you breathe and live technology and you like helping other
          businesses reap many rewards and achieve their goals, digital
          consulting is your win card. We recommend you consider this solution.
          Even if you are providing another solution. You can leverage your
          skills to use as a digital consultant for small, local businesses near
          you.
        </Text>
        <br></br>
        <br></br>
        <Grid.Container className={styles.content}>
          <Grid
            md={1}
            sm={12}
            xs={12}
            alignItems="flex-start"
            css={{ marginTop: "0.5em" }}
          >
            <Image
              height={20}
              width={50}
              src={"/images/blog22.2.svg"}
              alt=""
              objectFit="contain"
            />
          </Grid>
          <Grid md={11}>
            <Text span>
              We have discovered together in a previous blog one of the many
              sides of digital consulting; the digital marketing audit:{" "}
              <Link href="/blog/when-should-you-run-a-marketing-audit">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.oranger}
                >
                  when you should consider it
                </a>
              </Link>
              ,{" "}
              <Link href="/blog/strong-digital-marketing-audit">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.oranger}
                >
                  {" "}
                  how important It is
                </a>
              </Link>
              ,{" "}
              <Link href="/blog/5-steps-for-a-successfu-digital-marketing-audit">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.oranger}
                >
                  {" "}
                  its steps
                </a>
              </Link>
              , and the{" "}
              <Link href="/blog/2022-digital-marketing-toolbox">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.oranger}
                >
                  {" "}
                  toolbox
                </a>
              </Link>{" "}
              needed to conduct it.
            </Text>
          </Grid>
        </Grid.Container>

        <br></br>
        <Text h2 className={styles.numlist}>
          2. Digitally enhanced service
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Nowadays, customers are more and more interested in experience over
          product. Therefore, businesses should focus on providing outcomes
          rather than just selling their product.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This digital solution consists of adding a digital component to the
          typical services to enhance their user’s experience. We are talking
          here about digital technologies such as AI (artificial intelligence),
          blockchain, VR/AR (virtual/augmented reality), and IOT (internet of
          things).
        </Text>
        <br></br>
        <Text span className={styles.bold}>
          If you are providing a service, we suggest questioning the possibility
          of adding the touch of technology to it
        </Text>
        <br></br>
        <br></br>

        <Grid.Container className={styles.content}>
          <Grid
            md={1}
            sm={12}
            xs={12}
            alignItems="flex-start"
            css={{ marginTop: "0.5em" }}
          >
            <Image
              height={20}
              width={50}
              src="/images/blog22.2.svg"
              alt=""
              objectFit="contain"
            />
          </Grid>
          <Grid md={10} sm={11} xs={10}>
            <Text span>
              We have already talked in depth about{" "}
              <Link href="/blog/AI-Solutions">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.oranger}
                >
                  AI solutions
                </a>
              </Link>
              , their impact on{" "}
              <Link href="/blog/digital-businesses">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.oranger}
                >
                  business
                </a>
              </Link>{" "}
              and{" "}
              <Link href="/blog/AI-Marketing">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.oranger}
                >
                  marketing
                </a>
              </Link>
              , their{" "}
              <Link href="/blog/Artificial-Intelligence-Trends">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={styles.oranger}
                >
                  trends
                </a>
              </Link>
              , and a set of other topics that will help you dive deep into the
              world of AI.
            </Text>
          </Grid>
        </Grid.Container>

        <br></br>
        <Text h2 className={styles.numlist}>
          3. Process optimization software
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This digital solution is a cutting-edge tool that enhances performance
          and maximizes the outturn, especially for industrial manufacturers. It
          is a type of software that not only improves but also maximizes
          business operations.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Process optimization software combines asset and project management
          tools with data analytics, to help large industrial enterprises to
          detect the day-to-day operations’ gaps and provide actionable insights
          to optimize the core process.
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          4. Software platform
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This digital solution is a platform; a framework of software and
          resources from the surrounding ecosystem. It intends to allow
          applications to work together without integrations or workarounds.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          It is a flexible approach to managing a growing business; it doesn’t
          limit you with a toolbox like does a product suite, but is designed in
          a way that you can easily plug in tools as needed.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Therefore, other than its features, the software platform enables
          businesses to connect processes, external tools, data, and teams.
        </Text>
        <br></br>
        <br></br>

        <Text h2 className={styles.numlist}>
          5. Digital system
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This digital solution communicates, processes, and stores information
          in digital form. We can here mention the computer as the most known
          typical example of a digital system. Also, mobile phones, radios,
          megaphones, and many more are considered digital systems.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          The specificity of this digital solution is that it deals with
          discrete signals. In other words, the outputs and inputs are binary
          values; either 1 or 0.
        </Text>
        <br></br>
        <br></br>
        <Text h2 className={styles.numlist}>
          6. Digital machine option
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This is the most sophisticated type of digital solution. Smart
          machines with digital options are devices embedded with computing
          technologies such as deep learning or machine learning, artificial
          intelligence (AI), etc.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          This digital solution is designed to reason, solve problems, make
          decisions, and even take action. This type includes self-driving cars,
          robots, and other computing systems that work autonomously without
          human intervention.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          To conclude, everyone’s mindset has shifted with the arrival of
          technological solutions. Nowadays, we are always looking for the most
          convenient solution to our needs. Therefore, if you are considering
          one or more of the digital solutions above you should think seriously
          about the sales approach that you will follow. We are not undervaluing
          the importance of the efficiency of your current digital solution, but
          it is crucial to show how it perfectly meets the needs of your
          customers. You need to highlight why your customers need to get your
          product.
        </Text>
        <br></br>
        <Text span className={styles.content}>
          Don’t hesitate to contact us, our experts can gladly help you figure
          this out.
        </Text>
      </Grid>
    </>
  );
};

export default Blog3Slider;

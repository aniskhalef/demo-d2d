import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";

import Image from "next/image";
const Blog12Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            Business Growth
          </Text>
          <br></br>
          <Grid
            container
            align="center"
            justifyContent="center"
            alignItems="center"
            md={4}
          >
            <Image
              src="/images/blog13.1.svg"
              alt="Business growth"
              width={350}
              height={250}
              objectFit="contain"
            />
          </Grid>
        </Grid>
        <br></br>
        <Text span className={styles.titleB}>
          What is Business Growth
        </Text>
        <br></br>
        <Grid md={11} sm={12} xs={12}>
          <Text span className={styles.content}>
            Business growth is the expansion of a business, done in one or
            multiple ways. From marketing strategies to business model updates,
            companies have virtually unlimited growth opportunities.
          </Text>
          <br></br>
          <Text span className={styles.content}>
            This evolvement can be critical. It needs expertise and knowledge to
            choose the right Business Growth capital to your personalized
            company.
          </Text>
          <br></br>
          <Text span className={styles.content}>
            That’s why it is primordial to choose correctly. If ever a company
            misses the opportunity and take the wrong decisions by choosing
            unwisely, That’s where the disaster shows up.
          </Text>

          <br></br>
          <Text span className={styles.content}>
            To take the right decisions, strategies, and road-map for your
            business, as:
            <br></br>
            <br></br>
            <ul className={styles.list}>
              <li>- The size of the capital.</li>
              <li>- The cost of the capital.</li>
              <li>- The flexibility of the capital</li>
              <li>- The term structure of capital.</li>
            </ul>
          </Text>
          <br></br>

          <Text span className={styles.content}>
            Considering these four variables to finally being optimized will
            conduct companies to the best business growth solution that is
            adaptative to their business.
          </Text>
          <br></br>
          <Text span className={styles.content}>
            When we talk about solutions it’s important to mention how
            <Link href="/blog/digital-problem">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.violet}
              >
                {" "}
                Digital Solutions{" "}
              </a>
            </Link>
            can work for every business, since the business world has gone
            Digital.
            <br></br>
            The benefits are numerous, including enhanced collaboration within
            teams and between businesses, improved consumer engagement, higher
            staff productivity and creativity, and superior insights from data.
            Is your company already on the path to digital transformation? If
            not, take a look at the importance to
            <Link href="/blog/digital-businesses">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.violet}
              >
                {" "}
                rethink businesses in a Digital way
              </a>
            </Link>
            .
          </Text>
          <br></br>
        </Grid>
        <br></br>

        <Text span className={styles.bold}>
          Top reasons why Digital Solutions can lead to Business Growth
        </Text>
        <br></br>
        <br></br>
        <Grid.Container>
          <Grid justifyContent="center" md={9} sm={12}>
            <Text span className={styles.content}>
              In this Digital era, companies found out that customers are now
              all digitalized. That’s why it is important to put the focus on
              Digital platforms and implement a strategy that goes with your
              target audience.
              <br></br>
              This will increase productivity, profits, and reduce costs. How?
              Companies that aren’t Digital or do not have a Digital strategy
              within their organism spend more time and money. That’s why with a
              proper Digital solution and with appropriate tools, Businesses can
              go a long way in reducing costs and increasing productivity. And
              that’s how you can take your business to the next level.
            </Text>
          </Grid>

          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={3}
            sm={12}
          >
            <Image
              src="/images/blog13.2.svg"
              alt="Digital solution"
              width={350}
              height={350}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>

        <br></br>
        <Text span className={styles.bold}>
          <Link href="/">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.blue}
            >
              Check out DueToData - Your Digital Solution Provider.
            </a>
          </Link>
        </Text>
      </Grid>
    </>
  );
};

export default Blog12Slider;

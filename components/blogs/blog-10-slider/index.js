import React from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import Link from "next/link";
import styles from "./styles.module.css";
import Image from "next/image";
const Blog10Slider = () => {
  return (
    <>
      <Grid md={11} sm={11} css={{ lineHeight: "2.2em" }}>
        <Grid>
          <Text h1 className={styles.title}>
            4 ways Artificial Intelligence is changing the world
          </Text>
          <br></br>
        </Grid>
        <Grid justifyContent="center" md={8} sm={12}>
          <Text span className={styles.content}>
            AI is a fast-paced technology that is controlling the globe with its
            various uses.
          </Text>
          <Text span className={styles.content}>
            It transformed the way we all do businesses and became as
            intelligent as human beings in several jobs and tasks. To learn more
            about the evolvement of artificial intelligence check out this
            article:
            <Link href="/blog/Artificial-Intelligence-Trends">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.green}
              >
                {" "}
                Artificial Intelligence trends
              </a>
            </Link>
            .
          </Text>

          <Text span className={styles.content}>
            Nevertheless, let's see what changes AI delivers in 2022:
          </Text>
        </Grid>
        <Grid
          justifyContent="center"
          align="center"
          alignItems="center"
          md={3}
          sm={12}
        >
          <Image
            src="/images/blog15.1.svg"
            alt="Artificial Intelligence"
            height={350}
            width={350}
            objectFit="contain"
          />
        </Grid>

        <Grid>
          <Text span className={styles.titleP}>
            1. Machine Learning
          </Text>
          <br></br>
          <Text span className={styles.content}>
            Machine Learning (ML) is an application of AI that help in analyzing
            automatically without the need of being programmed. It’s the idea of
            a computer that can learn new informations without human
            intevention. Want to know more about machine learning? check out
            this article:
            <Link href="/blog/machine-learning">
              <a
                target="_blank"
                rel="noopener noreferrer"
                className={styles.blue}
              >
                {" "}
                «Machine Learning»
              </a>
            </Link>
            .
          </Text>
          <br></br>
          <br></br>
          <Text span className={styles.titleP}>
            2. AI in education
          </Text>
          <br></br>
          <Text span className={styles.content}>
            There are several courses available on platforms that may be quite
            instructive and can be accessed from anywhere, at any time. AI has
            the potential to automate administrative chores for teachers and
            educational institutions. Educators devote a significant amount of
            effort to grading tests, assessing assignments, and imparting
            critical reactions to their learners. AI enables the automation of
            categories and the processing of paperwork. Schooling may be
            redefined from the comfort of one's own home, with each student's
            needs being met individually.
          </Text>
        </Grid>
        <br></br>

        <Grid.Container justifyContent="center">
          <Grid md={8} sm={12}>
            <Grid>
              <Text span className={styles.titleP}>
                3. Robots
              </Text>
              <br></br>
              <Text span className={styles.content}>
                The use of robots is becoming trendy these years and it is
                evolving within the years. But diving more in this trend, drones
                will be the new 2022 trend. They will be used for medical apps,
                transportation of samples and reagents from hospitals to
                laboratories will become easier and much quicker than
                automobiles could drive. Robots will be used frequently in all
                the terms.. Learn more in this article:
                <a
                  target="_blank"
                  href="/blog/Artificial-Intelligence-and-Robots"
                  rel="noopener noreferrer"
                  className={styles.green}
                >
                  {" "}
                  Artificial Intelligence and robots
                </a>
                .
              </Text>
            </Grid>
          </Grid>
          <Grid
            justifyContent="center"
            align="center"
            alignItems="center"
            md={4}
            sm={12}
          >
            <Image
              src="/images/blog15.2.svg"
              alt="Artificial intelligence in entreprise"
              height={200}
              width={300}
              objectFit="contain"
            />
          </Grid>
        </Grid.Container>
        <br></br>

        <Text span className={styles.titleP}>
          4. AI in Entreprise
        </Text>
        <br></br>
        <Text span className={styles.content}>
          The reason businesses use this technology in their strategies is
          because of the way it help employees become more productive than ever.
          The way it handles tasks that are repetitive is what makes it special.
          Employees will focus on the things that demand more creativeness, and
          complicated problems to solve. Want to learn more about businesses
          that are currently use AI? Check out this article for more
          information:
          <Link href="/blog/Artificial-Intelligence-Trends">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className={styles.green}
            >
              {" "}
              Artificial Intelligence trends
            </a>
          </Link>
          .
        </Text>
      </Grid>
    </>
  );
};

export default Blog10Slider;

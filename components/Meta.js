import Head from "next/head";

const Meta = ({ title, keywords, description, thumbnail }) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="keywords" content={keywords} />
      <meta name="author" content="DuetoData" />
      <meta
        name="description"
        content={description}
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      />
      <meta property="og:title" content={title} />
      <meta property="og:type" content="article" />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={thumbnail} />
    </Head>
  );
};

Meta.defaultProps = {
  title: "Your digital solution provider",
  keywords:
    "Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing",
  description:
    "Discover how we can help you unlock  your business or scale it through Digital solutions based on Artificial Intelligence specifically for you",
  thumbnail: "https://i.postimg.cc/L6SK9JJc/thumbnail-1.png",
};
export default Meta;

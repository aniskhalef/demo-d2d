
/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: 'https://duetodata.com',
    generateRobotsTxt: true,
}
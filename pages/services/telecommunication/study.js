import HeroGenericComponent from "../../../components/services/telecom/HeroGenericComp";
import StudySlider from "../../../components/services/telecom/study-slider";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Meta from "../../../components/Meta";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ServiceNavigation from "../../../components/services/telecom/Navigation";
const services = [
  {
    link: "/services/telecommunication/study",
  },
  {
    link: "/services/telecommunication/deployment",
  },
];
const Study = () => {
  const { t } = useTranslation("common");
  const router = useRouter();
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const currentPageIndex = services.findIndex(
      (service) => service.link === router.pathname
    );
    if (currentPageIndex !== -1) {
      setCurrentIndex(currentPageIndex);
    }
  }, [router.pathname]);
  return (
    <>
      <Meta
        title={t("titletagstudy")}
        description={t("desctagstudy")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <HeroGenericComponent
        title={t("study")}
        value1Title={t("service_tel_value_1")}
        value1Desc={t("service_tel_value_desc_1")}
        value2Title={t("service_tel_value_2")}
        value2Desc={t("service_tel_value_desc_2")}
        value3Title={t("service_tel_value_3")}
        value3Desc={t("service_tel_value_desc_3")}
        value4Title={t("service_tel_value_4")}
        value4Desc={t("service_tel_value_desc_4")}
        imageSrc="/assets/servicestel/study.svg"
        backgroundColor="#B4B5B5"
        slider={<StudySlider />}
      />
      <ServiceNavigation
        services={services}
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
      />
    </>
  );
};

export default Study;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

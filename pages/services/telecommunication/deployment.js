import HeroGenericComponent from "../../../components/services/telecom/HeroGenericComp";
import DeploySlider from "../../../components/services/telecom/deploy-slider";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../../../components/Meta";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ServiceNavigation from "../../../components/services/telecom/Navigation";
const services = [
  {
    link: "/services/telecommunication/deployment",
  },
  {
    link: "/services/telecommunication/study",
  },
];
const Deployment = () => {
  const { t } = useTranslation("common");
  const router = useRouter();
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const currentPageIndex = services.findIndex(
      (service) => service.link === router.pathname
    );
    if (currentPageIndex !== -1) {
      setCurrentIndex(currentPageIndex);
    }
  }, [router.pathname]);

  return (
    <>
      <Meta
        title={t("titletagwork")}
        description={t("desctagwork")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <HeroGenericComponent
        title={t("works")}
        value1Title={t("service_deployment_value_1")}
        value1Desc={t("service_deployment_value_desc_1")}
        value2Title={t("service_deployment_value_2")}
        value2Desc={t("service_deployment_value_desc_2")}
        value3Title={t("service_deployment_value_3")}
        value3Desc={t("service_deployment_value_desc_3")}
        value4Title={t("service_deployment_value_4")}
        value4Desc={t("service_deployment_value_desc_4")}
        imageSrc="/assets/servicestel/deployment.svg"
        backgroundColor="#E4B752"
        slider={<DeploySlider />}
      />
      <ServiceNavigation
        services={services}
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
      />
    </>
  );
};

export default Deployment;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

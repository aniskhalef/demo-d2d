import HeroGenericComponent from "../../../components/services/dev/HeroGenericComp";
import WebSlider from "../../../components/services/dev/web-slider";
import ServiceNavigation from "../../../components/services/dev/Navigation";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../../../components/Meta";
import { useTranslation } from "next-i18next";

const services = [
  {
    link: "/services/development/web-development",
  },
  {
    link: "/services/development/mobile-development",
  },
  {
    link: "/services/development/digital-marketing",
  },
  {
    link: "/services/development/artificial-intelligence",
  },
  {
    link: "/services/development/iot",
  },
  {
    link: "/services/development/blockchain",
  },
  {
    link: "/services/development/talents-on-demand",
  },
  {
    link: "/services/development/ux-ui-design",
  },
  {
    link: "/services/development/motion-design",
  },
];

const ServicePage = ({
  title,
  value1,
  value2,
  imageSrc,
  backgroundColor,
  slider,
}) => {
  const router = useRouter();
  const [currentIndex, setCurrentIndex] = useState(0);
  const { t } = useTranslation("common");

  useEffect(() => {
    const currentPageIndex = services.findIndex(
      (service) => service.link === router.pathname
    );
    if (currentPageIndex !== -1) {
      setCurrentIndex(currentPageIndex);
    }
  }, [router.pathname]);

  return (
    <>
      <Meta
        title={t("title_devweb")}
        description={t("desc_devweb")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <HeroGenericComponent
        title={title}
        value1={value1}
        value2={value2}
        imageSrc={imageSrc}
        backgroundColor={backgroundColor}
        slider={slider}
      />
      <ServiceNavigation
        services={services}
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
      />
    </>
  );
};
const WebDev = () => {
  const { t } = useTranslation("common");
  const data = {
    title: `${t("service_devweb")}`,
    value1: `${t("service_devweb_services_1")}`,
    value2: `${t("service_devweb_services_2")}`,

    imageSrc: "/assets/servicesdev/devweb.svg",
    backgroundColor: "#EDC468",
    slider: <WebSlider />,
  };

  return <ServicePage {...data} />;
};

export default WebDev;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

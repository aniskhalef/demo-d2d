import HeroGenericComponent from "../../../components/services/dev/HeroGenericComp";
import BlockchainSlider from "../../../components/services/dev/blockchain-slider";
import ServiceNavigation from "../../../components/services/dev/Navigation";
import { useEffect, useState } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Meta from "../../../components/Meta";
import { useRouter } from "next/router";
const services = [
  {
    link: "/services/development/web-development",
  },
  {
    link: "/services/development/mobile-development",
  },
  {
    link: "/services/development/digital-marketing",
  },
  {
    link: "/services/development/artificial-intelligence",
  },
  {
    link: "/services/development/iot",
  },
  {
    link: "/services/development/blockchain",
  },
  {
    link: "/services/development/talents-on-demand",
  },
  {
    link: "/services/development/ux-ui-design",
  },
  {
    link: "/services/development/motion-design",
  },
];

const ServicePage = ({
  title,
  value1,
  value2,
  imageSrc,
  backgroundColor,
  slider,
}) => {
  const router = useRouter();
  const [currentIndex, setCurrentIndex] = useState(0);
  const { t } = useTranslation("common");

  useEffect(() => {
    const currentPageIndex = services.findIndex(
      (service) => service.link === router.pathname
    );
    if (currentPageIndex !== -1) {
      setCurrentIndex(currentPageIndex);
    }
  }, [router.pathname]);

  return (
    <>
      <Meta
        title={t("title_blockchain")}
        description={t("desc_blockchain")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <HeroGenericComponent
        title={title}
        value1={value1}
        value2={value2}
        imageSrc={imageSrc}
        backgroundColor={backgroundColor}
        slider={slider}
      />
      <ServiceNavigation
        services={services}
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
      />
    </>
  );
};
const BlockchainPage = () => {
  const { t } = useTranslation("common");

  const data = {
    title: "BLOCKCHAIN",
    value1: `${t("service_blockchain_services_1")}`,
    value2: `${t("service_blockchain_services_2")}`,
    imageSrc: "/assets/servicesdev/blockchain.svg",
    backgroundColor: "#CAA9F3",
    slider: <BlockchainSlider />,
  };

  return <ServicePage {...data} />;
};

export default BlockchainPage;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

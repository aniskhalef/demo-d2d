import React, { useState, useEffect } from "react"; // Import useState hook
import { Grid, Text } from "@nextui-org/react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../../components/Meta";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import Layout from "../../layout";
import { useRouter } from "next/router";
const ServicesDev = () => {
  const router = useRouter();
  const { category: queryCategory } = router.query;
  const [category, setCategory] = useState(queryCategory);
  const [showFilters, setShowFilters] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  const checkMobileView = () => {
    setIsMobile(window.innerWidth <= 960);
  };

  useEffect(() => {
    checkMobileView();

    window.addEventListener("resize", checkMobileView);

    return () => window.removeEventListener("resize", checkMobileView);
  }, []);
  const { t } = useTranslation("common");

  const servicesDev = [
    {
      title: `${t("service_devweb")}`,
      description: `${t("service_devweb")}`,
      image: "/assets/servicesdev/devweb.svg",
      path: "/services/development/web-development",
      color: "#EDC468",
    },
    {
      title: `${t("service_devmobile")}`,
      description: `${t("service_devmobile")}`,
      image: "/assets/servicesdev/devmobile.svg",
      path: "/services/development/mobile-development",
      color: "#B8BCF5",
    },
    {
      title: `${t("service_marketing")}`,
      description: `${t("service_marketing")}`,
      image: "/assets/servicesdev/digitalmarketing.svg",
      path: "/services/development/digital-marketing",
      color: "#F9AAA4",
    },
    {
      title: `${t("service_ai")}`,
      description: `${t("service_ai")}`,
      image: "/assets/servicesdev/ai.svg",
      path: "/services/development/artificial-intelligence",
      color: "#E69AC2",
    },
    {
      title: `${t("service_iot")}`,
      description: `${t("service_iot")}`,
      image: "/assets/servicesdev/iot.svg",
      path: "/services/development/iot",
      color: "#F5B86C",
    },
    {
      title: `${t("service_blockchain")}`,
      description: `${t("service_blockchain")}`,
      image: "/assets/servicesdev/blockchain.svg",
      path: "/services/development/blockchain",
      color: "#CAA9F3",
    },
    {
      title: `${t("service_talents")}`,
      description: `${t("service_talents")}`,
      image: "/assets/servicesdev/talents.svg",
      path: "/services/development/talents-on-demand",
      color: "#DBB4DF",
    },
    {
      title: `${t("service_uxui")}`,
      description: `${t("service_uxui")}`,
      image: "/assets/servicesdev/uxui.svg",
      path: "/services/development/ux-ui-design",
      color: "#E9AD51",
    },
    {
      title: `${t("service_motion")}`,
      description: `${t("service_motion")}`,
      image: "/assets/servicesdev/motion.svg",
      path: "/services/development/motion-design",
      color: "#96CBF6",
    },
  ];

  const servicesTel = [
    {
      title: `${t("service_deployment_study")}`,
      description: `${t("service_deployment_study")}`,
      image: "/assets/servicestel/deployment.svg",
      path: "/services/telecommunication/deployment",
      color: "#E4B752",
    },
    {
      title: `${t("service_tel_study")}`,
      description: `${t("service_tel_study")}`,
      image: "/assets/servicestel/study.svg",
      path: "/services/telecommunication/study",
      color: "#B4B5B5",
    },
  ];
  return (
    <>
      <Meta
        title={t("title_services")}
        description={t("desc_services")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <Layout>
        <Grid
          css={{ height: "1Sem", marginTop: "3em" }}
          className="filter-grid"
          style={{
            display: showFilters ? "block" : "none",
            position: "fixed",
            top: 0,
            left: 0,
            height: "100vh",
            width: "100vw",
            zIndex: 9999,
            padding: "20px",
            backgroundColor: "#fff",
          }}
        >
          <button
            className="close-filters-button"
            onClick={() => setShowFilters(false)}
            style={{
              position: "absolute",
              top: "20px",
              right: "30px",
              zIndex: 99999,
            }}
          >
            <Image
              src="/assets/icons/close.svg"
              alt="duetodata close"
              width={20}
              height={50}
              objectFit="contain"
            />
          </button>
          <Grid>
            <Grid>
              {category === "development" ? (
                <>
                  <Grid xs={8} css={{ height: "10em" }}>
                    <Text b size={"$lg"}>
                      {t("title_services_dev_side")}
                    </Text>
                    <br></br>
                    <br></br>
                    <Text span size={"$sm"}>
                      {t("desc_services_dev_side")}
                    </Text>
                  </Grid>
                </>
              ) : (
                <>
                  <Grid xs={8} css={{ height: "10em" }}>
                    <Text b size={"$lg"}>
                      {t("title_services_tel_side")}
                    </Text>
                    <br></br>
                    <br></br>
                    <Text span size={"$sm"}>
                      {t("desc_services_tel_side")}
                    </Text>
                  </Grid>
                </>
              )}
            </Grid>
            <Grid css={{ marginTop: "4em" }}>
              <Text b>{t("filters")}</Text>
              <br></br>
              <Text span size={"$sm"}>
                {t("speciality")}
              </Text>
              <br></br>
              <br></br>
              <Grid>
                <input
                  type="radio"
                  name="category"
                  value="development"
                  checked={category === "development"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("development")}
                />
                <Text span size={"$sm"}>
                  {t("services_dev")}
                </Text>
                <br></br>
                <br></br>
                <input
                  type="radio"
                  name="category"
                  value="telecommunication"
                  checked={category === "telecommunication"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("telecommunication")}
                />
                <Text span size={"$sm"}>
                  {t("services_tel")}
                </Text>
              </Grid>
              <br></br>
            </Grid>
          </Grid>
        </Grid>
        <Grid.Container className="service-wrapper">
          {isMobile && (
            <button
              className="open-filters-button"
              onClick={() => setShowFilters(!showFilters)}
              style={{
                display: showFilters ? "none" : "block",
                position: "fixed",
                bottom: "20px",
                right: "20px",
                zIndex: 10,
              }}
            >
              <Image
                src={"/assets/icons/filter.svg"}
                height={50}
                width={50}
                objectFit="contain"
                alt="filter"
              />
            </button>
          )}
          <Grid md={3} xs={0}>
            <Grid css={{ marginRight: "2em" }}>
              {category === "development" ? (
                <>
                  <Grid css={{ height: "15em", marginTop: "8em" }}>
                    <Text b size={"$lg"}>
                      {t("title_services_dev_side")}
                    </Text>
                    <br></br>
                    <br></br>
                    <Text span size={"$sm"}>
                      {t("desc_services_dev_side")}
                    </Text>
                  </Grid>
                </>
              ) : (
                <>
                  <Grid css={{ height: "15em", marginTop: "8em" }}>
                    <Text b size={"$lg"}>
                      {t("title_services_tel_side")}
                    </Text>
                    <br></br>
                    <br></br>
                    <Text span size={"$sm"}>
                      {t("desc_services_tel_side")}
                    </Text>
                  </Grid>
                </>
              )}
              <Grid>
                <Text b>{t("filters")}</Text>
                <br></br>
                <Text span size={"$sm"}>
                  {t("speciality")}
                </Text>
              </Grid>
              <br></br>
              <Grid>
                <Grid>
                  <input
                    type="radio"
                    name="category"
                    value="development"
                    checked={category === "development"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("development")}
                  />
                  <Text span size={"$sm"}>
                    {t("services_dev")}
                  </Text>
                </Grid>
                <br></br>
                <Grid>
                  <input
                    type="radio"
                    name="category"
                    value="telecommunication"
                    checked={category === "telecommunication"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("telecommunication")}
                  />
                  <Text span size={"$sm"}>
                    {t("services_tel")}
                  </Text>
                </Grid>
              </Grid>
              <br></br>
            </Grid>
          </Grid>
          <Grid md={8} className="main_content">
            <Grid.Container>
              {category === "development"
                ? servicesDev.map((service) => (
                    <Grid key={service.name} md={4} className="service-dev">
                      <Grid>
                        <Grid
                          className="service-dev-image"
                          css={{
                            backgroundColor: `${service.color}`,
                          }}
                        >
                          <Link href={service.path}>
                            <Grid>
                              <Grid>
                                <Text
                                  b
                                  className="text-white"
                                  css={{
                                    position: "relative",
                                    zIndex: 1,
                                  }}
                                >
                                  {service.title}
                                </Text>
                              </Grid>

                              <Grid
                                css={{
                                  position: "absolute",
                                  top: "50%",
                                  left: "50%",
                                  transform: "translate(-50%, -50%)",
                                }}
                                className="dev-image"
                              >
                                <Image
                                  src={service.image}
                                  alt=""
                                  width={300}
                                  height={300}
                                  objectFit="contain"
                                />
                              </Grid>
                              <Grid md={12} className="service-dev-text">
                                <br></br>
                                <br></br>

                                <Text
                                  b
                                  size={"$4xl"}
                                  className="text-white link"
                                >
                                  {t("seemore")}
                                </Text>
                              </Grid>
                            </Grid>
                          </Link>
                        </Grid>
                      </Grid>
                    </Grid>
                  ))
                : servicesTel.map((service) => (
                    <Grid
                      key={service.name}
                      md={6}
                      sm={6}
                      className="service-tel"
                    >
                      <Grid>
                        <Grid
                          className="service-tel-image"
                          css={{
                            backgroundColor: `${service.color}`,
                          }}
                        >
                          <Link href={service.path}>
                            <Grid>
                              <Grid>
                                <Text b className="text-white">
                                  {service.title}
                                </Text>
                              </Grid>

                              <Grid
                                css={{
                                  position: "absolute",
                                  top: "50%",
                                  left: "50%",
                                  transform: "translate(-50%, -50%)",
                                }}
                                className="tel-image"
                              >
                                <Image
                                  src={service.image}
                                  alt=""
                                  width={800}
                                  height={800}
                                  objectFit="contain"
                                />
                              </Grid>
                              <Grid md={12} className="tel-text">
                                <Text b size={"$4xl"} className="text-white">
                                  {t("seemore")}
                                </Text>
                              </Grid>
                            </Grid>
                          </Link>
                        </Grid>
                      </Grid>
                    </Grid>
                  ))}
            </Grid.Container>
          </Grid>
        </Grid.Container>
      </Layout>
    </>
  );
};
export default ServicesDev;

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

import React from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Link from "next/link";
import { useState, useEffect } from "react";
import { useTranslation } from "next-i18next";
import Meta from "../components/Meta";
import { Grid, Text } from "@nextui-org/react";

export default function Error404() {
  const text = ["ERROR 404, PAGE NOT FOUND..."];
  const [output, setOutput] = useState("");
  const [lineIndex, setLineIndex] = useState(0);
  const [charIndex, setCharIndex] = useState(0);
  const { t } = useTranslation("common");

  useEffect(() => {
    const interval = setInterval(() => {
      if (
        text &&
        text[lineIndex] &&
        text[lineIndex][charIndex] &&
        lineIndex < text.length - 1
      ) {
        setOutput((output) => `${output}${text[lineIndex][charIndex]}`);
        setCharIndex((charIndex) => charIndex + 1);
      } else if (
        text &&
        text[lineIndex] &&
        text[lineIndex][charIndex] &&
        lineIndex === text.length - 1
      ) {
        setOutput((output) => `${output}${text[lineIndex][charIndex]}`);
        setCharIndex((charIndex) => charIndex + 1);
      } else {
        setTimeout(() => {
          setLineIndex((lineIndex) => lineIndex + 1);
          setCharIndex(0);
          setOutput((output) => `${output}\n`);
        }, 1000);
      }
    }, 50);

    return () => clearInterval(interval);
  }, [lineIndex, charIndex]);

  return (
    <>
      <Meta
        title={t("title404")}
        description={t("desc404")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <Grid className="px-6 py-6">
        <Text h3>{output}</Text>
        <br></br>
        <Link href="/">
          <Text b css={{ cursor: "pointer" }}>
            BACK TO HOME
          </Text>
        </Link>
      </Grid>
    </>
  );
}

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

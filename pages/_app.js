import { appWithTranslation } from "next-i18next";
import "../styles/globals.css";
import { GoogleAnalytics } from "nextjs-google-analytics";
import React from "react";
import { NextUIProvider } from "@nextui-org/react";
import { ThemeProvider as NextThemesProvider } from "next-themes";
import { SSRProvider } from "@react-aria/ssr";
import App from "next/app";
import "../styles/tailwind.css";

class MyApp extends App {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    let comment = document.createComment(`
`);
    document.insertBefore(comment, document.documentElement);
  }

  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    const Layout = Component.layout || (({ children }) => <>{children}</>);

    return (
      <NextThemesProvider defaultTheme="system" attribute="class">
        <NextUIProvider>
          <GoogleAnalytics />
          <SSRProvider>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </SSRProvider>
        </NextUIProvider>
      </NextThemesProvider>
    );
  }
}
export default appWithTranslation(MyApp);

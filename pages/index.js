import Landing from "../components/home/Landing";
import { useTranslation } from "next-i18next";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../components/Meta";
import Layout from "../layout";

const Home = () => {
  const { t } = useTranslation("common");
  return (
    <>
      <Meta
        title={t("title_home")}
        description={t("desc_home")}
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <Layout>
        <Landing />
      </Layout>
    </>
  );
};

export default Home;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

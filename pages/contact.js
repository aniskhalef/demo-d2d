import React, { Fragment, useState } from "react";
import { Grid, Text, Button } from "@nextui-org/react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../components/Meta";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Layout from "../layout";
import Link from "next/link";

const Contact = () => {
  const { t } = useTranslation("common");
  const [selectedCountry, setSelectedCountry] = useState("France");

  const handleCountryClick = (country) => {
    setSelectedCountry(country);
  };

  return (
    <>
      <Layout>
        <Meta
          title={t("titlecontact")}
          description={t("desccontact")}
          thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
          keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
        ></Meta>
        <Grid className="contact" align="center">
          <Grid.Container
            css={{
              alignItems: "center",
              margin: "auto",
            }}
            className="contact_wrapper"
          >
            <Grid md={2} sm={12} className="languages">
              <Grid>
                <Text
                  span
                  className={`contact-title ${
                    selectedCountry === "Tunisia" ? "underlined" : ""
                  }`}
                  onClick={() => handleCountryClick("Tunisia")}
                >
                  TUNISIE
                </Text>
                <br></br>
                <br></br>
                <br></br>
                <Text
                  span
                  className={`contact-title ${
                    selectedCountry === "France" ? "underlined" : ""
                  }`}
                  onClick={() => handleCountryClick("France")}
                >
                  FRANCE
                </Text>
              </Grid>
            </Grid>
            <Grid md={3} xs={12}>
              <Image
                src="/assets/contact/globe.svg"
                alt=""
                width={400}
                height={400}
              />
            </Grid>
            <Grid md={7} sm={12} className="details_contact">
              {selectedCountry === "France" ? (
                <Grid>
                  <Text span className="location">
                    FRANCE
                  </Text>
                  <br></br>
                  <br></br>
                  <Text span className="text-white">
                    49 Rue Voltaire Levallois Perrer, Paris •
                  </Text>
                  <br></br>
                  <Text span className="text-white">
                    9 Rue la dame jeanne 95800 Cergy •
                  </Text>
                  <br></br>

                  <Text span className="text-white">
                    207 Imp Joseph Tholon - 84810 Aubigan •
                  </Text>
                  <br></br>
                  <br></br>
                  <hr style={{ background: "white" }}></hr>
                  <br></br>

                  <Text b className="text-white">
                    {t("call")}
                  </Text>
                  <br></br>

                  <Text span className="text-white">
                    +33 6 03 94 99 01
                  </Text>
                  <br></br>
                  <br></br>

                  <hr style={{ background: "white" }}></hr>
                  <br></br>

                  <Text b className="text-white">
                    {t("email")}
                  </Text>
                  <br></br>

                  <Text span className="text-white">
                    sales@duetodata.com
                  </Text>

                  <Grid className="contact_btn">
                    <Button
                      css={{
                        backgroundColor: "#FFF",
                        borderRadius: "0px",
                        color: "#000",
                        fontWeight: "bold",
                      }}
                    >
                      <Link href="/contact-us">
                        <Text span css={{ textTransform: "uppercase" }}>
                          {" "}
                          {t("send")}
                        </Text>
                      </Link>
                    </Button>
                    <br></br>
                  </Grid>
                </Grid>
              ) : selectedCountry === "Tunisia" ? (
                <Grid>
                  <Text span className="location">
                    {t("tunisia")}
                  </Text>
                  <br></br>
                  <Text span className="text-white">
                    2 Rue Massicault, 4000 Sousse Tunisie
                  </Text>
                  <br></br>

                  <br></br>
                  <hr style={{ background: "white" }}></hr>
                  <br></br>

                  <Text b className="text-white">
                    {t("call")}
                  </Text>
                  <br></br>

                  <Text span className="text-white">
                    00216 54 790 199
                  </Text>
                  <br></br>
                  <br></br>

                  <hr style={{ background: "white" }}></hr>
                  <br></br>

                  <Text b className="text-white">
                    {t("email")}
                  </Text>
                  <br></br>

                  <Text span className="text-white">
                    sales@duetodata.com
                  </Text>
                  <Grid className="contact_btn">
                    <Button
                      css={{
                        backgroundColor: "#FFF",
                        borderRadius: "0px",
                        color: "#000",
                        fontWeight: "bold",
                      }}
                    >
                      <Link href="/contact-us">
                        <Text span css={{ textTransform: "uppercase" }}>
                          {t("send")}
                        </Text>
                      </Link>
                    </Button>
                  </Grid>
                  <br></br>
                </Grid>
              ) : null}
            </Grid>
          </Grid.Container>
        </Grid>
      </Layout>
    </>
  );
};

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

export default Contact;

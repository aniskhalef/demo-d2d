import React from "react";
import { Grid, Text } from "@nextui-org/react";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../components/Meta";
import { useTranslation } from "next-i18next";
import Layout from "../layout";
import Lottie from "react-lottie";
import back from "../public/assets/lotties/back_black.json";
import Link from "next/link";
const Privacy = () => {
  const { t } = useTranslation("common");
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: back,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <>
      <Meta
        title={t("titleprivacy")}
        description={t("descprivacy")}
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <Layout>
        <Link href="/">
          <Grid
            css={{ position: "fixed", top: 20, right: 140, zIndex: 99 }}
            className="back_arrow_terms"
          >
            <Lottie options={defaultOptions} width={50} />
          </Grid>
        </Link>
        <Grid
          css={{
            width: "80%",
            margin: "auto",
            padding: "4em",
            "@smMax": {
              padding: "1.5em",
              marginTop: "3em",
            },
          }}
        >
          <Text span>
            <h1>Privacy Policy for DuetoData</h1>

            <Text span>
              At www.duetodata.com, accessible from https://duetodata.com, one
              of our main priorities is the privacy of our visitors.
              <br></br>
              This Privacy Policy document contains types of information that is
              collected and recorded by www.duetodata.com and how we use it.
            </Text>

            <Text span>
              If you have additional questions or require more information about
              our Privacy Policy, do not hesitate to contact us.
            </Text>
            <br></br>
            <br></br>
            <h3>Consent</h3>

            <Text span>
              By using our website, you hereby consent to our Privacy Policy and
              agree to its terms.
            </Text>
            <br></br>
            <br></br>

            <h3>Information we collect</h3>

            <Text span>
              The personal information that you are asked to provide, and the
              reasons why you are asked to provide it, will be made clear to you
              at the point we ask you to provide your personal information.
            </Text>
            <Text span>
              If you contact us directly, we may receive additional information
              about you such as your name, email address, phone number, the
              contents of the message and/or attachments you may send us, and
              any other information you may choose to provide.
            </Text>
            <Text span>
              When you register for an Account, we may ask for your contact
              information, including items such as name, company name, address,
              email address, and telephone number.
            </Text>
            <br></br>
            <br></br>

            <h3>How we use your information</h3>

            <Text span>
              We use the information we collect in various ways, including to:
            </Text>

            <ul>
              <li>- Provide, operate, and maintain our website</li>
              <li>- Improve, personalize, and expand our website</li>
              <li>- Understand and analyze how you use our website</li>
              <li>
                - Develop new products, services, features, and functionality
              </li>
              <li>
                - Communicate with you, either directly or through one of our
                partners, including for customer service, to provide you with
                updates and other information relating to the website, and for
                marketing and promotional purposes
              </li>
              <li>- Send you emails</li>
              <li>- Find and prevent fraud</li>
            </ul>
            <br></br>

            <h3>Log Files</h3>

            <Text span>
              www.duetodata.com follows a standard procedure of using log files.
              These files log visitors when they visit websites. All hosting
              companies do this and a part of hosting services&apos; analytics.
              The information collected by log files include internet protocol
              (IP) addresses, browser type, Internet Service Provider (ISP),
              date and time stamp, referring/exit pages, and possibly the number
              of clicks. These are not linked to any information that is
              personally identifiable. The purpose of the information is for
              analyzing trends, administering the site, tracking users&apos;
              movement on the website, and gathering demographic information.
            </Text>
            <br></br>
            <br></br>

            <h3>Advertising Partners Privacy Policies</h3>

            <Text span>
              You may consult this list to find the Privacy Policy for each of
              the advertising partners of www.duetodata.com.
            </Text>

            <Text span>
              Third-party ad servers or ad networks uses technologies like
              cookies, JavaScript, or Web Beacons that are used in their
              respective advertisements and links that appear on
              www.duetodata.com, which are sent directly to users&apos; browser.
              They automatically receive your IP address when this occurs. These
              technologies are used to measure the effectiveness of their
              advertising campaigns and/or to personalize the advertising
              content that you see on websites that you visit.
            </Text>

            <Text span>
              Note that www.duetodata.com has no access to or control over these
              cookies that are used by third-party advertisers.
            </Text>
            <br></br>
            <br></br>

            <h3>Third Party Privacy Policies</h3>

            <Text span>
              www.duetodata.com&apos;s Privacy Policy does not apply to other
              advertisers or websites. Thus, we are advising you to consult the
              respective Privacy Policies of these third-party ad servers for
              more detailed information. It may include their practices and
              instructions about how to opt-out of certain options.{" "}
            </Text>

            <Text span>
              You can choose to disable cookies through your individual browser
              options. To know more detailed information about cookie management
              with specific web browsers, it can be found at the browsers&apos;
              respective websites.
            </Text>
            <br></br>
            <br></br>

            <h3>CCPA Privacy Rights (Do Not Sell My Personal Information)</h3>

            <Text span>
              Under the CCPA, among other rights, California consumers have the
              right to:
            </Text>
            <Text span>
              Request that a business that collects a consumer&apos;s personal
              data disclose the categories and specific pieces of personal data
              that a business has collected about consumers.
            </Text>
            <Text span>
              Request that a business delete any personal data about the
              consumer that a business has collected.
            </Text>
            <Text span>
              Request that a business that sells a consumer&apos;s personal
              data, not sell the consumer&apos;s personal data.
            </Text>
            <Text span>
              If you make a request, we have one month to respond to you. If you
              would like to exercise any of these rights, please contact us.
            </Text>
            <br></br>
            <br></br>

            <h3>GDPR Data Protection Rights</h3>

            <Text span>
              We would like to make sure you are fully aware of all of your data
              protection rights. Every user is entitled to the following:
            </Text>
            <Text span>
              The right to access – You have the right to request copies of your
              personal data. We may charge you a small fee for this service.
            </Text>
            <Text span>
              The right to rectification – You have the right to request that we
              correct any information you believe is inaccurate. You also have
              the right to request that we complete the information you believe
              is incomplete.
            </Text>
            <Text span>
              The right to erasure – You have the right to request that we erase
              your personal data, under certain conditions.
            </Text>
            <Text span>
              The right to restrict processing – You have the right to request
              that we restrict the processing of your personal data, under
              certain conditions.
            </Text>
            <Text span>
              The right to object to processing – You have the right to object
              to our processing of your personal data, under certain conditions.
            </Text>
            <Text span>
              The right to data portability – You have the right to request that
              we transfer the data that we have collected to another
              organization, or directly to you, under certain conditions.
            </Text>
            <Text span>
              If you make a request, we have one month to respond to you. If you
              would like to exercise any of these rights, please contact us.
            </Text>
            <br></br>
            <br></br>

            <h3>Children&apos;s Information</h3>

            <Text span>
              Another part of our priority is adding protection for children
              while using the internet. We encourage parents and guardians to
              observe, participate in, and/or monitor and guide their online
              activity.
            </Text>
            <Text span>
              www.duetodata.com does not knowingly collect any Personal
              Identifiable Information from children under the age of 13. If you
              think that your child provided this kind of information on our
              website, we strongly encourage you to contact us immediately and
              we will do our best efforts to promptly remove such information
              from our records.
            </Text>
          </Text>
        </Grid>
      </Layout>
    </>
  );
};

export default Privacy;

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

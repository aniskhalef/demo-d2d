import GenericComponent from "../../components/blogs/HeroGenericComp";
import Blog16Slider from "../../components/blogs/blog-16-slider";
import Meta from "../../components/Meta";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import React, { useState, useEffect } from "react";
import BlogNavigation from "../../components/blogs/Navigation";

const Blog16 = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const router = useRouter();

  useEffect(() => {
    const currentPageIndex = blogs.findIndex(
      (blog) => blog.link === router.pathname
    );
    if (currentPageIndex !== -1) {
      setCurrentIndex(currentPageIndex);
    }
  }, [router.pathname]);
  const blogs = [
    {
      link: "/blog/5-steps-for-a-successfu-digital-marketing-audit",
    },
    {
      link: "/blog/2022-digital-marketing-toolbox",
    },
    {
      link: "/blog/AI-Ethics",
    },
    {
      link: "/blog/AI-helps-in-company-growth",
    },
    {
      link: "/blog/AI-Marketing",
    },
    {
      link: "/blog/AI-Solutions",
    },
    {
      link: "/blog/AI-vs-human",
    },
    {
      link: "/blog/artificial-emotional-intelligence",
    },
    {
      link: "/blog/Artificial-Intelligence-and-Robots",
    },
    {
      link: "/blog/Artificial-Intelligence-is-changing-the-world",
    },
    {
      link: "/blog/Artificial-Intelligence-Trends",
    },
    {
      link: "/blog/Artificial-Intelligence's-influence-on-customer-experience",
    },
    {
      link: "/blog/blockchain-importance",
    },
    {
      link: "/blog/Business-Growth",
    },
    {
      link: "/blog/combinig-AI-with-marketing-automation",
    },
    {
      link: "/blog/digital-businesses",
    },
    {
      link: "/blog/digital-problem",
    },
    {
      link: "/blog/digitalisation-of-customer-experience",
    },
    {
      link: "/blog/machine-learning",
    },
    {
      link: "/blog/melding-AI-and-data",
    },
    {
      link: "/blog/strong-digital-marketing-audit",
    },
    {
      link: "/blog/types-of-digital-solutions",
    },
    {
      link: "/blog/when-should-you-run-a-marketing-audit",
    },
  ];
  const { t } = useTranslation("common");
  return (
    <>
      <Meta
        title={t("titleaicombination")}
        description={t("descaicombination")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <GenericComponent
        title="COMBINING AI WITH MARKETING AUTOMATION"
        description=" In this article we will discuss; What is Marketing Automation (MA),
          The combination of AI and MA and how can this merge be beneficial for
          Businesses ."
        subtitle=" "
        imageSrc="/assets/blogs/combineai.svg"
        backgroundColor="#BCA0E7"
        slider={<Blog16Slider />}
      />
      <BlogNavigation
        blogs={blogs}
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
      />{" "}
    </>
  );
};

export default Blog16;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

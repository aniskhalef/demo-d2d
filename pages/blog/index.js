import React, { useState, useEffect } from "react"; // Import useState hook
import { Grid, Text } from "@nextui-org/react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../../components/Meta";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Layout from "../../layout";
import Link from "next/link";

const Blog = () => {
  const { t } = useTranslation("common");
  const [category, setCategory] = useState("dev");
  const [showFilters, setShowFilters] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  const checkMobileView = () => {
    setIsMobile(window.innerWidth <= 1380);
  };

  useEffect(() => {
    checkMobileView();

    window.addEventListener("resize", checkMobileView);

    return () => window.removeEventListener("resize", checkMobileView);
  }, []);
  const blogs = [
    {
      title: "BLOCKCHAIN IMPORTANCE",
      image: "/assets/blogs/blockchainimportance.svg",
      desc: "Lorem ipsum",
      path: "/blog/blockchain-importance",
      color: "#5B5D60",
    },
    {
      title: "DATA SCIENTIST OBJECTIVES",
      image: "/assets/blogs/datascobjectives.svg",
      path: "/blog/data-scientist-objectives",
      color: "#58CBE2",
    },
    {
      title: "6 TYPES OF DIGITAL SOLUTIONS",
      image: "/assets/blogs/6types.svg",
      path: "/blog/types-of-digital-solutions",
      color: "#9290FF",
    },
    {
      title: "YOUR DIGITAL MARKETING AUDIT TOOLBOX",
      image: "/assets/blogs/toolbox.svg",
      path: "/blog/2022-digital-marketing-toolbox",
      color: "#D5AAEB",
    },
    {
      title: "5 STEPS FOR A SUCCESSFUL DIGITAL MARKETING AUDIT",
      image: "/assets/blogs/5steps.svg",
      path: "/blog/5-steps-for-a-successfu-digital-marketing-audit",
      color: "#E1A494",
    },
    {
      title: "THE WHY AND HOW OF A STRONG DIGITAL MARKETING AUDIT",
      image: "/assets/blogs/whyandhow.svg",
      path: "/blog/strong-digital-marketing-audit",
      color: "#59BE5CCE",
    },
    {
      title: "WHEN SHOULD YOU RUN A MARKETING AUDIT?",
      image: "/assets/blogs/whenmarketing.svg",
      path: "/blog/when-should-you-run-a-marketing-audit",
      color: "#E397A8",
    },
    {
      title: "AI ETHICS",
      image: "/assets/blogs/aiethics.svg",
      path: "/blog/AI-Ethics",
      color: "#625B88",
    },
    {
      title: "CAN ARTIFICIAL INTELLIGENCE REPLACE HUMAN INTELLIGENCE ?",
      image: "/assets/blogs/replacehuman.svg",
      path: "/blog/AI-vs-human",
      color: "#8BD8F3",
    },
    {
      title: "4 WAYS ARTIFICIAL INTELLIGENCE IS CHANGING THE WORLD",
      image: "/assets/blogs/4ways.svg",
      path: "/blog/Artificial-Intelligence-is-changing-the-world",
      color: "#EEC38F",
    },
    {
      title: "ARTIFICIAL INTELLIGENCE’S INFLUENCE ON CUSTOMER EXPERIENCE",
      image: "/assets/blogs/aiinfluence.svg",
      path: "/blog/Artificial-Intelligence's-influence-on-customer-experience",
      color: "#B1D5ED",
    },
    {
      title: "BUSINESS GROWTH",
      image: "/assets/blogs/businessgrowth.svg",
      path: "/blog/Business-Growth",
      color: "#F3BF6D",
    },
    {
      title: "ARTIFICIAL INTELLIGENCE AND ROBOTS",
      image: "/assets/blogs/airobots.svg",
      path: "/blog/Artificial-Intelligence-and-Robots",
      color: "#4C5094",
    },
    {
      title: "MACHINE LEARNING",
      image: "/assets/blogs/ml.svg",
      path: "/blog/machine-learning",
      color: "#FFC47B",
    },
    {
      title: "ARTIFICIAL EMOTIONAL INTELLIGENCE",
      image: "/assets/blogs/aiemotion.svg",
      path: "/blog/artificial-emotional-intelligence",
      color: "#F9D975",
    },
    {
      title: "COMBINING AI WITH MARKETING AUTOMATION",
      image: "/assets/blogs/combineai.svg",
      path: "/blog/combinig-AI-with-marketing-automation",
      color: "#BCA0E7",
    },
    {
      title: "MELDING AI AND DATA",
      image: "/assets/blogs/aianddata.svg",
      path: "/blog/melding-AI-and-data",
      color: "#ACABAB",
    },
    {
      title: "ARTIFICIAL INTELLIGENCE TRENDS",
      image: "/assets/blogs/aitrends.svg",
      path: "/blog/Artificial-Intelligence-Trends",
      color: "#F3D5ED",
    },
    {
      title: "HOW DOES ARTIFICIAL INTELLIGENCE HELPS IN COMPANY GROWTH",
      image: "/assets/blogs/aigrowth.svg",
      path: "/blog/AI-helps-in-company-growth",
      color: "#97E2D9",
    },
    {
      title: "DIGITALISATION OF CUSTOMER EXPERIENCE",
      image: "/assets/blogs/digitalisation.svg",
      path: "/blog/digitalisation-of-customer-experience",
      color: "#7A7A9D",
    },
    {
      title: "HOW AI IS RESHAPING MARKETING",
      image: "/assets/blogs/aireshape.svg",
      path: "/blog/AI-Marketing",
      color: "#44516B",
    },
    {
      title: "RETHINK BUSINESSES IN A DIGITAL WAY",
      image: "/assets/blogs/rethink.svg",
      path: "/blog/digital-businesses",
      color: "#6299E0",
    },
    {
      title: "AI SOLUTIONS, LET’S TALK ABOUT IT.",
      image: "/assets/blogs/aisolutions.svg",
      path: "/blog/AI-Solutions",
      color: "#F5BDBD",
    },
    {
      title: "THE “DIGITAL” PROBLEM HAS A VERY DIGITAL SOLUTION.",
      image: "/assets/blogs/digitalsolution.svg",
      path: "/blog/digital-problem",
      color: "#E4D054",
    },
  ];

  return (
    <>
      <Meta
        title={t("title_blog")}
        description={t("desc_blog")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <Layout>
        <Grid
          css={{ height: "1Sem", marginTop: "3em" }}
          className="filter-grid"
          style={{
            display: showFilters ? "block" : "none",
            position: "fixed",
            top: 0,
            left: 0,
            height: "100vh",
            width: "100vw",
            zIndex: 9999,
            backgroundColor: "#fff",
            padding: "20px",
            marginTop: "3em",
          }}
        >
          <button
            className="close-filters-button"
            onClick={() => setShowFilters(false)}
            style={{
              position: "absolute",
              top: "20px",
              right: "20px",
              zIndex: 99999,
            }}
          >
            <Image
              src="/assets/icons/close.svg"
              alt="duetodata close"
              width={20}
              height={50}
              objectFit="contain"
            />
          </button>
          <Grid>
            <Grid css={{ lineHeight: "1.7em" }}>
              <Grid xs={10}>
                <Text b size={"$lg"}>
                  {t("title_blog_side")}
                </Text>
                <br></br>
                <br></br>

                <Text span size={"$sm"}>
                  {t("desc_blog_side")}
                </Text>
                <Grid css={{ marginTop: "2em" }}>
                  <Text b>{t("filters")}</Text>
                  <br></br>
                  <Text span>{t("speciality")}</Text>
                </Grid>
              </Grid>

              <br></br>
              <label>
                <input
                  type="radio"
                  name="category"
                  value="dev"
                  checked={category === "dev"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("dev")}
                />
                <Text span size={"$sm"}>
                  {t("all")}
                </Text>
              </label>
              <br></br>

              <input
                type="radio"
                name="category"
                value="tel"
                checked={category === "tel"}
                style={{
                  border: "1px solid #000",
                  marginRight: "5px",
                  color: "#000",
                }}
                onChange={() => setCategory("tel")}
              />
              <Text span size={"$sm"}>
                Artificial intelligence
              </Text>
              <br></br>

              <label>
                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Blockchain
                </Text>
              </label>
              <br></br>

              <label>
                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Web development
                </Text>
              </label>
              <br></br>

              <label>
                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Internet of things (IOT)
                </Text>
              </label>
              <br></br>

              <label>
                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Mobile development
                </Text>
              </label>
              <br></br>

              <label>
                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Web development
                </Text>
              </label>
              <br></br>

              <label>
                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Talents on-demand
                </Text>
              </label>
              <br></br>

              <input
                type="radio"
                name="category"
                value="tel"
                checked={category === "tel"}
                style={{
                  border: "1px solid #000",
                  marginRight: "5px",
                  color: "#000",
                }}
                onChange={() => setCategory("tel")}
              />
              <Text span size={"$sm"}>
                Digital marketing
              </Text>
              <br></br>

              <input
                type="radio"
                name="category"
                value="tel"
                checked={category === "tel"}
                style={{
                  border: "1px solid #000",
                  marginRight: "5px",
                  color: "#000",
                }}
                onChange={() => setCategory("tel")}
              />
              <Text span size={"$sm"}>
                Design UX/UI
              </Text>
              <br></br>

              <input
                type="radio"
                name="category"
                value="tel"
                checked={category === "tel"}
                style={{
                  border: "1px solid #000",
                  marginRight: "5px",
                  color: "#000",
                }}
                onChange={() => setCategory("tel")}
              />
              <Text span size={"$sm"}>
                Design motion
              </Text>
            </Grid>
          </Grid>
        </Grid>
        <Grid.Container className="service-wrapper">
          {isMobile && (
            <button
              className="open-filters-button"
              onClick={() => setShowFilters(!showFilters)}
              style={{
                display: showFilters ? "none" : "block",
                position: "fixed",
                bottom: "20px",
                right: "20px",
                zIndex: 10,
              }}
            >
              <Image
                src={"/assets/icons/filter.svg"}
                height={50}
                width={50}
                objectFit="contain"
                alt="filter"
              />
            </button>
          )}
          <Grid md={3} xs={0}>
            <Grid>
              <Grid css={{ lineHeight: "1.7em", marginTop: "4em" }}>
                <Grid xs={10}>
                  <Text b size={"$lg"}>
                    {t("title_blog_side")}
                  </Text>
                  <br></br>
                  <br></br>

                  <Text span size={"$sm"}>
                    {t("desc_blog_side")}
                  </Text>
                  <Grid css={{ marginTop: "2em" }}>
                    <Text b>{t("filters")}</Text>
                    <br></br>
                    <Text span>{t("speciality")}</Text>
                  </Grid>
                </Grid>

                <br></br>
                <label>
                  <input
                    type="radio"
                    name="category"
                    value="dev"
                    checked={category === "dev"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("dev")}
                  />
                  <Text span size={"$sm"}>
                    {t("all")}
                  </Text>
                </label>
                <br></br>

                <label>
                  <input
                    type="radio"
                    name="category"
                    value="tel"
                    checked={category === "tel"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("tel")}
                  />
                  <Text span size={"$sm"}>
                    Artificial intelligence
                  </Text>
                </label>
                <br></br>

                <label>
                  <input
                    type="radio"
                    name="category"
                    value="tel"
                    checked={category === "tel"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("tel")}
                  />
                  <Text span size={"$sm"}>
                    Blockchain
                  </Text>
                </label>
                <br></br>

                <label>
                  <input
                    type="radio"
                    name="category"
                    value="tel"
                    checked={category === "tel"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("tel")}
                  />
                  <Text span size={"$sm"}>
                    Web development
                  </Text>
                </label>
                <br></br>

                <label>
                  <input
                    type="radio"
                    name="category"
                    value="tel"
                    checked={category === "tel"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("tel")}
                  />
                  <Text span size={"$sm"}>
                    Internet of things (IOT)
                  </Text>
                </label>
                <br></br>

                <label>
                  <input
                    type="radio"
                    name="category"
                    value="tel"
                    checked={category === "tel"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("tel")}
                  />
                  <Text span size={"$sm"}>
                    Mobile development
                  </Text>
                </label>
                <br></br>

                <label>
                  <input
                    type="radio"
                    name="category"
                    value="tel"
                    checked={category === "tel"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("tel")}
                  />
                  <Text span size={"$sm"}>
                    Web development
                  </Text>
                </label>
                <br></br>

                <label>
                  <input
                    type="radio"
                    name="category"
                    value="tel"
                    checked={category === "tel"}
                    style={{
                      border: "1px solid #000",
                      marginRight: "5px",
                      color: "#000",
                    }}
                    onChange={() => setCategory("tel")}
                  />
                  <Text span size={"$sm"}>
                    Talents on-demand
                  </Text>
                </label>
                <br></br>

                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Digital marketing
                </Text>
                <br></br>

                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Design UX/UI
                </Text>
                <br></br>

                <input
                  type="radio"
                  name="category"
                  value="tel"
                  checked={category === "tel"}
                  style={{
                    border: "1px solid #000",
                    marginRight: "5px",
                    color: "#000",
                  }}
                  onChange={() => setCategory("tel")}
                />
                <Text span size={"$sm"}>
                  Design motion
                </Text>
              </Grid>
            </Grid>
          </Grid>
          <Grid md={8} className="main_content">
            <Grid.Container>
              {blogs.map((service) => (
                <Grid
                  md={4}
                  sm={12}
                  xs={12}
                  key={service.name}
                  className="service-dev"
                >
                  <Grid>
                    <Grid
                      className="service-dev-image"
                      css={{
                        backgroundColor: `${service.color}`,
                      }}
                    >
                      <Text
                        b
                        className="text-white"
                        css={{ position: "relative", zIndex: 10 }}
                      >
                        {" "}
                        {service.title}
                      </Text>
                      <Grid
                        css={{
                          position: "absolute",
                          top: "50%",
                          left: "50%",
                          transform: "translate(-50%, -50%)",
                        }}
                        className="dev-image"
                      >
                        <Image
                          src={service.image}
                          alt=""
                          width={280}
                          height={270}
                          objectFit="contain"
                        />
                      </Grid>
                      <Grid className="text">
                        <br></br>
                        <br></br>

                        <Link href={service.path}>
                          <Text b size={"$4xl"} className="text-white link">
                            {t("seemore")}
                          </Text>
                        </Link>
                        <br></br>
                        <Text span size={"$xs"}>
                          {service.description}
                        </Text>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid.Container>
          </Grid>
        </Grid.Container>
      </Layout>
    </>
  );
};

export default Blog;

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

import GenericComponent from "../../components/products/HeroGenericComp";
import IrukaiSlider from "../../components/products/sliders/irukai";
import ProductNavigation from "../../components/products/Navigation";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import Meta from "../../components/Meta";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

const products = [
  {
    link: "/products/audio2text",
  },
  {
    link: "/products/aweni",
  },
  {
    link: "/products/biocheckai",
  },
  {
    link: "/products/duetolearn",
  },
  {
    link: "/products/ellves",
  },
  {
    link: "/products/irukai",
  },
  {
    link: "/products/kitchensavvy",
  },
  {
    link: "/products/nftgalahub",
  },
  {
    link: "/products/wpclicktopay",
  },
];
const ProductPage = ({
  challenge1,
  challenge2,
  value1,
  imageSrc,
  backgroundColor,
  slider,
}) => {
  const router = useRouter();

  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const currentPageIndex = products.findIndex(
      (product) => product.link === router.pathname
    );
    if (currentPageIndex !== -1) {
      setCurrentIndex(currentPageIndex);
    }
  }, [router.pathname]);
  const { t } = useTranslation("common");

  return (
    <>
      <Meta
        title={t("title_irukai")}
        description={t("desc_irukai")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>{" "}
      <GenericComponent
        challenge1={challenge1}
        challenge2={challenge2}
        value1={value1}
        imageSrc={imageSrc}
        backgroundColor={backgroundColor}
        slider={slider}
      />
      <ProductNavigation
        products={products}
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
      />
    </>
  );
};

const IrukaiI = () => {
  const { t } = useTranslation("common");
  const data = {
    challenge1: `${t("irukai_challenges_1")}`,
    challenge2: `${t("irukai_challenges_2")}`,
    value1: `${t("irukai_value")}`,
    imageSrc: "/assets/products/irukai.svg",
    backgroundColor: "#E5F4F1",
    slider: <IrukaiSlider />,
  };

  return <ProductPage {...data} />;
};

export default IrukaiI;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

import GenericComponent from "../../components/products/HeroGenericComp";
import ObserveMasterSlider from "../../components/products/sliders/observemaster";

import ProductNavigation from "../../components/products/Navigation";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../../components/Meta";
import { useTranslation } from "next-i18next";
const products = [
  {
    link: "/products/audio2text",
  },
  {
    link: "/products/aweni",
  },
  {
    link: "/products/biocheckai",
  },
  {
    link: "/products/duetolearn",
  },
  {
    link: "/products/ellves",
  },
  {
    link: "/products/irukai",
  },
  {
    link: "/products/kitchensavvy",
  },
  {
    link: "/products/nftgalahub",
  },
  {
    link: "/products/wpclicktopay",
  },
];
const ProductPage = ({
  challenge1,
  value1,
  value2,
  imageSrc,
  backgroundColor,
  slider,
}) => {
  const router = useRouter();
  const [currentIndex, setCurrentIndex] = useState(0);
  const { t } = useTranslation("common");

  useEffect(() => {
    const currentPageIndex = products.findIndex(
      (product) => product.link === router.pathname
    );
    if (currentPageIndex !== -1) {
      setCurrentIndex(currentPageIndex);
    }
  }, [router.pathname]);

  return (
    <>
      <Meta
        title={t("title_observer")}
        description={t("desc_observer")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>{" "}
      <GenericComponent
        challenge1={challenge1}
        value1={value1}
        value2={value2}
        imageSrc={imageSrc}
        backgroundColor={backgroundColor}
        slider={slider}
      />
      <ProductNavigation
        products={products}
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
      />
    </>
  );
};
const ObserveMaster = () => {
  const { t } = useTranslation("common");
  const data = {
    challenge1: `${t("observermaster_challenges")}`,
    value1: `${t("observermaster_value1")}`,
    value2: `${t("observermaster_value2")}`,
    imageSrc: "/assets/products/observe.svg",
    backgroundColor: "#5F69E241",
    slider: <ObserveMasterSlider />,
  };

  return <ProductPage {...data} />;
};

export default ObserveMaster;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

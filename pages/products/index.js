import { Grid, Text } from "@nextui-org/react";
import Meta from "../../components/Meta";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import Link from "next/link";
import Layout from "../../layout";
const Products = () => {
  const { t } = useTranslation("common");

  const products = [
    {
      description: `${t("irukai_solution")}`,
      image: "/assets/products/irukai.svg",
      path: "/products/irukai",
      color: "#E5F4F1",
    },
    {
      description: `${t("ellves_solution")}`,
      image: "/assets/products/ellves.svg",
      path: "/products/ellves",
      color: "#DFD0E3",
    },
    {
      description: `${t("aweni_solution")}`,
      image: "/assets/products/aaweni.svg",
      path: "/products/aweni",
      color: "#E0EFF1",
    },
    {
      description: `${t("kitchensavvy_solution")}`,
      image: "/assets/products/kitchensavvy.svg",
      path: "/products/kitchensavvy",
      color: "#E5F1DE",
    },
    {
      description: `${t("nftgalahub_solution")}`,
      image: "/assets/products/nft.svg",
      path: "/products/nftgalahub",
      color: "#FFC8646F",
    },
    {
      description: `${t("biocheckai_solution")}`,
      image: "/assets/products/biocheckai.svg",
      path: "/products/biocheckai",
      color: "#10ADD41D",
    },
    {
      description: `${t("observermaster_solution")}`,
      image: "/assets/products/observe.svg",
      path: "/products/observemaster",
      color: "#5F69E241",
    },

    {
      description: `${t("duetolearn_solution")}`,
      image: "/assets/products/duetolearn.svg",
      path: "/products/duetolearn",
      color: "#E5E9F7",
    },

    {
      description: `${t("click2pay_solution")}`,
      image: "/assets/products/wpclick.svg",
      path: "/products/wpclicktopay",
      color: "#D0E3D4",
    },
    {
      description: `${t("audio2txt_solution")}`,
      image: "/assets/products/audiototext.svg",
      path: "/products/audio2text",
      color: "#FCEBE3",
    },
  ];

  return (
    <>
      <Layout>
        <Meta
          title={t("title_products")}
          description={t("desc_products")}
          thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
          keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
        ></Meta>{" "}
        <Grid.Container className="products-wrapper">
          {products.map((product) => (
            <Grid key={product.name} md={3} sm={12} xs={12} className="product">
              <Grid>
                <Grid
                  className="product-image"
                  css={{
                    backgroundColor: `${product.color}`,
                  }}
                >
                  <Grid
                    css={{
                      position: "absolute",
                      top: "50%",
                      left: "50%",
                      transform: "translate(-50%, -50%)",
                    }}
                    className="image"
                  >
                    <Image
                      src={product.image}
                      alt=""
                      width={100}
                      height={150}
                      objectFit="contain"
                    />
                  </Grid>
                  <Link href={product.path}>
                    <Grid md={8} xs={8} className="text">
                      <Text span>{product.title}</Text>
                      <br></br>
                      <br></br>
                      <Grid className="product_description">
                        <Text span size={"$xs"}>
                          {product.description}
                        </Text>
                      </Grid>

                      <br></br>

                      <Text b size={"$3xl"} className="link">
                        {t("seemore")}
                      </Text>
                    </Grid>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          ))}
        </Grid.Container>
      </Layout>
    </>
  );
};

export default Products;

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

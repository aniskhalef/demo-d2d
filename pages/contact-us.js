import React, { useState } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../components/Meta";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { Grid, Text, Input, Textarea, Button } from "@nextui-org/react";

import { CONSTANTS } from "../constants/index";
import axios from "axios";
import Layout from "../layout";

import toast, { Toaster } from "react-hot-toast";

const ContactUs = () => {
  const { t } = useTranslation("common");
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [tel, setTel] = useState("");
  const [message, setMessage] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleLastNameChange = (event) => {
    setLastName(event.target.value);
  };
  const handleTelChange = (event) => {
    setTel(event.target.value);
  };

  const handleMessageChange = (event) => {
    setMessage(event.target.value);
  };
  const handleSubmit = (e) => {
    const url = `${CONSTANTS.API_URL_PROD}/user/contact`;

    axios
      .post(url, { email, name, lastName, tel, message })
      .then((response) => {
        console.log(response);
        toast.success("Demande de rendez-vous envoyé !");
        setEmail("");
        setName("");
        setLastName("");
        setTel("");
        setMessage("");
      })
      .catch((error) => {
        console.log(error);
        toast.error(error.response.data.message);
      });
  };
  return (
    <>
      <Meta
        title={t("titlecontact")}
        description={t("desccontact")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <Layout>
        <Grid className="contact_form">
          <Toaster
            position="top-center"
            toastOptions={{
              duration: 5000,
            }}
          />

          <Grid.Container align="center" className="wrapper">
            <Grid md={4} xs={0}>
              <Image
                src="/assets/contact/contact.svg"
                alt="contact duetodata"
                width={400}
                height={400}
              />
            </Grid>

            <Grid align="left" md={6} xs={12}>
              <Grid className="career-container">
                <Grid>
                  <Text
                    span
                    css={{ fontWeight: 600, fontSize: "20px", color: "white" }}
                  >
                    {t("contact")}
                  </Text>
                </Grid>
                <br></br>
                <Grid>
                  <label htmlFor="name">{t("name")}</label>
                  <br></br>
                  <Input
                    placeholder=""
                    value={name}
                    onChange={handleNameChange}
                    animated={false}
                    fullWidth
                    type="text"
                  />
                </Grid>
                <br></br>
                <Grid>
                  <label htmlFor="lastname">{t("lastname")}</label>
                  <br></br>
                  <Input
                    placeholder=""
                    value={lastName}
                    onChange={handleLastNameChange}
                    animated={false}
                    fullWidth
                    type="text"
                  />
                </Grid>
                <br></br>
                <Grid>
                  <label htmlFor="number">{t("number")}</label>
                  <br></br>
                  <Input
                    css={{
                      background: "transparent",
                    }}
                    fullWidth
                    type="text"
                    animated={false}
                    value={tel}
                    onChange={handleTelChange}
                  />
                </Grid>

                <br></br>
                <Grid>
                  <label htmlFor="email">Email</label>
                  <br></br>
                  <Input
                    type="email"
                    fullWidth
                    animated={false}
                    value={email}
                    onChange={handleEmailChange}
                  />
                </Grid>
                <br></br>

                <Grid>
                  <label htmlFor="message">Message</label>
                  <br></br>
                  <Textarea
                    fullWidth
                    animated={false}
                    value={message}
                    onChange={handleMessageChange}
                    minRows={3}
                  />
                </Grid>
                <br></br>
                <Grid align="left">
                  <Button
                    css={{
                      backgroundColor: "#fff",
                      border: "none",
                      width: "244px",
                      height: "52px",
                      borderRadius: "0px",
                    }}
                    onClick={handleSubmit}
                  >
                    <Text span>{t("send")}</Text>
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid.Container>
        </Grid>
      </Layout>
    </>
  );
};

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

export default ContactUs;

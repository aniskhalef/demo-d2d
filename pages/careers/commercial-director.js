import CareersGenericComponent from "../../components/careers/GenericComp";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Meta from "../../components/Meta";

const CommercialDirector = () => {
  const { t } = useTranslation("common");
  return (
    <>
      <Meta
        title={t("title_career_devcom")}
        description={t("desc_career_devcom")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>{" "}
      <CareersGenericComponent
        title={t("career4")}
        description={t("career7")}
        mission1={t("career8")}
        mission2={t("career9")}
        mission3={t("career10")}
        mission4={t("career11")}
        mission5={t("career12")}
        qualification1={t("career15")}
        qualification2={t("career16")}
      />
    </>
  );
};

export default CommercialDirector;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

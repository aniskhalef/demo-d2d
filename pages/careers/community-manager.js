import CareersGenericComponent from "../../components/careers/GenericComp";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Meta from "../../components/Meta";

const CommunityManager = () => {
  const { t } = useTranslation("common");
  return (
    <>
      <Meta
        title={t("title_career_community")}
        description={t("desc_career_community")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>{" "}
      <CareersGenericComponent
        title={t("career6")}
        description={t("career32")}
        mission1={t("career33")}
        mission2={t("career34")}
        mission3={t("career35")}
        mission4={t("career36")}
        mission5={t("career37")}
        qualification1={t("career39")}
        qualification2={t("career40")}
      />
    </>
  );
};

export default CommunityManager;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

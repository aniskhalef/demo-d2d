import React from "react";
import CareersGenericComponent from "../../components/careers/GenericComp";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Meta from "../../components/Meta";

const Candidate = () => {
  const { t } = useTranslation("common");

  return (
    <>
      <Meta
        title={t("title_candidtate")}
        description={t("desc_candidate")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>
      <CareersGenericComponent
        title={t("candidate")}
        description={t("candidate_desc")}
        mission1={t("career54")}
        mission2={t("career55")}
        mission3={t("career56")}
        mission4={t("career57")}
        mission5={t("career58")}
        qualification1={t("career59")}
        qualification2={t("career60")}
      />
    </>
  );
};

export default Candidate;

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

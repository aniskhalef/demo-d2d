import CareersGenericComponent from "../../components/careers/GenericComp";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Meta from "../../components/Meta";

const Devops = () => {
  const { t } = useTranslation("common");
  return (
    <>
      <Meta
        title={t("title_career_devops")}
        description={t("desc_career_devops")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>{" "}
      <CareersGenericComponent
        title={t("DEVOPS")}
        description={t("career43")}
        mission1={t("career49")}
        mission2={t("career53")}
        mission3={t("career47")}
        mission4={t("career48")}
        mission5={t("career46")}
        qualification1={t("career50")}
        qualification2={t("career51")}
      />
    </>
  );
};

export default Devops;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

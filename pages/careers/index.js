import React, { Fragment, useState } from "react"; // Import useState hook
import { Grid, Text, Button } from "@nextui-org/react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Meta from "../../components/Meta";
import { useTranslation } from "next-i18next";
import Link from "next/link";
import Layout from "../../layout";
const Careers = () => {
  const { t } = useTranslation("common");
  const [category, setCategory] = useState("all");

  const careers = [
    {
      title: `${t("career4")}`,
      description: `${t("career7")}`,
      path: "/careers/commercial-director",
      color: "#EDC468",
      specialty: "all",
    },
    {
      title: `${t("career5")}`,
      description: `${t("career21")}`,
      path: "/careers/technical-director",
      color: "#C9CDFF",
      specialty: "all",
    },
    {
      title: `${t("career6")}`,
      description: `${t("career32")}`,
      path: "/careers/community-manager",
      color: "#F9AAA4",
      specialty: "all",
    },
    {
      title: `${t("DEVOPS")}`,
      description: `${t("career43")}`,
      path: "/careers/devops",
      color: "#97E2D9",
      specialty: "all",
    },

    {
      title: `${t("candidate")}`,
      description: `${t("candidate_desc")}`,
      path: "/careers/spontanious-candidate",
      color: "#F9D975",
      specialty: "all",
    },
  ];

  return (
    <>
      <Layout>
        <Meta
          title={t("title_career")}
          description={t("desc_career")}
          thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
          keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
        ></Meta>
        <Grid>
          <Grid.Container className="career-container">
            <Grid.Container md={12}>
              {careers
                .filter(
                  (service) =>
                    service.specialty === category || category === "all"
                )
                .map((service) => (
                  <Grid
                    key={service.name}
                    md={4}
                    sm={12}
                    xs={12}
                    className="career"
                  >
                    <Grid>
                      <Grid
                        className="career-image"
                        css={{
                          backgroundColor: `${service.color}`,
                        }}
                      >
                        <Text b className="text-white">
                          {service.title}
                        </Text>

                        <Grid className="career-text">
                          <Text span size={"$xs"}>
                            {service.description}
                          </Text>
                          <br></br>
                          <br></br>
                          <br></br>
                          <br></br>
                          <Link href={service.path}>
                            <Text b size={"$4xl"} className="text-white link">
                              {t("career20")}
                            </Text>
                          </Link>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                ))}
            </Grid.Container>
          </Grid.Container>
        </Grid>
      </Layout>
    </>
  );
};

export default Careers;

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

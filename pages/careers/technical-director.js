import CareersGenericComponent from "../../components/careers/GenericComp";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Meta from "../../components/Meta";

const TechnicalDirector = () => {
  const { t } = useTranslation("common");
  return (
    <>
      <Meta
        title={t("title_career_director")}
        description={t("desc_career_director")}
        thumbnail="https://i.postimg.cc/L6SK9JJc/thumbnail-1.png"
        keywords="Digital Solutions, Digital Transformation, Solutions based on Data, Digital Solutions examples, Web Solutions, Artificial Intelligence, AI, AI solutions Examples, The use of artificial Intelligence in Businesses, AI Marketing"
      ></Meta>

      <CareersGenericComponent
        title={t("career5")}
        description={t("career21")}
        mission1={t("career22")}
        mission2={t("career23")}
        mission3={t("career24")}
        mission4={t("career25")}
        mission5={t("career26")}
        qualification1={t("career30")}
        qualification2={t("career31")}
      />
    </>
  );
};

export default TechnicalDirector;
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, "common")),
  },
});

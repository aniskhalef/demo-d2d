import React, { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { Grid, Button, Text, Input } from "@nextui-org/react";
import { CONSTANTS } from "../constants/index";
import toast from "react-hot-toast";
import { Toaster } from "react-hot-toast";
import axios from "axios";
import Fade from "react-reveal/Fade";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

const RightSide = () => {
  const { t } = useTranslation("common");
  const router = useRouter();

  let textColor = "white";
  let menuPath = "/assets/icons/menu_white.svg";
  let menuTitleIndex = 0;
  const menuItems = [
    { name: `${t("nav1")}`, link: "/" },
    { name: `${t("menu_services")}`, link: "/services" },
    { name: `${t("menu_product")}`, link: "/products" },
    { name: "BLOG", link: "/blog" },
    { name: `${t("menu_careers")}`, link: "/careers" },
    { name: `${t("menu_contact")}`, link: "/contact" },
  ];
  const menuTitles = [
    `${t("nav1")}`,
    `${t("menu_services")}`,
    `${t("menu_product")}`,
    `${t("menu_careers")}`,
    "BLOG",
    `${t("menu_contact")}`,
  ];
  if (
    router.pathname.startsWith("/products/") ||
    router.pathname.startsWith("/careers/")
  ) {
    menuPath = "/assets/icons/menu_black.svg";
    textColor = "black";
  } else if (router.pathname.startsWith("/products/")) {
    menuTitleIndex = 2;
  } else if (router.pathname.startsWith("/careers/")) {
    menuTitleIndex = 3;
  } else if (
    router.pathname === "/careers" ||
    router.pathname === "/services" ||
    router.pathname === "/blog" ||
    router.pathname === "/products"
  ) {
    textColor = "black";
    menuPath = "/assets/icons/menu_black.svg";
  }
  if (router.pathname === "/services") {
    menuTitleIndex = 1;
  } else if (router.pathname === "/blog") {
    menuTitleIndex = 4;
  } else if (router.pathname === "/products") {
    menuTitleIndex = 2;
  } else if (router.pathname.startsWith("/careers")) {
    menuTitleIndex = 3;
  } else if (router.pathname.startsWith("/contact")) {
    menuTitleIndex = 5;
  }

  const [isMenuOpen, setMenuOpen] = useState(false);
  const [email, setEmail] = useState("");
  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };
  const handleSubmit = () => {
    const url = `${CONSTANTS.API_URL_PROD}/user/newsletter`;

    axios
      .post(url, { email })
      .then((response) => {
        console.log(response);
        toast.success("Newsletter sent successfully !");
        setEmail("");
      })
      .catch((error) => {
        console.log(error);
        toast.error("Erreur lors de l'envoi.");
      });
  };
  const handleMenuToggle = () => {
    setMenuOpen(!isMenuOpen);
  };

  const handleMenuClose = () => {
    setMenuOpen(false);
  };

  return (
    <Grid
      className="right-side"
      css={{ position: "absolute", padding: 30, height: "100%" }}
    >
      <Toaster
        position="top-center"
        toastOptions={{
          duration: 5000,
        }}
      />
      <Grid className="hamburger">
        <Image
          src={menuPath}
          alt="duetodata logo"
          width={30}
          height={30}
          objectFit="contain"
          onClick={handleMenuToggle}
        />
      </Grid>
      {isMenuOpen && (
        <Fade right>
          <Grid
            css={{
              position: "absolute",
              height: "101vh",
              right: -30,
              top: -67,
              width: "80vw",
              background: "white ",
              padding: "50px",
              lineHeight: "1.3em",
              boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.10)",
              zIndex: 99999999999,
            }}
            className="menu_slider"
          >
            <Grid className="link" css={{ right: 70, position: "absolute" }}>
              <Image
                src="/assets/icons/close.svg"
                alt="duetodata close"
                width={30}
                height={30}
                objectFit="contain"
                onClick={handleMenuClose}
              />
            </Grid>
            <Grid.Container className="menu_wrapper">
              <Grid xs={4} md={6}>
                <Grid
                  css={{
                    borderRight: "1px #bbbbbb solid",
                    paddingRight: "4em",
                  }}
                >
                  <Grid
                    css={{
                      position: "absolute",
                      top: 0,
                      left: -30,
                      cursor: "pointer",
                    }}
                  >
                    <Link href="/">
                      <Image
                        src="/assets/icons/logo_d2d.svg"
                        alt="duetodata logo"
                        width={290}
                        height={90}
                        objectFit="contain"
                      />
                    </Link>
                  </Grid>
                  <br></br>
                  <br></br>
                  <Grid>
                    <Text h3> {t("whoarewe")}</Text>
                    <br></br>
                    <Text span size={"$xs"}>
                      {t("whoarewe_desc")}
                    </Text>
                  </Grid>

                  <br></br>
                  <Grid>
                    <Grid>
                      <Text h3> {t("value")}</Text>
                    </Grid>
                    <br></br>

                    <Grid>
                      <Text b>{t("telecomvalueexpertise")}</Text>
                      <br></br>
                      <Text span size={"$xs"}>
                        {t("telecomvalueexpertisedesc")}
                      </Text>
                    </Grid>
                  </Grid>

                  <br></br>
                  <Grid>
                    <Text b>{t("telecomvaluefiablite")}</Text>
                    <br></br>

                    <Text span size={"$xs"}>
                      {t("telecomvaluefiablitedesc")}
                    </Text>
                  </Grid>
                  <br></br>
                  <Grid>
                    <Text b>{t("telecomvalueflexibilite")}</Text>
                    <br></br>
                    <Text span size={"$xs"}>
                      {t("telecomvalueflexibilitedesc")}
                    </Text>
                  </Grid>

                  <br></br>
                  <Grid>
                    <Text b>{t("telecomvaluesatisfaction")}</Text>
                    <br></br>
                    <Text span size={"$xs"}>
                      {t("telecomvaluesatisfactiondesc")}
                    </Text>
                  </Grid>

                  <br></br>

                  <Grid>
                    <Text h3>NEWSLETTER</Text>
                    <Text span size={"$xs"}>
                      {t("footer9")}
                    </Text>
                    <br></br>
                    <br></br>

                    <Grid>
                      <Input
                        type="email"
                        value={email}
                        required
                        fullWidth
                        onChange={handleEmailChange}
                        contentRightStyling={false}
                        placeholder="Votre email"
                        contentClickable={true}
                        contentRight={
                          <Button
                            onClick={handleSubmit}
                            size={"$xs"}
                            flat
                            css={{
                              background: "transparent",
                              border: "none",
                              padding: "3px",
                            }}
                          >
                            <Image
                              src="/assets/icons/send.png"
                              width={50}
                              height={40}
                              objectFit="contain"
                              alt=""
                            />
                          </Button>
                        }
                      />
                    </Grid>
                    <br></br>

                    <Grid.Container className="bottom_links">
                      <Grid md={6}>
                        <Link href="/privacy-policy">
                          <Text b size={"$sm"} className="link">
                            {t("footer11")}
                          </Text>
                        </Link>
                      </Grid>

                      <Grid md={6}>
                        <Link href="/terms-and-conditions">
                          <Text b size={"$sm"} className="link">
                            {t("footer12")}
                          </Text>
                        </Link>
                      </Grid>
                    </Grid.Container>
                  </Grid>
                </Grid>
              </Grid>
              <Grid
                css={{
                  display: "flex",
                  flexDirection: "column",
                  lineHeight: "3em",
                  left: "60%",
                  top: "30%",
                  width: "100%",
                  position: "absolute",
                }}
              >
                {menuItems.map((item) => (
                  <Link href={item.link} key={item.name}>
                    <Text b size={"$2xl"} className="menu">
                      {item.name}
                    </Text>
                  </Link>
                ))}
              </Grid>
            </Grid.Container>
          </Grid>
        </Fade>
      )}

      <Grid
        css={{ writingMode: "vertical-rl", position: "absolute", top: "50%" }}
      >
        <Text b css={{ color: textColor, textTransform: "uppercase" }}>
          {menuTitles[menuTitleIndex]}
        </Text>
      </Grid>
      <Grid
        css={{
          position: "absolute",
          right: 50,
          bottom: 30,
        }}
      >
        {router.locale === "fr" ? (
          <Grid css={{ cursor: "pointer" }}>
            <Text
              span
              css={{ color: textColor }}
              onClick={() => router.push("", "", { locale: "en" })}
            >
              EN
            </Text>
          </Grid>
        ) : (
          <Grid css={{ cursor: "pointer" }}>
            <Text
              span
              css={{ color: textColor }}
              onClick={() => router.push("", "", { locale: "fr" })}
            >
              FR
            </Text>
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

export default RightSide;

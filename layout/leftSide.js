import React from "react";
import Link from "next/link";
import Image from "next/image";
import { Grid } from "@nextui-org/react";
import { useRouter } from "next/router";
import ProductNavigation from "../components/products/Navigation";
import ServiceNavigation from "../components/services/dev/Navigation";
import BlogNavigation from "../components/blogs/Navigation";

const LeftSide = () => {
  const router = useRouter();
  let logoPath = "";

  const isProductRoute = router.pathname.startsWith("/products/");
  const isSeviceRoute = router.pathname.startsWith("/services/");
  const isBlogRoute = router.pathname.startsWith("/blog/");

  if (isProductRoute) {
    return <ProductNavigation />;
  }
  if (isSeviceRoute) {
    return <ServiceNavigation />;
  }
  if (isBlogRoute) {
    return <BlogNavigation />;
  }
  if (
    router.pathname === "/careers" ||
    router.pathname === "/services" ||
    router.pathname === "/products" ||
    router.pathname === "/blog" ||
    router.pathname === "/terms-and-conditions" ||
    router.pathname === "/privacy-policy"
  ) {
    logoPath = "/assets/icons/logo_orange.svg";
  } else if (router.pathname.match(/^\/careers\//)) {
    logoPath = "/assets/icons/logo_orange.svg";
  } else {
    logoPath = "/assets/icons/logo_white.svg";
  }

  const socialIcons = [
    {
      icon: "/assets/icons/insta.svg",
      alt: "Instagram",
      link: "https://www.instagram.com/duetodata/",
    },
    {
      icon: "/assets/icons/fb.svg",
      alt: "Facebook",
      link: "https://www.facebook.com/DuetoData/",
    },
    {
      icon: "/assets/icons/linkedin.svg",
      alt: "LinkedIn",
      link: "https://www.linkedin.com/company/duetodata/?originalSubdomain=fr",
    },
    {
      icon: "/assets/icons/pinterest.svg",
      alt: "Pinterest",
      link: "https://www.pinterest.com/Due2Data/",
    },
  ];
  return (
    <Grid className="left-side" css={{ position: "absolute" }}>
      {isProductRoute || isBlogRoute || isSeviceRoute ? (
        <BlogNavigation /> || <ProductNavigation /> || <ServiceNavigation />
      ) : (
        <>
          <Grid css={{ padding: 30, cursor: "pointer" }}>
            <Link href="/">
              <Image
                src={logoPath}
                alt="duetodata logo"
                width={80}
                height={80}
                objectFit="contain"
              />
            </Link>
          </Grid>

          <Grid className="bg_sm">
            {socialIcons.map((socialIcon, index) => (
              <a
                key={index}
                href={socialIcon.link}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image
                  src={socialIcon.icon}
                  alt={socialIcon.alt}
                  width={25}
                  height={25}
                  objectFit="contain"
                />
                <br />
              </a>
            ))}
          </Grid>
        </>
      )}
    </Grid>
  );
};

export default LeftSide;

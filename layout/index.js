// Layout.js
import React from "react";
import LeftSide from "./leftSide";
import RightSide from "./rightSide";
import Navbar from "./Navbar"; // Import your updated Navbar component
import { useEffect, useState } from "react";

export default function Layout({ children }) {
  const [isMobile, setIsMobile] = useState(false);
  const [isMenuOpen, setMenuOpen] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    window.addEventListener("resize", handleResize);

    handleResize();

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const handleMenuToggle = () => {
    setMenuOpen(!isMenuOpen);
  };

  return (
    <>
      {isMobile && <Navbar handleMenuToggle={handleMenuToggle} />}

      <div
        style={{
          display: "flex",
          width: "100%",
          height: "100%",
          justifyContent: "space-between",
        }}
      >
        <LeftSide />

        <main>{children}</main>

        <RightSide />
      </div>
    </>
  );
}

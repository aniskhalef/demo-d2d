import React, { useState } from "react";
import Image from "next/image";
import { Grid, Text, Input, Button } from "@nextui-org/react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import Fade from "react-reveal/Fade";
import axios from "axios";
import toast, { Toaster } from "react-hot-toast";
import { CONSTANTS } from "../constants/index";
import Link from "next/link";
const Navbar = () => {
  const { t } = useTranslation("common");
  const router = useRouter();
  let logoPath = "";
  let menu_path = "";
  if (
    router.pathname === "/careers" ||
    router.pathname === "/services" ||
    router.pathname === "/blog" ||
    router.pathname === "/products" ||
    router.pathname === "/terms-and-conditions" ||
    router.pathname === "/privacy-policy"
  ) {
    logoPath = "/assets/icons/logo_orange.svg";
    menu_path = "/assets/icons/menu_black.svg";
  } else if (
    router.pathname === "/" ||
    router.pathname === "/contact" ||
    router.pathname === "/contact-us"
  ) {
    logoPath = "/assets/icons/logo_white.svg";
    menu_path = "/assets/icons/menu_white.svg";
  } else if (router.pathname.startsWith("/careers/")) {
    logoPath = "/assets/icons/logo_orange.svg";
    menu_path = "/assets/icons/menu_black.svg";
  } else if (router.pathname.startsWith("/products/")) {
    logoPath = "/assets/icons/logo_black.svg";
    menu_path = "/assets/icons/menu_black.svg";
  } else {
    menu_path = "/assets/icons/menu_white.svg";
    logoPath = "/assets/icons/logo_white.svg";
  }
  const [isMenuOpen, setMenuOpen] = useState(false);
  const [email, setEmail] = useState("");

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleSubmit = () => {
    const url = `${CONSTANTS.API_URL_PROD}/user/newsletter`;

    axios
      .post(url, { email })
      .then((response) => {
        console.log(response);
        toast.success("Newsletter sent successfully !");
        setEmail("");
      })
      .catch((error) => {
        console.log(error);
        toast.error("Erreur lors de l'envoi.");
      });
  };
  const handleMenuToggle = () => {
    setMenuOpen(!isMenuOpen);
  };

  const handleMenuClose = () => {
    setMenuOpen(false);
  };

  const menuItems = [
    { name: "MENU", link: "/" },
    { name: `${t("menu_services")}`, link: "/services" },
    { name: `${t("menu_product")}`, link: "/products" },
    { name: "BLOG", link: "/blog" },
    { name: `${t("menu_careers")}`, link: "/careers" },
    { name: `${t("menu_contact")}`, link: "/contact" },
  ];

  return (
    <>
      <Grid className="navbar">
        <Link href="/">
          <Image
            src={logoPath}
            alt="duetodata logo"
            width={40}
            height={30}
            objectFit="contain"
          />
        </Link>

        <Image
          src={menu_path}
          alt="Menu"
          width={30}
          height={30}
          objectFit="contain"
          onClick={handleMenuToggle}
        />
      </Grid>
      {isMenuOpen && (
        <Fade right>
          <Grid
            css={{
              padding: "40px",
              lineHeight: "1.3em",
              boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.10)",
              backgroundColor: "#fff",
              height: "100%",
              marginTop: -50,
            }}
          >
            <Grid
              css={{
                position: "absolute",
                top: 30,
                left: -40,
                cursor: "pointer",
              }}
            >
              <Grid>
                <Link href="/">
                  <Image
                    src="/assets/icons/logo_d2d.svg"
                    alt="duetodata logo"
                    width={290}
                    height={90}
                    objectFit="contain"
                  />
                </Link>
              </Grid>
              <Grid
                css={{
                  position: "absolute",
                  right: 0,
                  top: 40,
                }}
              >
                {router.locale === "fr" ? (
                  <Grid css={{ cursor: "pointer" }}>
                    <Text
                      b
                      css={{ color: "black" }}
                      onClick={() => router.push("", "", { locale: "en" })}
                    >
                      EN
                    </Text>
                  </Grid>
                ) : (
                  <Grid css={{ cursor: "pointer" }}>
                    <Text
                      b
                      css={{ color: "black" }}
                      onClick={() => router.push("", "", { locale: "fr" })}
                    >
                      FR
                    </Text>
                  </Grid>
                )}
              </Grid>
              <Grid
                className="link"
                css={{ position: "absolute", top: 40, right: -120 }}
              >
                <Image
                  src="/assets/icons/close.svg"
                  alt="duetodata close"
                  width={30}
                  height={20}
                  objectFit="contain"
                  onClick={handleMenuClose}
                />
              </Grid>
            </Grid>
            <Grid>
              <Grid
                css={{
                  display: "flex",
                  flexDirection: "column",
                  lineHeight: "3em",
                  marginTop: "4em",
                }}
              >
                {menuItems.map((item) => (
                  <Link href={item.link} key={item.name}>
                    <Text b size={"$2xl"} className="menu">
                      {item.name}
                    </Text>
                  </Link>
                ))}
              </Grid>
              <Grid>
                <Grid>
                  <br></br>
                  <br></br>
                  <Grid>
                    <Text h3> {t("whoarewe")}</Text>
                    <br></br>
                    <Text span size={"$xs"}>
                      {t("whoarewe_desc")}
                    </Text>
                  </Grid>

                  <br></br>
                  <Grid>
                    <Grid>
                      <Text h3> {t("value")}</Text>
                    </Grid>
                    <br></br>

                    <Grid>
                      <Text b>{t("telecomvalueexpertise")}</Text>
                      <br></br>
                      <Text span size={"$xs"}>
                        {t("telecomvalueexpertisedesc")}
                      </Text>
                    </Grid>
                  </Grid>

                  <br></br>
                  <Grid>
                    <Text b>{t("telecomvaluefiablite")}</Text>
                    <br></br>

                    <Text span size={"$xs"}>
                      {t("telecomvaluefiablitedesc")}
                    </Text>
                  </Grid>
                  <br></br>
                  <Grid>
                    <Text b>{t("telecomvalueflexibilite")}</Text>
                    <br></br>
                    <Text span size={"$xs"}>
                      {t("telecomvalueflexibilitedesc")}
                    </Text>
                  </Grid>

                  <br></br>
                  <Grid>
                    <Text b>{t("telecomvaluesatisfaction")}</Text>
                    <br></br>
                    <Text span size={"$xs"}>
                      {t("telecomvaluesatisfactiondesc")}
                    </Text>
                  </Grid>

                  <br></br>

                  <Grid>
                    <Text h3>NEWSLETTER</Text>
                    <Text span size={"$xs"}>
                      {t("footer9")}
                    </Text>
                    <br></br>
                    <br></br>

                    <Grid>
                      <Input
                        type="email"
                        value={email}
                        required
                        fullWidth
                        onChange={handleEmailChange}
                        contentRightStyling={false}
                        placeholder="Votre email"
                        contentClickable={true}
                        contentRight={
                          <Button
                            onClick={handleSubmit}
                            size={"$xs"}
                            flat
                            css={{
                              background: "transparent",
                              border: "none",
                              padding: "3px",
                            }}
                          >
                            <Image
                              src="/assets/icons/send.png"
                              width={50}
                              height={40}
                              objectFit="contain"
                              alt=""
                            />
                          </Button>
                        }
                      />
                    </Grid>
                    <br></br>

                    <Grid.Container gap={2}>
                      <Grid md={6}>
                        <Link href="/privacy-policy">
                          <Text b size={"$sm"} className="link">
                            {t("footer11")}
                          </Text>
                        </Link>
                      </Grid>

                      <Grid md={6}>
                        <Link href="/terms-and-conditions">
                          <Text b size={"$sm"} className="link">
                            {t("footer12")}
                          </Text>
                        </Link>
                      </Grid>
                    </Grid.Container>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>{" "}
          </Grid>
        </Fade>
      )}
    </>
  );
};

export default Navbar;
